#!/usr/bin/env bash

SDK_ROOT=$NEXT_SDK_ROOT
echo SDK_ROOT = $SDK_ROOT
ASM=$SDK_ROOT/sjasmplus/sjasmplus
echo ASM = $ASM

$ASM src/main.s -I/env/src --zxnext=cspect --msg=war --fullpath || exit

echo Generating dot command...
cat odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data > odin
rm odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data

#echo Copying Odin to Emulator SD card...
../env/hdfmonkey/hdfmonkey put ../env/tbblue.mmc odin /dot
../env/hdfmonkey/hdfmonkey put ../env/tbblue.mmc docs/odin.gde /docs/guides
../env/hdfmonkey/hdfmonkey put ../env/tbblue.mmc etc/learn.odn /

#\env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/plottest/* /
#call i.bat
#)
