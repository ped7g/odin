#!/usr/bin/env bash
# check for "-h" arg

PATH="$HOME/next/env/sjasmplus:$HOME/next/env/hdfmonkey:$PATH"

if [[ "-h" == "$1" ]]; then
    echo "build.sh [<directory1> ...] [<file>.mmc|<file>.img [1] [2]]"
    echo " * if directory(ies) is/are provided, the odin files will be copied there"
    echo " * \".mmc\" or \".img\" file to use hdfmonkey to put odin into image (or set MMC env var)"
    echo " * if MMC image is provided, add 1 or 2 to launch CSpect or ZESERUse with MMC image"
    exit
fi

if [[ "test" == "$1" ]]; then
    # assemble test-runner NEX files
    sjasmplus src/testr.s -D__T_SUITE_3 -I/env/src --zxnext=cspect --msg=war --fullpath --lst --lstlab=sort || exit
    sjasmplus src/testr.s -D__T_SUITE_2 -I/env/src --zxnext=cspect --msg=war --fullpath --lst --lstlab=sort || exit
    sjasmplus src/testr.s -D__T_SUITE_1 -I/env/src --zxnext=cspect --msg=war --fullpath --lst --lstlab=sort || exit
    # check if TEST_MMC is defined or there's file at default place, run MMC-contained tests with such image file
    if [[ -z $TEST_MMC && -s "../odin_tst.mmc" ]]; then
        TEST_MMC="../odin_tst.mmc"
    fi
    if [[ -s "$TEST_MMC" ]]; then
        # first assemble test asm file with sjasmplus for comparison
        sjasmplus etc/t_odin_sj.asm --msg=war --fullpath || exit
        # assemble and merge final binary
        sjasmplus src/main.s -D_TEST_KEYS_INJECT_FILE_=src/tests/t_injectkeys_opcodes.s -I/env/src --zxnext=cspect --msg=war --fullpath && \
        cat odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data > odin && \
        rm odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data || exit
        echo "Test-odin binary built successfully"
        # copy test files and odin to the MMC image, remove old binary result, and run test
        rm -f _t_odin.bin.tmp
        hdfmonkey rm "$TEST_MMC" _t_odin.bin > /dev/null
        echo "Copying test-odin binary to MMC image: $TEST_MMC"
        hdfmonkey put "$TEST_MMC" etc/*.asm / && \
        hdfmonkey put "$TEST_MMC" odin /dot && \
        hdfmonkey put "$TEST_MMC" etc/autoexec.bas /nextzxos && \
        mmcCSpect "$TEST_MMC" -brk -map=odin.map
        # extract resulting bin file from running the t_injectkeys_opcodes.s
        hdfmonkey get "$TEST_MMC" /_t_odin.bin > _t_odin.bin.tmp || echo "[ERROR] binary output of Odin not found in the card image"
        diff _t_odin.bin.tmp _t_odin_sj.bin.tmp && rm _t_odin_sj.bin.tmp || exit
        echo "[OK] binary output of odin did match sjasmplus output (etc/t_odin.asm)"
    fi
    # run the NEX-files (from the oldest built, which is usually the one most worked on)
    for nexfile in `ls -r -t *.nex`; do
        runCSpect -brk -map=testr.map $nexfile
    done
    exit
fi

# assemble and merge final binary
sjasmplus src/main.s -I/env/src --zxnext=cspect --msg=war --fullpath && \
cat odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data > odin && \
rm odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data || exit

echo "Odin binary built successfully"

for argx in "$@"; do
    # check if ".img" or ".mmc" file was part of command line, use it as MMC variable then
    if [[ "mmc" == ${argx##*.} || "img" == ${argx##*.} ]]; then
        if [[ -f "$argx" && -s "$argx" && -r "$argx" && -w "$argx" ]]; then
            echo "MMC image filename provided: $argx (odin will be copied to it)"
            MMC="$argx"
        else
            echo "MMC image \"$argx\" not found or not writeable!"
        fi
    fi
    # check if some dir was provided as CLI argument, copy files to it as if it was card
    if [[ -d "$argx" && -r "$argx" && -w "$argx" ]]; then
        echo "Copying odin to provided directory: $argx"
        mkdir -p "$argx/dot" "$argx/docs/guides"
        cp odin "$argx/dot/" && \
        cp docs/odin.gde "$argx/docs/guides/" && \
        cp etc/learn.odn "$argx/" && \
        cp etc/plottest/* "$argx/"
    fi
done

# do the extra copying to provided MMC file or launching when requested
if [[ -f "$MMC" && -s "$MMC" ]]; then
    echo "Copying odin binary to MMC image: $MMC"
    hdfmonkey put "$MMC" odin /dot && \
    hdfmonkey put "$MMC" docs/odin.gde /docs/guides && \
    hdfmonkey put "$MMC" etc/learn.odn / && \
    hdfmonkey put "$MMC" etc/plottest/* /

    # launch emulator if requested ("1" to run CSpect, "2" to run ZESERUse)
    for argx in "$@"; do
        [[ "1" == "$argx" ]] && mmcCSpect "$MMC" -brk   # check etc/mmcCSpect and etc/mmczeseruse
        [[ "2" == "$argx" ]] && mmczeseruse "$MMC"      # for examples of launch script
    done
fi
