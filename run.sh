source make.sh

if [ $? -eq 0 ]
then
  ../env/hdfmonkey/hdfmonkey put ../env/tbblue.mmc etc/autoexec.bas /nextzxos
  mono ../env/cspect/CSpect.exe -sound -r -tv -brk -16bit -s28 -w3 -zxnext -nextrom -map=odin.map -mmc=../env/tbblue.mmc
fi

