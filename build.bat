@echo off

setlocal

set SDK_ROOT=n:\env
set ASM=%SDK_ROOT%\sjasmplus\sjasmplus
set MONKEY=%SDK_ROOT%\hdfmonkey\hdfmonkey
set CSPECT=%SDK_ROOT%\cspect\cspect.exe

%ASM% src/main.s --zxnext=cspect --msg=war --fullpath
if %errorlevel% neq 0 exit /b %errorlevel%
copy /b odin_boot+odin_shared+odin_monitor+odin_editor+odin_asm+odin_dbg+odin_data odin
if %errorlevel% neq 0 exit /b %errorlevel%
rm odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data

echo Binary built successfully

echo Copying to Emulator SD card...
%MONKEY% put %SDK_ROOT%/tbblue.mmc odin /dot

echo Done

