; Test file for Z80N instructions

NN              equ     $1234
N               equ     $56

Start:
                ; ED 20
                swapnib                 ; ED 23
                mirror  a               ; ED 24
                test    N               ; ED 27 56
                bsla    de,b            ; ED 28
                bsra    de,b            ; ED 29
                bsrl    de,b            ; ED 2A
                bsrf    de,b            ; ED 2B
                brlc    de,b            ; ED 2C

                ; ED 30
                mul                     ; ED 30
                add     hl,a            ; ED 31
                add     de,a            ; ED 32
                add     bc,a            ; ED 33
                add     hl,NN           ; ED 34 34 12
                add     de,NN           ; ED 35 34 12
                add     bc,NN           ; ED 36 34 12

                ; ED 80
                push    NN              ; ED 8A 12 34

                ; ED 90
                outinb                  ; ED 90
                nextreg N,42            ; ED 91 56 2A
                nextreg N,a             ; ED 92 56
                pixeldn                 ; ED 93
                pixelad                 ; ED 94
                setae                   ; ED 95
                jp      (c)             ; ED 98

                ; ED A0
                ldix                    ; ED A4
                ldws                    ; ED A5
                lddx                    ; ED AC

                ; ED B0
                ldirx                   ; ED B4
                ldpirx                  ; ED B7
                lddrx                   ; ED BC



