; Test file for Z80N instructions

NN              equ     $1234
N               equ     $56

Start:
                ; ED 20
                swap                    ; ED 23
                mirr                    ; ED 24
                test    N               ; ED 27 56
                bsla    de,b            ; ED 28
                bsra    de,b            ; ED 29
                bsrl    de,b            ; ED 2A
                bsrf    de,b            ; ED 2B
                brlc    de,b            ; ED 2C

                ; ED 30
                mul                     ; ED 30
                add     hl,a            ; ED 31
                add     de,a            ; ED 32
                add     bc,a            ; ED 33
                add     hl,NN           ; ED 34 34 12
                add     de,NN           ; ED 35 34 12
                add     bc,NN           ; ED 36 34 12

                ; ED 80
                push    NN              ; ED 8A 12 34

                ; ED 90
                otib                    ; ED 90
                nreg    N,42            ; ED 91 56 2A
                nreg    N,a             ; ED 92 56
                pxdn                    ; ED 93
                pxad                    ; ED 94
                stae                    ; ED 95
                jp      (c)             ; ED 98

                ; ED A0
                ldix                    ; ED A4
                ldws                    ; ED A5
                lddx                    ; ED AC

                ; ED B0
                lirx                    ; ED B4
                lprx                    ; ED B7
                ldrx                    ; ED BC



