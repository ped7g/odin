;;; automated test with only valid lines of assembly, producing _t_odin.bin
; this is used by build.sh to produce the bin file, then extract it and "diff" it
; on Ped's local machine that means to run full set of automated tests:
;   TEST_MMC="../odin_tst.mmc" ./build.sh test
; After each test in emulator does finish, close the emulator to continue the build script.

;;; 1) classic Z80 opcodes

NN              equ     $1234
N               equ     $56
DISP            equ     -128

Start:
	nop			            ; 00
	ld      bc,NN           ; 01 34 12
	ld      (bc),a          ; 02
	inc     bc              ; 03
	inc     b               ; 04
	dec     b               ; 05
	ld      b,N             ; 06 56
	rlca                    ; 07
	ex      af,af'          ; 08
	add     hl,bc           ; 09
	ld      a,(bc)          ; 0a
	dec     bc              ; 0b
	inc     c               ; 0c
	dec     c               ; 0d
	ld      c,N             ; 0e 56
	rrca                    ; 0f

	djnz    $+2             ; 10 00
	ld      de,NN           ; 11 34 12
	ld      (de),a          ; 12
	inc     de              ; 13
	inc     d               ; 14
	dec     d               ; 15
	ld      d,N             ; 16 56
	rla                     ; 17
	jr      $+2             ; 18 00
	add     hl,de           ; 19
	ld      a,(de)          ; 1a
	dec     de              ; 1b
	inc     e               ; 1c
	dec     e               ; 1d
	ld      e,N             ; 1e 56
	rra                     ; 1f

	jr      nz,$+2          ; 20 00
	ld      hl,NN           ; 21 34 12
	ld      (NN),hl         ; 22 34 12
	inc     hl              ; 23
	inc     h               ; 24
	dec     h               ; 25
	ld      h,N             ; 26 56
	daa                     ; 27
	jr      z,$+2           ; 28 00
	add     hl,hl           ; 29
	ld      hl,(NN)         ; 2a 34 12
	dec     hl              ; 2b
	inc     l               ; 2c
	dec     l               ; 2d
	ld      l,N             ; 2e 56
	cpl                     ; 2f

	jr      nc,$+2          ; 30 00
	ld      sp,NN           ; 31 34 12
	ld      (NN),a          ; 32 34 12
	inc     sp              ; 33
	inc     (hl)            ; 34
	dec     (hl)            ; 35
	ld      (hl),N          ; 36 56
	scf                     ; 37
	jr      c,$+2           ; 38 00
	add     hl,sp           ; 39
	ld      a,(NN)          ; 3a 34 12
	dec     sp              ; 3b
	inc     a               ; 3c
	dec     a               ; 3d
	ld      a,N             ; 3e 56
	ccf                     ; 3f

	ld      b,b             ; 40
	ld      b,c             ; 41
	ld      b,d             ; 42
	ld      b,e             ; 43
	ld      b,h             ; 44
	ld      b,l             ; 45
	ld      b,(hl)          ; 46
	ld      b,a             ; 47
	ld      c,b             ; 48
	ld      c,c             ; 49
	ld      c,d             ; 4a
	ld      c,e             ; 4b
	ld      c,h             ; 4c
	ld      c,l             ; 4d
	ld      c,(hl)          ; 4e
	ld      c,a             ; 4f

	ld      d,b             ; 50
	ld      d,c             ; 51
	ld      d,d             ; 52
	ld      d,e             ; 53
	ld      d,h             ; 54
	ld      d,l             ; 55
	ld      d,(hl)          ; 56
	ld      d,a             ; 57
	ld      e,b             ; 58
	ld      e,c             ; 59
	ld      e,d             ; 5a
	ld      e,e             ; 5b
	ld      e,h             ; 5c
	ld      e,l             ; 5d
	ld      e,(hl)          ; 5e
	ld      e,a             ; 5f

	ld      h,b             ; 60
	ld      h,c             ; 61
	ld      h,d             ; 62
	ld      h,e             ; 63
	ld      h,h             ; 64
	ld      h,l             ; 65
	ld      h,(hl)          ; 66
	ld      h,a             ; 67
	ld      l,b             ; 68
	ld      l,c             ; 69
	ld      l,d             ; 6a
	ld      l,e             ; 6b
	ld      l,h             ; 6c
	ld      l,l             ; 6d
	ld      l,(hl)          ; 6e
	ld      l,a             ; 6f

	ld      (hl),b          ; 70
	ld      (hl),c          ; 71
	ld      (hl),d          ; 72
	ld      (hl),e          ; 73
	ld      (hl),h          ; 74
	ld      (hl),l          ; 75
	halt                    ; 76
	ld      (hl),a          ; 77
	ld      a,b             ; 78
	ld      a,c             ; 79
	ld      a,d             ; 7a
	ld      a,e             ; 7b
	ld      a,h             ; 7c
	ld      a,l             ; 7d
	ld      a,(hl)          ; 7e
	ld      a,a             ; 7f

	add     a,b             ; 80
	add     a,c             ; 81
	add     a,d             ; 82
	add     a,e             ; 83
	add     a,h             ; 84
	add     a,l             ; 85
	add     a,(hl)          ; 86
	add     a,a             ; 87
	adc     a,b             ; 88
	adc     a,c             ; 89
	adc     a,d             ; 8a
	adc     a,e             ; 8b
	adc     a,h             ; 8c
	adc     a,l             ; 8d
	adc     a,(hl)          ; 8e
	adc     a,a             ; 8f

	sub     b               ; 90
	sub     c               ; 91
	sub     d               ; 92
	sub     e               ; 93
	sub     h               ; 94
	sub     l               ; 95
	sub     (hl)            ; 96
	sub     a               ; 97
	sbc     b               ; 98
	sbc     c               ; 99
	sbc     d               ; 9a
	sbc     e               ; 9b
	sbc     h               ; 9c
	sbc     l               ; 9d
	sbc     (hl)            ; 9e
	sbc     a               ; 9f

	and     b               ; a0
	and     c               ; a1
	and     d               ; a2
	and     e               ; a3
	and     h               ; a4
	and     l               ; a5
	and     (hl)            ; a6
	and     a               ; a7
	xor     b               ; a8
	xor     c               ; a9
	xor     d               ; aa
	xor     e               ; ab
	xor     h               ; ac
	xor     l               ; ad
	xor     (hl)            ; ae
	xor     a               ; af

	or      b               ; b0
	or      c               ; b1
	or      d               ; b2
	or      e               ; b3
	or      h               ; b4
	or      l               ; b5
	or      (hl)            ; b6
	or      a               ; b7
	cp      b               ; b8
	cp      c               ; b9
	cp      d               ; ba
	cp      e               ; bb
	cp      h               ; bc
	cp      l               ; bd
	cp      (hl)            ; be
	cp      a               ; bf

	ret     nz              ; c0
	pop     bc              ; c1
	jp      nz,NN           ; c2 34 12
	jp      NN              ; c3 34 12
	call    nz,NN           ; c4 34 12
	push    bc              ; c5
	add     a,N             ; c6 56
	rst     0               ; c7
	ret     z               ; c8
	ret                     ; c9
	jp      z,NN            ; ca 34 12

	rlc     b               ; cb 00
	rlc     c               ; cb 01
	rlc     d               ; cb 02
	rlc     e               ; cb 03
	rlc     h               ; cb 04
	rlc     l               ; cb 05
	rlc     (hl)            ; cb 06
	rlc     a               ; cb 07
	rrc     b               ; cb 08
	rrc     c               ; cb 09
	rrc     d               ; cb 0a
	rrc     e               ; cb 0b
	rrc     h               ; cb 0c
	rrc     l               ; cb 0d
	rrc     (hl)            ; cb 0e
	rrc     a               ; cb 0f
	rl      b               ; cb 10
	rl      c               ; cb 11
	rl      d               ; cb 12
	rl      e               ; cb 13
	rl      h               ; cb 14
	rl      l               ; cb 15
	rl      (hl)            ; cb 16
	rl      a               ; cb 17
	rr      b               ; cb 18
	rr      c               ; cb 19
	rr      d               ; cb 1a
	rr      e               ; cb 1b
	rr      h               ; cb 1c
	rr      l               ; cb 1d
	rr      (hl)            ; cb 1e
	rr      a               ; cb 1f
	sla     b               ; cb 20
	sla     c               ; cb 21
	sla     d               ; cb 22
	sla     e               ; cb 23
	sla     h               ; cb 24
	sla     l               ; cb 25
	sla     (hl)            ; cb 26
	sla     a               ; cb 27
	sra     b               ; cb 28
	sra     c               ; cb 29
	sra     d               ; cb 2a
	sra     e               ; cb 2b
	sra     h               ; cb 2c
	sra     l               ; cb 2d
	sra     (hl)            ; cb 2e
	sra     a               ; cb 2f
	sl1     b               ; cb 30
	sl1     c               ; cb 31
	sl1     d               ; cb 32
	sl1     e               ; cb 33
	sl1     h               ; cb 34
	sl1     l               ; cb 35
	sl1     (hl)            ; cb 36
	sl1     a               ; cb 37
	srl     b               ; cb 38
	srl     c               ; cb 39
	srl     d               ; cb 3a
	srl     e               ; cb 3b
	srl     h               ; cb 3c
	srl     l               ; cb 3d
	srl     (hl)            ; cb 3e
	srl     a               ; cb 3f

	bit     0,b             ; cb 40
	bit     0,c             ; cb 41
	bit     0,d             ; cb 42
	bit     0,e             ; cb 43
	bit     0,h             ; cb 44
	bit     0,l             ; cb 45
	bit     0,(hl)          ; cb 46
	bit     0,a             ; cb 47
	bit     1,b             ; cb 48
	bit     1,c             ; cb 49
	bit     1,d             ; cb 4a
	bit     1,e             ; cb 4b
	bit     1,h             ; cb 4c
	bit     1,l             ; cb 4d
	bit     1,(hl)          ; cb 4e
	bit     1,a             ; cb 4f
	bit     2,b             ; cb 50
	bit     2,c             ; cb 51
	bit     2,d             ; cb 52
	bit     2,e             ; cb 53
	bit     2,h             ; cb 54
	bit     2,l             ; cb 55
	bit     2,(hl)          ; cb 56
	bit     2,a             ; cb 57
	bit     3,b             ; cb 58
	bit     3,c             ; cb 59
	bit     3,d             ; cb 5a
	bit     3,e             ; cb 5b
	bit     3,h             ; cb 5c
	bit     3,l             ; cb 5d
	bit     3,(hl)          ; cb 5e
	bit     3,a             ; cb 5f
	bit     4,b             ; cb 60
	bit     4,c             ; cb 61
	bit     4,d             ; cb 62
	bit     4,e             ; cb 63
	bit     4,h             ; cb 64
	bit     4,l             ; cb 65
	bit     4,(hl)          ; cb 66
	bit     4,a             ; cb 67
	bit     5,b             ; cb 68
	bit     5,c             ; cb 69
	bit     5,d             ; cb 6a
	bit     5,e             ; cb 6b
	bit     5,h             ; cb 6c
	bit     5,l             ; cb 6d
	bit     5,(hl)          ; cb 6e
	bit     5,a             ; cb 6f
	bit     6,b             ; cb 70
	bit     6,c             ; cb 71
	bit     6,d             ; cb 72
	bit     6,e             ; cb 73
	bit     6,h             ; cb 74
	bit     6,l             ; cb 75
	bit     6,(hl)          ; cb 76
	bit     6,a             ; cb 77
	bit     7,b             ; cb 78
	bit     7,c             ; cb 79
	bit     7,d             ; cb 7a
	bit     7,e             ; cb 7b
	bit     7,h             ; cb 7c
	bit     7,l             ; cb 7d
	bit     7,(hl)          ; cb 7e
	bit     7,a             ; cb 7f

	res     0,b             ; cb 80
	res     0,c             ; cb 81
	res     0,d             ; cb 82
	res     0,e             ; cb 83
	res     0,h             ; cb 84
	res     0,l             ; cb 85
	res     0,(hl)          ; cb 86
	res     0,a             ; cb 87
	res     1,b             ; cb 88
	res     1,c             ; cb 89
	res     1,d             ; cb 8a
	res     1,e             ; cb 8b
	res     1,h             ; cb 8c
	res     1,l             ; cb 8d
	res     1,(hl)          ; cb 8e
	res     1,a             ; cb 8f
	res     2,b             ; cb 90
	res     2,c             ; cb 91
	res     2,d             ; cb 92
	res     2,e             ; cb 93
	res     2,h             ; cb 94
	res     2,l             ; cb 95
	res     2,(hl)          ; cb 96
	res     2,a             ; cb 97
	res     3,b             ; cb 98
	res     3,c             ; cb 99
	res     3,d             ; cb 9a
	res     3,e             ; cb 9b
	res     3,h             ; cb 9c
	res     3,l             ; cb 9d
	res     3,(hl)          ; cb 9e
	res     3,a             ; cb 9f
	res     4,b             ; cb a0
	res     4,c             ; cb a1
	res     4,d             ; cb a2
	res     4,e             ; cb a3
	res     4,h             ; cb a4
	res     4,l             ; cb a5
	res     4,(hl)          ; cb a6
	res     4,a             ; cb a7
	res     5,b             ; cb a8
	res     5,c             ; cb a9
	res     5,d             ; cb aa
	res     5,e             ; cb ab
	res     5,h             ; cb ac
	res     5,l             ; cb ad
	res     5,(hl)          ; cb ae
	res     5,a             ; cb af
	res     6,b             ; cb b0
	res     6,c             ; cb b1
	res     6,d             ; cb b2
	res     6,e             ; cb b3
	res     6,h             ; cb b4
	res     6,l             ; cb b5
	res     6,(hl)          ; cb b6
	res     6,a             ; cb b7
	res     7,b             ; cb b8
	res     7,c             ; cb b9
	res     7,d             ; cb ba
	res     7,e             ; cb bb
	res     7,h             ; cb bc
	res     7,l             ; cb bd
	res     7,(hl)          ; cb be
	res     7,a             ; cb bf

	set     0,b             ; cb c0
	set     0,c             ; cb c1
	set     0,d             ; cb c2
	set     0,e             ; cb c3
	set     0,h             ; cb c4
	set     0,l             ; cb c5
	set     0,(hl)          ; cb c6
	set     0,a             ; cb c7
	set     1,b             ; cb c8
	set     1,c             ; cb c9
	set     1,d             ; cb ca
	set     1,e             ; cb cb
	set     1,h             ; cb cc
	set     1,l             ; cb cd
	set     1,(hl)          ; cb ce
	set     1,a             ; cb cf
	set     2,b             ; cb d0
	set     2,c             ; cb d1
	set     2,d             ; cb d2
	set     2,e             ; cb d3
	set     2,h             ; cb d4
	set     2,l             ; cb d5
	set     2,(hl)          ; cb d6
	set     2,a             ; cb d7
	set     3,b             ; cb d8
	set     3,c             ; cb d9
	set     3,d             ; cb da
	set     3,e             ; cb db
	set     3,h             ; cb dc
	set     3,l             ; cb dd
	set     3,(hl)          ; cb de
	set     3,a             ; cb df
	set     4,b             ; cb e0
	set     4,c             ; cb e1
	set     4,d             ; cb e2
	set     4,e             ; cb e3
	set     4,h             ; cb e4
	set     4,l             ; cb e5
	set     4,(hl)          ; cb e6
	set     4,a             ; cb e7
	set     5,b             ; cb e8
	set     5,c             ; cb e9
	set     5,d             ; cb ea
	set     5,e             ; cb eb
	set     5,h             ; cb ec
	set     5,l             ; cb ed
	set     5,(hl)          ; cb ee
	set     5,a             ; cb ef
	set     6,b             ; cb f0
	set     6,c             ; cb f1
	set     6,d             ; cb f2
	set     6,e             ; cb f3
	set     6,h             ; cb f4
	set     6,l             ; cb f5
	set     6,(hl)          ; cb f6
	set     6,a             ; cb f7
	set     7,b             ; cb f8
	set     7,c             ; cb f9
	set     7,d             ; cb fa
	set     7,e             ; cb fb
	set     7,h             ; cb fc
	set     7,l             ; cb fd
	set     7,(hl)          ; cb fe
	set     7,a             ; cb ff

	call    z,NN            ; cc 34 12
	call    NN              ; cd 34 12
	adc     a,N             ; ce 56
	rst     $8              ; cf
	ret     nc              ; d0
	pop     de              ; d1
	jp      nc,NN           ; d2 34 12
	out     (N),a           ; d3 56
	call    nc,NN           ; d4 34 12
	push    de              ; d5
	sub     N               ; d6 56
	rst     $10             ; d7
	ret     c               ; d8
	exx                     ; d9
	jp      c,NN            ; da 34 12
	in      a,(N)           ; db 56
	call    c,NN            ; dc 34 12

	; DD = IX opcodes
	add     ix,bc           ; dd 09
	add     ix,de           ; dd 19
	ld      ix,NN           ; dd 21 34 12
	ld      (NN),ix         ; dd 22 34 12
	inc     ix              ; dd 23
	inc     ixh             ; dd 24
	dec     ixh             ; dd 25
	ld      ixh,N           ; dd 26 56
	add     ix,ix           ; dd 29
	ld      ix,(NN)         ; dd 2a 34 12
	dec     ix              ; dd 2b
	inc     ixl             ; dd 2c
	dec     ixl             ; dd 2d
	ld      ixl,N           ; dd 2e 56
	inc     (ix+DISP)       ; dd 34 80
	dec     (ix+DISP)       ; dd 35 80
	ld      (ix+DISP),N     ; dd 36 80 56
	add     ix,sp           ; dd 39

	ld      b,ixh           ; dd 44
	ld      b,ixl           ; dd 45
	ld      b,(ix+DISP)     ; dd 46 80
	ld      c,ixh           ; dd 4c
	ld      c,ixl           ; dd 4d
	ld      c,(ix+DISP)     ; dd 4e 80
	ld      d,ixh           ; dd 54
	ld      d,ixl           ; dd 55
	ld      d,(ix+DISP)     ; dd 56 80
	ld      e,ixh           ; dd 5c
	ld      e,ixl           ; dd 5d
	ld      e,(ix+DISP)     ; dd 5e 80

	ld      ixh,b           ; dd 60
	ld      ixh,c           ; dd 61
	ld      ixh,d           ; dd 62
	ld      ixh,e           ; dd 63
	ld      ixh,ixh         ; dd 64
	ld      ixh,ixl         ; dd 65
	ld      h,(ix+DISP)     ; dd 66 80
	ld      ixh,a           ; dd 67

	ld      ixl,b           ; dd 68
	ld      ixl,c           ; dd 69
	ld      ixl,d           ; dd 6a
	ld      ixl,e           ; dd 6b
	ld      ixl,ixh         ; dd 6c
	ld      ixl,ixl         ; dd 6d
	ld      l,(ix+DISP)     ; dd 6e 80
	ld      ixl,a           ; dd 6f

	ld      (ix+DISP),b     ; dd 70 80
	ld      (ix+DISP),c     ; dd 71 80
	ld      (ix+DISP),d     ; dd 72 80
	ld      (ix+DISP),e     ; dd 73 80
	ld      (ix+DISP),h     ; dd 74 80
	ld      (ix+DISP),l     ; dd 75 80
	ld      (ix+DISP),a     ; dd 77 80

	ld      a,ixh           ; dd 7c
	ld      a,ixl           ; dd 7d
	ld      a,(ix+DISP)     ; dd 7e 80

	add     a,ixh           ; dd 84
	add     a,ixl           ; dd 85
	add     a,(ix+DISP)     ; dd 86 80
	adc     a,ixh           ; dd 8c
	adc     a,ixl           ; dd 8d
	adc     a,(ix+DISP)     ; dd 8e 80
	sub     ixh             ; dd 94
	sub     ixl             ; dd 95
	sub     (ix+DISP)       ; dd 96 80
	sbc     a,ixh           ; dd 9c
	sbc     a,ixl           ; dd 9d
	sbc     a,(ix+DISP)     ; dd 9e 80
	and     ixh             ; dd a4
	and     ixl             ; dd a5
	and     (ix+DISP)       ; dd a6 80
	xor     ixh             ; dd ac
	xor     ixl             ; dd ad
	xor     (ix+DISP)       ; dd ae 80
	or      ixh             ; dd b4
	or      ixl             ; dd b5
	or      (ix+DISP)       ; dd b6 80
	cp      ixh             ; dd bc
	cp      ixl             ; dd bd
	cp      (ix+DISP)       ; dd be 80

	; CB entries
	rlc     (ix+DISP)       ; dd cb 80 06
	rrc     (ix+DISP)       ; dd cb 80 0e
	rl      (ix+DISP)       ; dd cb 80 16
	rr      (ix+DISP)       ; dd cb 80 1e
	sla     (ix+DISP)       ; dd cb 80 26
	sra     (ix+DISP)       ; dd cb 80 2e
	sl1     (ix+DISP)       ; dd cb 80 36
	srl     (ix+DISP)       ; dd cb 80 3e

	bit     0,(ix+DISP)     ; dd cb 80 46
	bit     1,(ix+DISP)     ; dd cb 80 4e
	bit     2,(ix+DISP)     ; dd cb 80 56
	bit     3,(ix+DISP)     ; dd cb 80 5e
	bit     4,(ix+DISP)     ; dd cb 80 66
	bit     5,(ix+DISP)     ; dd cb 80 6e
	bit     6,(ix+DISP)     ; dd cb 80 76
	bit     7,(ix+DISP)     ; dd cb 80 7e

	res     0,(ix+DISP)     ; dd cb 80 86
	res     1,(ix+DISP)     ; dd cb 80 8e
	res     2,(ix+DISP)     ; dd cb 80 96
	res     3,(ix+DISP)     ; dd cb 80 9e
	res     4,(ix+DISP)     ; dd cb 80 a6
	res     5,(ix+DISP)     ; dd cb 80 ae
	res     6,(ix+DISP)     ; dd cb 80 b6
	res     7,(ix+DISP)     ; dd cb 80 be

	set     0,(ix+DISP)     ; dd cb 80 c6
	set     1,(ix+DISP)     ; dd cb 80 ce
	set     2,(ix+DISP)     ; dd cb 80 d6
	set     3,(ix+DISP)     ; dd cb 80 de
	set     4,(ix+DISP)     ; dd cb 80 e6
	set     5,(ix+DISP)     ; dd cb 80 ee
	set     6,(ix+DISP)     ; dd cb 80 f6
	set     7,(ix+DISP)     ; dd cb 80 fe

	pop     ix              ; dd e1
	ex      (sp),ix         ; dd e3
	push    ix              ; dd e5
	jp      (ix)            ; dd e9
	ld      sp,ix           ; dd f9

	sbc     a,N             ; de 56
	rst     $18             ; df

	ret     po              ; e0
	pop     hl              ; e1
	jp      po,NN           ; e2 34 12
	ex      (sp),hl         ; e3
	call    po,NN           ; e4 34 12
	push    hl              ; e5
	and     N               ; e6 56
	rst     $20             ; e7
	ret     pe              ; e8
	jp      (hl)            ; e9
	jp      pe,NN           ; ea 34 12
	ex      de,hl           ; eb
	call    pe,NN           ; ec 34 12

	; ED opcodes
	in      b,(c)           ; ed 40
	out     (c),b           ; ed 41
	sbc     hl,bc           ; ed 42
	ld      (NN),bc         ; ed 43 34 12
	neg                     ; ed 44
	retn                    ; ed 45
	im      0               ; ed 46
	ld      i,a             ; ed 47
	in      c,(c)           ; ed 48
	out     (c),c           ; ed 49
	adc     hl,bc           ; ed 4a
	ld      bc,(NN)         ; ed 4b 34 12
	reti                    ; ed 4d
	ld      r,a             ; ed 4f

	in      d,(c)           ; ed 50
	out     (c),d           ; ed 51
	sbc     hl,de           ; ed 52
	ld      (NN),de         ; ed 54 34 12
	im      1               ; ed 56
	ld      a,i             ; ed 57
	in      e,(c)           ; ed 58
	out     (c),e           ; ed 59
	adc     hl,de           ; ed 5a
	ld      de,(NN)         ; ed 5b 34 12
	im      2               ; ed 5e
	ld      a,r             ; ed 5f

	in      h,(c)           ; ed 60
	out     (c),h           ; ed 61
	sbc     hl,hl           ; ed 62
	rrd                     ; ed 67
	in      l,(c)           ; ed 68
	out     (c),l           ; ed 69
	adc     hl,hl           ; ed 6a
	rld                     ; ed 6f

	sbc     hl,sp           ; ed 72
	ld      (NN),sp         ; ed 73 34 12
	in      a,(c)           ; ed 78
	out     (c),a           ; ed 79
	adc     hl,sp           ; ed 7a
	ld      sp,(NN)         ; ed 7b 34 12

	ldi                     ; ed a0
	cpi                     ; ed a1
	ini                     ; ed a2
	outi                    ; ed a3
	ldd                     ; ed a8
	cpd                     ; ed a9
	ind                     ; ed aa
	outd                    ; ed ab

	ldir                    ; ed b0
	cpir                    ; ed b1
	inir                    ; ed b2
	otir                    ; ed b3
	lddr                    ; ed b8
	cpdr                    ; ed b9
	indr                    ; ed ba
	otdr                    ; ed bb

	xor     N               ; ee 56
	rst     $28             ; ef

	ret     p               ; f0
	pop     af              ; f1
	jp      p,NN            ; f2 34 12
	di                      ; f3
	call    p,NN            ; f4 34 12
	push    af              ; f5
	or      N               ; f6 56
	rst     $30             ; f7
	ret     m               ; f8
	ld      sp,hl           ; f9
	jp      m,NN            ; fa 34 12
	ei                      ; fb
	call    m,NN            ; fc 34 12

	; FD = IY opcodes
	add     iy,bc           ; fd 09
	add     iy,de           ; fd 19
	ld      iy,NN           ; fd 21 34 12
	ld      (NN),iy         ; fd 22 34 12
	inc     iy              ; fd 23
	inc     iyh             ; fd 24
	dec     iyh             ; fd 25
	ld      iyh,N           ; fd 26 56
	add     iy,iy           ; fd 29
	ld      iy,(NN)         ; fd 2a 34 12
	dec     iy              ; fd 2b
	inc     iyl             ; fd 2c
	dec     iyl             ; fd 2d
	ld      iyl,N           ; fd 2e 56
	inc     (iy+DISP)       ; fd 34 80
	dec     (iy+DISP)       ; fd 35 80
	ld      (iy+DISP),N     ; fd 36 80 56
	add     iy,sp           ; fd 39

	ld      b,iyh           ; fd 44
	ld      b,iyl           ; fd 45
	ld      b,(iy+DISP)     ; fd 46 80
	ld      c,iyh           ; fd 4c
	ld      c,iyl           ; fd 4d
	ld      c,(iy+DISP)     ; fd 4e 80
	ld      d,iyh           ; fd 54
	ld      d,iyl           ; fd 55
	ld      d,(iy+DISP)     ; fd 56 80
	ld      e,iyh           ; fd 5c
	ld      e,iyl           ; fd 5d
	ld      e,(iy+DISP)     ; fd 5e 80

	ld      iyh,b           ; fd 60
	ld      iyh,c           ; fd 61
	ld      iyh,d           ; fd 62
	ld      iyh,e           ; fd 63
	ld      iyh,iyh         ; fd 64
	ld      iyh,iyl         ; fd 65
	ld      h,(iy+DISP)     ; fd 66 80
	ld      iyh,a           ; fd 67

	ld      iyl,b           ; fd 68
	ld      iyl,c           ; fd 69
	ld      iyl,d           ; fd 6a
	ld      iyl,e           ; fd 6b
	ld      iyl,iyh         ; fd 6c
	ld      iyl,iyl         ; fd 6d
	ld      l,(iy+DISP)     ; fd 6e 80
	ld      iyl,a           ; fd 6f

	ld      (iy+DISP),b     ; fd 70 80
	ld      (iy+DISP),c     ; fd 71 80
	ld      (iy+DISP),d     ; fd 72 80
	ld      (iy+DISP),e     ; fd 73 80
	ld      (iy+DISP),h     ; fd 74 80
	ld      (iy+DISP),l     ; fd 75 80
	ld      (iy+DISP),a     ; fd 77 80

	ld      a,iyh           ; fd 7c
	ld      a,iyl           ; fd 7d
	ld      a,(iy+DISP)     ; fd 7e 80

	add     a,iyh           ; fd 84
	add     a,iyl           ; fd 85
	add     a,(iy+DISP)     ; fd 86 80
	adc     a,iyh           ; fd 8c
	adc     a,iyl           ; fd 8d
	adc     a,(iy+DISP)     ; fd 8e 80
	sub     iyh             ; fd 94
	sub     iyl             ; fd 95
	sub     (iy+DISP)       ; fd 96 80
	sbc     a,iyh           ; fd 9c
	sbc     a,iyl           ; fd 9d
	sbc     a,(iy+DISP)     ; fd 9e 80
	and     iyh             ; fd a4
	and     iyl             ; fd a5
	and     (iy+DISP)       ; fd a6 80
	xor     iyh             ; fd ac
	xor     iyl             ; fd ad
	xor     (iy+DISP)       ; fd ae 80
	or      iyh             ; fd b4
	or      iyl             ; fd b5
	or      (iy+DISP)       ; fd b6 80
	cp      iyh             ; fd bc
	cp      iyl             ; fd bd
	cp      (iy+DISP)       ; fd be 80

	; CB entries
	rlc     (iy+DISP)       ; fd cb 80 06
	rrc     (iy+DISP)       ; fd cb 80 0e
	rl      (iy+DISP)       ; fd cb 80 16
	rr      (iy+DISP)       ; fd cb 80 1e
	sla     (iy+DISP)       ; fd cb 80 26
	sra     (iy+DISP)       ; fd cb 80 2e
	sl1     (iy+DISP)       ; fd cb 80 36
	srl     (iy+DISP)       ; fd cb 80 3e

	bit     0,(iy+DISP)     ; fd cb 80 46
	bit     1,(iy+DISP)     ; fd cb 80 4e
	bit     2,(iy+DISP)     ; fd cb 80 56
	bit     3,(iy+DISP)     ; fd cb 80 5e
	bit     4,(iy+DISP)     ; fd cb 80 66
	bit     5,(iy+DISP)     ; fd cb 80 6e
	bit     6,(iy+DISP)     ; fd cb 80 76
	bit     7,(iy+DISP)     ; fd cb 80 7e

	res     0,(iy+DISP)     ; fd cb 80 86
	res     1,(iy+DISP)     ; fd cb 80 8e
	res     2,(iy+DISP)     ; fd cb 80 96
	res     3,(iy+DISP)     ; fd cb 80 9e
	res     4,(iy+DISP)     ; fd cb 80 a6
	res     5,(iy+DISP)     ; fd cb 80 ae
	res     6,(iy+DISP)     ; fd cb 80 b6
	res     7,(iy+DISP)     ; fd cb 80 be

	set     0,(iy+DISP)     ; fd cb 80 c6
	set     1,(iy+DISP)     ; fd cb 80 ce
	set     2,(iy+DISP)     ; fd cb 80 d6
	set     3,(iy+DISP)     ; fd cb 80 de
	set     4,(iy+DISP)     ; fd cb 80 e6
	set     5,(iy+DISP)     ; fd cb 80 ee
	set     6,(iy+DISP)     ; fd cb 80 f6
	set     7,(iy+DISP)     ; fd cb 80 fe

	pop     iy              ; fd e1
	ex      (sp),iy         ; fd e3
	push    iy              ; fd e5
	jp      (iy)            ; fd e9
	ld      sp,iy           ; fd f9

	cp      N               ; fe 56
	rst     $38             ; ff

; 2) Z80N opcodes

	; ED 20
	swap                    ; ED 23
	mirr                    ; ED 24
	test    N               ; ED 27 56
	bsla    de,b            ; ED 28
	bsra    de,b            ; ED 29
	bsrl    de,b            ; ED 2A
	bsrf    de,b            ; ED 2B
	brlc    de,b            ; ED 2C

	; ED 30
	mul                     ; ED 30 ; fake-ok
	add     hl,a            ; ED 31
	add     de,a            ; ED 32
	add     bc,a            ; ED 33
	add     hl,NN           ; ED 34 34 12
	add     de,NN           ; ED 35 34 12
	add     bc,NN           ; ED 36 34 12

	; ED 80
	push    NN              ; ED 8A 12 34

	; ED 90
	otib                    ; ED 90
	nreg    N,42            ; ED 91 56 2A
	nreg    N,a             ; ED 92 56
	pxdn                    ; ED 93
	pxad                    ; ED 94
	stae                    ; ED 95
	jp      (c)             ; ED 98

	; ED A0
	ldix                    ; ED A4
	ldws                    ; ED A5
	lddx                    ; ED AC

	; ED B0
	lirx                    ; ED B4
	lprx                    ; ED B7
	ldrx                    ; ED BC

; 3) Z80N opcodes - original long mnemonics

	; ED 20
	swapnib                 ; ED 23
	mirror  a               ; ED 24
	test    N               ; ED 27 56
	bsla    de,b            ; ED 28
	bsra    de,b            ; ED 29
	bsrl    de,b            ; ED 2A
	bsrf    de,b            ; ED 2B
	brlc    de,b            ; ED 2C

	; ED 30
	mul                     ; ED 30 ; fake-ok
	add     hl,a            ; ED 31
	add     de,a            ; ED 32
	add     bc,a            ; ED 33
	add     hl,NN           ; ED 34 34 12
	add     de,NN           ; ED 35 34 12
	add     bc,NN           ; ED 36 34 12

	; ED 80
	push    NN              ; ED 8A 12 34

	; ED 90
	outinb                  ; ED 90
	nextreg N,42            ; ED 91 56 2A
	nextreg N,a             ; ED 92 56
	pixeldn                 ; ED 93
	pixelad                 ; ED 94
	setae                   ; ED 95
	jp      (c)             ; ED 98

	; ED A0
	ldix                    ; ED A4
	ldws                    ; ED A5
	lddx                    ; ED AC

	; ED B0
	ldirx                   ; ED B4
	ldpirx                  ; ED B7
	lddrx                   ; ED BC

; 4) various aliases of instructions and arguments

	db      N               ; 56
	defb    N               ; 56
	dc      "AB"            ; 41 C2
	defc    "AB"            ; 41 C2
	ds      2,N             ; 56 56
	defs    2,N             ; 56 56
	dw      NN              ; 34 12
	defw    NN              ; 34 12
	dz      "AB"            ; 41 42 00
	defz    "AB"            ; 41 42 00
	ld      a,ixh           ; DD 7C
	ld      a,xh            ; DD 7C
	ld      a,ixl           ; DD 7D
	ld      a,xl            ; DD 7D
	ld      a,iyh           ; FD 7C
	ld      a,yh            ; FD 7C
	ld      a,iyl           ; FD 7D
	ld      a,yl            ; FD 7D
	mul     de              ; ED 30
	mul     d,e             ; ED 30
	;TODO: BIN vs INCBIN
	;TODO: LOAD vs INCLUDE

End:

	save "/_t_odin.bin",Start,End-Start
