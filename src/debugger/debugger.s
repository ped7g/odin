;;----------------------------------------------------------------------------------------------------------------------
;; Debugger interface
;;
;; RST Interface
;; ~~~~~~~~~~~~~
;;      $00     Return to Odin
;;      $08     esxDOS API
;;      $10     ROM print char routine
;;      $18     Call 48K ROM routine
;;      $20     NextZXOS finish DOT routine
;;      $28     Odin logger
;;      $30     Odin debugger API    
;;      $38     Breakpoint
;;

;;----------------------------------------------------------------------------------------------------------------------
;; RST $00 - Return to Odin or start debugger

                org     0

                jp      DebuggerStart

                ; Used to jump to the program
StartUsersCode:
ProgStart       equ     $+1
                jp      0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; RST $08 - Run the esxDOS API

                org     8

                jp      DebuggerBreak
DbgData         db      0
OdinMMU6        db      0
OdinMMU7        db      0

                org     16

                jp      DebuggerLog
OdinSP          dw      0

                org     24

                ret

                org     32

                ret

                org     40

                ret

                org     48

                ret

                org     56

                ex      (sp),hl
                call    DebuggerAPI
                ex      (sp),hl
                ret

                ds      32,$AA                  ; 32 bytes for stack
DebuggerStack   equ     $

;;----------------------------------------------------------------------------------------------------------------------
;; Register latch
;; Place where all registers are copied
;;

RegAF           dw      0
RegBC           dw      0
RegDE           dw      0
RegHL           dw      0
RegAFA          dw      0
RegBCA          dw      0
RegDEA          dw      0
RegHLA          dw      0
RegIX           dw      0
RegIY           dw      0
RegSP           dw      0
RegI            db      0
RegPC           dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; Debugger initial state as set up by Odin

DebuggerMMU     ds      7       ; The initial MMU state for MMU1-7 for the user's program.
DebuggerPause   db      0
DebuggerMode    db      0       ; 0 = Normal, 1 = DOT, 0xff = NEX (locked out)

;;----------------------------------------------------------------------------------------------------------------------
;; Debugger Modes
;; Describes all 8 MMUs for each mode.
;;

DebuggerModes:
                ; NEX mode
                db      $fe,$fe,$fe,$fe,$fe,$fe,$fe,$fe
                ; Normal mode
                db      $ff,$ff,$0a,$fe,$fe,$fe,$fe,$fe
                ; DOT mode
                db      $ff,$fe,$0a,$0b,$04,$05,$00,$01

;;----------------------------------------------------------------------------------------------------------------------
;; DebuggerStart
;; Start the debugger and run the user's program
;;

DebuggerStart:
                ld      (OdinSP),sp                     ; Store Odin's stack
                ld      sp,DebuggerStack                ; Set SP for users program

                ld      hl,DebuggerEnd
                ld      (1),hl                          ; Update RST 0 vector

                ; Set up the MMU state
                ; cmdExec initialises DebuggerMMU the assembler's current state
                ld      de,DebuggerMMU
                ld      a,(DebuggerMode)                ; ok
                inc     a
                add     a,a
                add     a,a
                add     a,a
                ld      hl,DebuggerModes
                add     hl,a                            ; HL = start of debugger mode info
                inc     hl

                ld      b,7
.l0:
                ld      a,(hl)                          ; A = page #
                inc     hl
                cp      $fe
                jr      nz,.not_fe
.loop
                inc     de
                djnz    .l0
                jr      .end_mmu
.not_fe:
                ld      (de),a
                jr      .loop
.end_mmu:
                ld      hl,DebuggerMMU
                ldi     a,(hl)
                page    1,a
                ldi     a,(hl)
                page    2,a
                ldi     a,(hl)
                page    3,a
                ldi     a,(hl)
                page    4,a
                ldi     a,(hl)
                page    5,a
                ldi     a,(hl)
                page    6,a
                ld      a,(hl)
                page    7,a

                rreg    REG_MMU0
                call    StartUsersCode

                ; Save the registers
DebuggerEnd:
                ld      (RegSP),sp
                ld      sp,RegSP
                push    iy
                push    ix
                exx
                push    hl
                push    de
                push    bc
                exx
                exa
                push    af
                exa
                push    hl
                push    de
                push    bc
                push    af
                ld      a,i
                ld      (RegI),a
                ld      sp,(RegSP)              ; rdlow-ok
                pop     hl                      ; HL = PC
                ld      (RegPC),hl

                ; Return back to Odin
                ld      hl,DebuggerStart
                ld      (1),hl                  ; Restore RST 0 vector to debugger toggle

                ld      a,(OdinMMU6)            ; ok
                page    6,a
                ld      a,(OdinMMU7)            ; ok
                page    7,a
                ld      sp,(OdinSP)             ; ok

                ; Test for PAUSE option
                ld      a,(DebuggerPause)       ; ok
                and     a
                jr      z,.no_pause
                call    waitNoKey
                call    waitAnyKey
.no_pause:

                ; Restore some hardware state
                reg     REG_CLK_SPEED,3         ; Restore clock
                reg     REG_SPRITE_CTL,0        ; Disable sprites
                ret

;;----------------------------------------------------------------------------------------------------------------------

DebuggerEsxDos:
DebuggerPrint:
DebuggerRom48:
DebuggerLog:
DebuggerAPI:
DebuggerBreak:
                rst     0
