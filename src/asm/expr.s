;;----------------------------------------------------------------------------------------------------------------------
;; Expression evaluator
;;----------------------------------------------------------------------------------------------------------------------
;; public API: exprIsNext, exprGet (everything else is private implementation and could change)
;;

ET_OPEN         equ     '('
ET_CLOSE        equ     ')'
ET_PLUS         equ     '+'
ET_MINUS        equ     '-'
ET_OR           equ     '|'
ET_AND          equ     '&'
ET_XOR          equ     '^'
ET_MUL          equ     '*'
ET_DIV          equ     '/'
ET_MOD          equ     OP_MOD
ET_U_INVERT     equ     $80+'~'
ET_U_PLUS       equ     $80+'+'
ET_U_MINUS      equ     $80+'-'
ET_SHL          equ     OP_SHL
ET_SHR          equ     OP_SHR

;;----------------------------------------------------------------------------------------------------------------------
;; Operator table
;;

OpTable:
                db      ET_OPEN,        0   ; 0
                db      ET_CLOSE,       0   ; 1
                db      ET_PLUS,        2   ; 2
                db      ET_MINUS,       2   ; 3
                db      ET_OR,          5   ; 4
                db      ET_AND,         4   ; 5
                db      ET_XOR,         5   ; 6
                db      ET_SHL,         3   ; 7
                db      ET_SHR,         3   ; 8
                db      ET_MUL,         1   ; 9
                db      ET_DIV,         1   ; 10
                db      ET_MOD,         1   ; 11
                db      ET_U_INVERT,    0   ; 12
                db      ET_U_PLUS,      0   ; 13
                db      ET_U_MINUS,     0   ; 14
.SZ:            equ     $-OpTable

OpFuncTable:
                dw      exprPlus
                dw      exprMinus
                dw      exprOr
                dw      exprAnd
                dw      exprXor
                dw      exprShl
                dw      exprShr
                dw      exprMultiply
                dw      exprDivide
                dw      exprMod
                dw      exprUnaryInvert
                dw      exprUnaryPlus
                dw      exprUnaryMinus

;;----------------------------------------------------------------------------------------------------------------------
;; exprGetOpPrecedence
;; gets operator precedence (level) from operator index
;;
;; Input:
;;      A = operator index
;;
;; Output:
;;      A = operator precedence level
;;

exprGetOpPrecedence:
                push    hl
                ld      hl,OpTable+1
                add     a,a
                add     hl,a
                ld      a,(hl)                  ; A = level of input op
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprCheckRoom
;; Check that there's room for adding an operator or value to the input stream.
;;
;; Input:
;;      HL = address of input stream before adding
;;
;; Output:
;;      CF = 1 + error message if no room, A undefined
;;
;; Destroys:
;;      C
;;

exprCheckRoom:
                ld      c,a
                ld      a,4
                add     a,l
                ld      a,c
                ret     nc
                ; display error message (does return with CF=1)
                call    errorPrint
                dz      "EXPRESSION TOO COMPLEX"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprInvalidOpError
;; Called when an error occurs in an expression
;;

exprInvalidOpError:
                call    errorPrint
                dz      "INVALID OPERATOR"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprAddOp
;; Add an operator to the expression input buffer.
;;
;; Input:
;;      A = token
;;      HL = current position in output buffer
;;
;; Output:
;;      HL = updated HL in output buffer
;;      CF = 1 if token not an operator + error message
;;      CF = 1 if there's no room in output buffer + error message
;;

exprAddOp:      ; find index of the token in the OpTable
                push    bc,hl
                ld      hl,OpTable
                ld      bc,OpTable.SZ   ; include also last precedence value to detect invalid op by BC==0
                cpir
                ; did match operator (or precedence value 0..5, but such operator token shouldn't exist)
                ld      a,OpTable.SZ-1
                sub     c
                rra                     ; convert it to operator index 0,1,2... (A = (OpTable.SZ-C-1)>>1)
                pop     hl,bc
                jr      c,exprInvalidOpError    ; token value was 0..5 (invalid) matching precedence value, or no match
                ; check for room in output buffer
                call    exprCheckRoom
                ret     c
.write_index:
                ld      (hl),$fe
                inc     l
                ld      (hl),a
                inc     l
                ld      (hl),0
                inc     l
                ld      (hl),0
                inc     l
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprAddOpIndex
;; Add an operator index to the expression output buffer.
;;
;; Input:
;;      A = operator index
;;      DE = current position in output buffer
;;
;; Output:
;;      DE = updated position in output buffer
;;

exprAddOpIndex:
                ex      de,hl
                call    exprAddOp.write_index
                ex      de,hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprSyntaxError
;; Called when an error occurs in an expression
;;

exprSyntaxError:
                call    errorPrint
                dz      "SYNTAX ERROR IN EXPRESSION"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprIsNext
;; Check token in A to see if it's a valid expression starter.  If it isn't CF = 1
;;
;; Possible expression starting characters:
;;
;;      * Any value
;;      * Any symbol
;;      * Any unary operator: - ~
;;

exprIsNext:
                cp      1
                ret     c               ; $00 -> not expression

                cp      T_EXPR
                ccf
                ret     nc              ; $01-$04 -> expression
                ;  |
                ; fallthrough into exprIsUnaryOp
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; exprIsUnaryOp
;; Check if A is valid unary operator (without displaying error message)
;;
;; CF=0 and ZF=1 when A is ASCII char of unary operator
;; CF=1 and ZF=0 when not
;;
;; (the CF flag is important for exprIsNext functionality, do not optimise it out - w/o changing exprIsNext logic too)
;;

exprIsUnaryOp:
                cp      '-'
                ret     z
                cp      '~'
                ret     z
                cp      '+'
                ret     z
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprGet
;; Parse the input to calculate a 24-bit value
;;
;; Input:
;;      DE = start of expression (within 256-byte aligned buffer).
;;
;; Output:
;;      DE = end of expression
;;      AHL = 24b value of expression
;;      CF = 1 if error occurred
;;
;; Destroys:
;;      BC',DE',HL'
;;

exprGet:
                push    bc,ix
                call    exprGet2
                pop     ix,bc
                ret

exprGet2:
                ld      hl,ExprBuffer           ; HL = start of output buffer (256-byte aligned)
                ld      b,1                     ; B = parentheses count + 1 (+1 for DJNZ exprProcess)

;;----------------------------------------------------------------------------------------------------------------------
;; State 0 of expression syntax: accepts '(' -> state 0, values -> state 1, unary operators -> state 2

expr0:
                ld      a,(de)
                call    exprIsUnaryOp
                jr      c,expr2.with_a          ; If not unary op, then expr0 is identical with expr2
                inc     e
                or      $80
                call    exprAddOp
                ret     c
                ;  |
                ; fallthrough into expr2 (switching state for next char)
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; State 2 of expression syntax: accepts '(' -> state 0, values -> state 1

expr2:
                ld      a,(de)
.with_a:
                and     a                       ; Check EOL
                jr      z,exprSyntaxError
                inc     e

                cp      ET_OPEN
                jr      nz,.check_val

                ;
                ; Handle (
                ;
                inc     b
.add_op:
                call    exprAddOp
                ret     c
                jr      expr0
.check_val:
                ;
                ; Handle values
                ;
                cp      T_EXPR
                jr      nc,exprSyntaxError      ; Not a value, syntax error then
                call    exprAddValue            ; Push value into input stream
                ret     c
                ;  |
                ; fallthrough into expr1 (switching state for next char)
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; State 1 of expression syntax: accepts ')' -> state 1, binary operator -> state 0, terminators -> exprProcess

expr1:
                ld      a,(de)
                and     a
                jr      z,exprProcess           ; Expression syntax check is complete

                ;
                ; Handle ,
                ;
                cp      ','
                jr      z,exprProcess

                ;
                ; Handle )
                ;
                cp      ET_CLOSE
                jr      nz,.not_1_cp
                djnz    .parse_cp
                ; This is a terminating ')' (not part of current expression)
                inc     b
                jr      exprProcess

.parse_cp:
                ; not terminating one, continue with syntax check and --B
                call    exprAddOp
                ret     c
                inc     e
                jr      expr1

.not_1_cp:
                ;
                ; Handle binary operators
                ;
                inc     e
                jr      expr2.add_op

;;----------------------------------------------------------------------------------------------------------------------

exprUnbalanced:
                call    errorPrint
                dz      "UNBALANCED PARENTHESES"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprProcess
;; The syntax checking process has completed.  Now we run the shunting yard algorithm on the input.
;;

exprProcess:
                ld      (exprCalculate.final_de),de
                djnz    exprUnbalanced          ; Check if B is one (balanced parentheses), otherwise error
                                                ; B = 0 amount of operators in stack
                ld      (hl),$ff                ; Mark end
                ld      l,b                     ; HL = ExprBuffer (input buffer)
                ld      de,hl                   ; DE = ExprBuffer (output buffer)
.item_loop:
                ld      a,(hl)
                inc     a
                jr      z,.end_shunt
                inc     a
                jr      z,.shunt_op
                ; value in input stream, write it to output
                ld      c,h                     ; prevent damage to B
                ldi
                ldi
                ldi
                ldi
                jr      .item_loop
.shunt_op:
                ; operator in input stream, shunt operators to output/stack by precedence/rules
                inc     l
                ld      a,(hl)                  ; operator index
                dec     a
                jr      z,.close_p              ; ')'
                inc     a
                jr      z,.open_p               ; '('
                ;
                ; Handle binary and unary operators
                ;
                ; Register allocation:
                ;       (HL) = op
                ;       C = input op level
                ;       B = amount of ops in stack
                ;       DE = Output buffer
                ;       HL = Input buffer+1 (needs +3 before going back to .item_loop)
                ;
                call    exprGetOpPrecedence     ; A = level of op
                ld      c,a
                inc     b                       ; for DJNZ to check for B == 0 upon entry
                jr      .shunt_l_entry
.shunt_loop:
                ; While op stack top is not (
                pop     af                      ; A = top op on op stack
                push    af
                jr      z,.shunt_end_l          ; Is value == '('? (ZF from stack)
                ; Check operator levels
                call    exprGetOpPrecedence     ; A = level of tos op
                cp      c
                jr      c,.pop_op               ; if tos op level < op level (tos higher precedence), pop tos into output
                jr      nz,.shunt_end_l         ; if tos op level > op level (op higher precedence), push it onto stack
                and     a                       ; if levels are equal, usually pop into output except level == 0
                jr      z,.shunt_end_l          ; If tos level == 0, push onto stack
.pop_op:
                pop     af
                call    exprAddOpIndex
.shunt_l_entry:
                djnz    .shunt_loop
.shunt_end_l:
                ; Push the op on the stack
                ld      a,(hl)
.open_p:
                and     a
                push    af                      ; also push ZF=1 when '('
                inc     b
.op_done:
                inc     l
                inc     l
                inc     l
                jr      .item_loop

.close_p:
                ; Handle )
                dec     b
                pop     af                      ; Is value == '('? (ZF from stack)
                jr      z,.op_done              ; Yes, do next item (with parentheses removed)
                call    exprAddOpIndex
                jr      .close_p                ; Pop other operators from stack until matching opening '('

                ; Flush the op stack to the output stream
.flush_loop:
                pop     af                      ; Get operator
                call    exprAddOpIndex
                dec     b                       ; compensate for `inc b` which is for entry logic
.end_shunt:
                inc     b
                djnz    .flush_loop

                ; Terminate the output buffer
                ld      a,$ff
                ld      (de),a                  ; $FF end-marker
                ld      e,b                     ; DE = ExprBuffer
                ;  |
                ; continue with exprCalculate
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; exprCalculate
;; The shunting yard algorithm has completed.  Now calculate the result.
;;
;; Output:
;;      DE = end of expression
;;      AHL = 24b value of expression
;;      CF = 1 if error occurred
;;

exprCalculate:
                ; HL = ExprBuffer = RPN input
                ; DE = ExprBuffer = value stack (growing upward, overwriting processed input)
                ld      hl,de
.calc_loop:
                ld      bc,4                    ; handy immediate
                ld      a,(hl)
                or      a
                jr      z,.calc_value
                inc     a
                jr      z,.calc_done

                ; Perform operation
                inc     l
                ld      a,(hl)
                dec     c
                add     hl,bc                   ; HL advanced at next input
                push    hl
                ld      hl,OpFuncTable-4        ; Adjusted address as () are not included in function table
                add     a,a
                add     hl,a                    ; HL = address of func pointer
                ldhl                            ; HL = func pointer for operator
                callhl                          ; Call operator, DE = value_stack.end(), BC = 3
                pop     hl
                ret     c
                jr      .calc_loop

.calc_value:
                ; Push the value to the stack
                ldir
                jr      .calc_loop

.calc_done:
                ;
                ; Return the 24-bit value AHL
                ;
                call    exprPopAHL              ; also CF=0
.final_de+1:    ld      de,$1234                ; restore DE
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprAddValue
;; Convert value tokens into an actual 24-bit value and store it in the buffer
;;
;; Input:
;;      A = token type
;;      DE = address of token params
;;      HL = current position in output buffer
;;
;; Output:
;;      DE = updated DE after token
;;      HL = updated HL in output buffer
;;      CF = 1 if error occurs
;;

exprAddValue:
                call    exprCheckRoom
                ret     c
                ld      (hl),0
                inc     l
                push    bc
                cp      T_SYMBOL
                jr      nz,.not_sym

                ;
                ; Handle symbol
                ;
                call    symLookUp               ; BC = value of symbol
                jr      c,.no_sym
.write:
                ld      (hl),c
                inc     l
                ld      (hl),b
                inc     l                       ; Add the value
                ld      (hl),0
                inc     l
                pop     bc
                and     a
                ret
.no_sym:
                ld      a,(Pass)
                and     a
                jr      z,.pass0
                pop     bc
                call    errorPrint
                dz      "UNKNOWN SYMBOL"
                ret
.pass0:
                ; Scan to the end of the symbol we didn't find
                ld      a,(de)
                inc     e
                and     a
                jr      nz,.pass0
                ld      b,a
                ld      c,a             ; BC = 0
                jr      .write

.not_sym:
                ;
                ; Handle values
                ;
                cp      T_VALUE
                jr      nz,.not_value
                ld      a,(de)
                inc     e
                ld      c,a
                ld      a,(de)
                inc     e
                ld      b,a
                jr      .write

.not_value:
                ;
                ; Handle strings
                ;
                sub     T_STRING
                jr      nz,.not_string
                ld      b,a
                ld      c,a             ; BC = 0
.l0:
                ld      a,(de)
                inc     e
                and     a
                jr      z,.write
                ld      b,c
                ld      c,a
                jr      .l0

.not_string:
                ;
                ; Handle $
                ;
                ld      bc,(LinePC)
                jr      .write

;;----------------------------------------------------------------------------------------------------------------------
;; Load two top values into DEHL (top of stack), DE'HL' (second value)
;;
;; Output:
;;      DEHL = first value (top of stack)
;;      DE'HL' = second value (one below)
;;
;; Destroys:
;;      DE, IX, A (DE is value stack pointer)
;;

exprPopTwo32b:
                push    de
                pop     ix
                xor     a

                ; Second value
                ld      d,a
                ld      e,(ix-5)
                ld      h,(ix-6)
                ld      l,(ix-7)
                exx

                ; First value
                ld      d,a
                ld      e,(ix-1)
                ld      h,(ix-2)
                ld      l,(ix-3)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Pop two 24-bit values from the 32-bit stack
;;
;; Input:
;;      DE = top of stack
;;
;; Output:
;;      IXL:BC = 24-bit first value (top of stack at DE-4)
;;      A:HL = 24-bit second value (one below at DE-8)
;;      DE-8
;;      CF=0
;;

exprPopTwo24b:
                call    exprPopAHL
                ld      ixl,a
                ld      bc,hl
                ;  |
                ; fallthrough into exprPopAHL
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; exprPopAHL
;; Pop a 24-bit value from the 32-bit stack
;;
;; Input:
;;      DE = top of stack
;;
;; Output:
;;      AHL = 24-bit value
;;      DE-4
;;      CF=0
;;

exprPopAHL:
                ex      de,hl
                dec     l
                ld      a,(hl)
                dec     l
                ld      d,(hl)
                dec     l
                ld      e,(hl)
                dec     l
                ex      de,hl
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Helper subroutines for operator handlers
;;----------------------------------------------------------------------------------------------------------------------

exprNegativeError:
                call    errorPrint
                dz      "POSITIVE VALUE EXPECTED"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Pops AHL from stack, reports error if it is negative, clamps it to 0..24 (for SHR/SHL) into B with +1, pops AHL value
;;

exprShiftInit:
                call    exprPopAHL              ; AHL = shift amount
                ld      b,24+1
                add     a,a
                jr      c,exprNegativeError     ; can't be negative
                or      h
                jr      nz,exprPopAHL           ; AHL = value to shift + return with clamped shift to 24+1
                ld      a,l
                inc     a
                cp      b
                jr      nc,exprPopAHL           ; AHL = value to shift + return with clamped shift to 24+1
                ld      b,a                     ; shift value is 0..23 (+1 adjusted)
                jr      exprPopAHL              ; AHL = value to shift + return

;;----------------------------------------------------------------------------------------------------------------------
;; Expression operator handlers and exprPushAHL
;;
;; Input:
;;      DE = top of stack, BC = 3
;;
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------

exprOr:
                call    exprPopTwo24b
                or      ixl
                ld      (de),a      ; temporary in empty space (was second value)
                ld      a,h
                or      b
                ld      h,a
                ld      a,l
                or      c
                ld      l,a
                ld      a,(de)
                jr      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprAnd:
                call    exprPopTwo24b
                and     ixl
                ld      (de),a      ; temporary in empty space (was second value)
                ld      a,h
                and     b
                ld      h,a
                ld      a,l
                and     c
                ld      l,a
                ld      a,(de)
                jr      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprXor:
                call    exprPopTwo24b
                xor     ixl
                ld      (de),a      ; temporary in empty space (was second value)
                ld      a,h
                xor     b
                ld      h,a
                ld      a,l
                xor     c
                ld      l,a
                ld      a,(de)
                jr      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprMultiply:
                call    exprPopTwo24b           ; IXL:BC = first value (top of stack), A:HL = second value (one below)
                push    de
                ld      e,l
                ld      l,h
                ld      h,a                     ; HL:E = A:HL
                ld      d,c
                ld      c,b
                ld      b,ixl                   ; BC:D = IXL:BC
                call    mul_24_24_24            ; (uint24)HLE = (uint24 HLE)x * (uint24 BCD)y
                ld      a,h
                ld      h,l
                ld      l,e                     ; A:HL = HL:E
                pop     de
                ;  |
                ; fallthrough into exprPushAHL
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; exprPushAHL
;; Push a 24-bit value to the 32-bit stack
;;
;; Input:
;;      AHL = 24-bit value
;;      DE = top of stack
;;
;; Output:
;;      DE+4
;;      CF=0
;;

exprPushAHL:
                ; type byte doesn't matter in the value-stack, only in RPN input-data, just skip it
                inc     e
                ; write 24b value at DE+1 and finish with DE+4, CF=0
                ex      de,hl
                ld      (hl),e
                inc     l
                ld      (hl),d
                inc     l
                ld      (hl),a
                inc     l
                ex      de,hl
exprUnaryPlus:
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------

exprDivide:
                push    de
                call    exprPopTwo32b
                call    divs_32_32_32
.pushResult:
                ld      a,e
                pop     de
                jr      c,.by_zero
                add     de,-8
                jr      exprPushAHL
.by_zero:
                call    errorPrint
                dz      "DIVIDE BY ZERO"
                ret

;;----------------------------------------------------------------------------------------------------------------------

exprMod:
                push    de
                call    exprPopTwo32b
                call    divs_32_32_32
                exx
                jr      exprDivide.pushResult

;;----------------------------------------------------------------------------------------------------------------------

exprPlus:
                call    exprPopTwo24b   ; IXL:BC = first value (top of stack), A:HL = second value (one below)
                add     hl,bc
                adc     a,ixl
                jr      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprMinus:      ; result = value2 - value1
                call    exprPopTwo24b   ; IXL:BC = first value (top of stack), A:HL = second value (one below)
                sbc     hl,bc           ; CF=0 from exprPopTwo24b
                sbc     a,ixl
                jr      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprUnaryInvert:
                ld      hl,exprPushAHL
                push    hl                      ; return through exprPushAHL
.popAHLandInv:
                call    exprPopAHL
                cpl
                ld      c,a
                ld      a,l
                cpl
                ld      l,a
                ld      a,h
                cpl
                ld      h,a
                ld      a,c
                ret

;;----------------------------------------------------------------------------------------------------------------------

exprUnaryMinus:
                call    exprUnaryInvert.popAHLandInv
                ld      c,1                     ; B == 0 from main calculator loop
                add     hl,bc
                adc     a,b                     ; AHL += 1 done
                jr      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprShl:
                call    exprShiftInit
                ret     c
.l0:
                ; Shift the value left
                dec     b
                jr      z,exprPushAHL
                sla     l
                rl      h
                rla
                jr      .l0

;;----------------------------------------------------------------------------------------------------------------------

exprShr:
                call    exprShiftInit
                ret     c
.l0:
                ; Shift the value right
                dec     b
                jr      z,exprPushAHL
                sra     a
                rr      h
                rr      l
                jr      .l0

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

