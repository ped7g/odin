;;----------------------------------------------------------------------------------------------------------------------
;; Table management
;;
;; DATA FORMAT:
;;
;;      TABLE HEADER
;;      ------------
;;      0       2       Previous table or 0
;;      2       2       Symbol for table
;;      4       2       Flags (0=1 if cannot have entries, 1=1 if cannot have tables)
;;
;;      TABLE ENTRY
;;      -----------
;;      0       2       Address
;;      
;;----------------------------------------------------------------------------------------------------------------------

TablePage       db      0               ; 8K page to store table information
CurrentTable    dw      0               ; Address of current table
TableAddr       dw      0               ; Current write position in current table

                STRUCT  TableHeader

Prev            dw      0               ; Address of previous table (or 0)
Symbol          dw      0               ; Symbol handle for table
Flags           db      0               ; Bit 0 = entries?; 1 = tables?
                db      0
                
                ENDS

;;----------------------------------------------------------------------------------------------------------------------
;; tablePageIn
;; Page in the table data

tablePageIn:
                push    af,bc
                rreg    REG_MMU4
                ld      (TableOldPage),a
                ld      a,(TablePage)
                page    4,a
                pop     bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; tablePageOut
;; Page out the table data and restore the previous page.

tablePageOut:
                push    af
TableOldPage+3:
                page    4,0
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; tableClear
;; Reset the memory used by the table system.

tableClear:
                ld      a,(TablePage)
                and     a
                ret     z
                call    asmFree
                ld      hl,0
                ld      (CurrentTable),hl
                ld      (TableAddr),hl
                xor     a
                ld      (TablePage),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; tableStart
;; Start the definition of a table:
;;
;; Input:
;;      DE = null-terminated string containing name symbol
;;
;; Output:
;;      CF = 1 an error occurred
;;

tableStart:
                push    af,bc,hl

                ; Check to see if we have a table already allocated
                ld      a,(TablePage)
                and     a
                jr      nz,.have_table

                ; Initialise the table data structure
                call    asmAlloc
                jr      c,.oom
                ld      (TablePage),a

                ld      hl,$8000
                ld      (TableAddr),hl
                ld      h,0
                ld      (CurrentTable),hl
                call    tablePageIn
                jr      .build_table

.have_table:
                ; We already have a table, which means this is nested
                ; We should check that we haven't added an entry yet
                call    tablePageIn
                push    ix
                ld      ix,(CurrentTable)
                set     1,(ix+TableHeader.Flags)        ; Set container table has having tables
                bit     0,(ix+TableHeader.Flags)        ; But does it already have entries
                pop     ix
                jr      nz,.error_entries
                call    tablePushAddr.internal

.build_table:
                ld      hl,(TableAddr)

                ; Check we have room for the table
                push    de
                ld      de,$a000-TableHeader+1
                call    compare16
                pop     de
                jp      nc,.error_large

                ; Store previous table
                ld      bc,(CurrentTable)
                ldi     (hl),c
                ldd     (hl),b
                ld      (CurrentTable),hl   ; Update current table
                inc     hl
                inc     hl

                ; Store symbol handle
                ld      a,(Pass)
                and     a
                jr      nz,.pass2

                push    de,hl
                ex      de,hl
                call    symCheckSym     ; Manage local labels
                jr      c,.error
                ld      hl,(SymAddr)
                call    symAddNew
                jr      c,.error
                pop     hl
                ldi     (hl),e
                ldi     (hl),d
                pop     de
                jr      .endpass1

.oom:
                call    errorPrint
                dz      "ERROR: OUT OF MEMORY"
.error:
                call    tablePageOut
                pop     hl,bc,af
                scf
                ret

.pass2:
                push    de,hl
                ex      de,hl
                call    symFind
                pop     hl,de
                ldi     (hl),c
                ldi     (hl),b

.endpass1
                ; Store flags
                xor     a
                ldi     (hl),a
                inc     hl

                ; HL = start of table
                ld      (TableAddr),hl
                call    tablePageOut
                pop     hl,bc,af
                ex      de,hl
                call    strEnd
                inc     hl
                ex      de,hl
                ret

.error_entries:
                call    errorPrint
                dz      "ERROR: ADDING TABLE INSIDE TABLE WITH ENTRIES"
                jr      .error
.error_large:
                call    errorPrint
                dz      "ERROR: TABLE TOO LARGE"


;;----------------------------------------------------------------------------------------------------------------------
;; tablePushAddr
;; Store an address in the current table
;;
;; OUTPUT:
;;      CF = 1 an error occurred
;;

tablePushAddr:
                push    af

                ; Check to see if we're in a table
                ld      a,(TablePage)
                and     a
                jr      z,.err_notable

                ; Check to see if we don't have any tables already inside this one
                call    tablePageIn
                push    ix
                ld      ix,(CurrentTable)
                bit     1,(ix+TableHeader.Flags)
                jr      nz,.err_havetables

                call    .internal

                ld      ix,(CurrentTable)
                set     0,(ix+TableHeader.Flags)
                call    tablePageOut
                pop     ix,af
                and     a
                ret
.err_notable:
                call    errorPrint
                dz      "ERROR: ENTRY OUTSIDE TABLE"
                pop     af
                scf
                ret
.err_havetables:
                call    errorPrint
                dz      "ERROR: ENTRY AND TABLE WITHIN TABLE"
                pop     ix,af
                scf
                ret

.internal:
                push    af,de,hl

                ld      hl,(TableAddr)
                ld      de,$a000
                call    compare16
                jr      z,.error

                ld      de,(PC)
                ldi     (hl),e
                ldi     (hl),d
                ld      (TableAddr),hl

                pop     hl,de,af
                and     a
                ret
.error:
                call    errorPrint
                dz      "ERROR: TOO MANY ENTRIES"
                pop     hl,de,af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; tableEnd
;; Write the addresses stored in the table page.
;;
;; OUTPUT:
;;      CF = 1 if error emitting words
;;
;; AFFECTS:
;;      AF,BC,DE,HL
;;

tableEnd:
                call    tablePageIn
                ld      hl,(CurrentTable)
                
                ; Set the address to the symbol
                inc     hl
                inc     hl
                ldi     e,(hl)
                ld      d,(hl)      ; DE = symbol
                call    symGet      ; DE = address of symbol data
                add     de,SymInfo.Value
                ld      a,(PC)
                ldi     (de),a
                ld      a,(PC+1)
                ld      (de),a      ; Store the current PC value for this symbol

                ; Write addresses from (CurrentTable)+TableHeader to (CurrentAddr)
                add     hl,TableHeader-3
                ld      de,(TableAddr)
                jr      .endloop

.loop:
                ldi     a,(hl)
                call    emitByte
                ret     c

                ldi     a,(hl)
                call    emitByte
                ret     c

.endloop:
                call    compare16
                jr      nz,.loop

                ; Delete current table
                ld      hl,(CurrentTable)
                ldi     e,(hl)
                ldd     d,(hl)              ; DE = previous table
                ld      a,d
                or      e
                jr      z,.last_table

                ld      (TableAddr),hl
                ld      (CurrentTable),de
                and     a
                jp      tablePageOut
.last_table:
                ; Delete the table page
                ld      a,(TablePage)
                call    asmFree
                xor     a
                ld      (TablePage),a
                jp      tablePageOut


                