;;----------------------------------------------------------------------------------------------------------------------
; console.s, memory.s tests
; Some of the tests here are manual and needs to be enabled and checked visually by user

; uncomment to run the manual+visual test of print routines (requires visual check of result by user)
;         DEFINE __T_CONSOLE_DO_MANUAL_TEST

__test_console_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Console: "

                ; TEST at
                __T_SETUP 'a'
                call    __t_con_preserve_at     ; preserve current "at" into HL' + DE'
                ld      bc,0
                call    at
                ld      bc,(CurrentCoords)
                call    __t_con_restore_at
                __T_CHECK flags_hl_bc : dw $4000, 0
                __T_SETUP '2'
                call    __t_con_preserve_at
                ld      bc,79
                call    at
                ld      bc,(CurrentCoords)
                call    __t_con_restore_at
                __T_CHECK flags_hl_bc : dw $4000+79*2, 79
                __T_SETUP '3'
                call    __t_con_preserve_at
                ld      bc,$0a10
                call    at
                ld      bc,(CurrentCoords)
                call    __t_con_restore_at
                __T_CHECK flags_hl_bc : dw $4000+10*160+16*2, $0a10

                ; TEST calcAddr
                __T_SETUP 'c'
                ld      bc,$0307
                call    calcAddr
                __T_CHECK flags_hl_bc : dw $4000+3*160+7*2, $0307

                ; TEST setColour
                __T_SETUP 'C'
                ld      a,%0101'0101
                call    setColour
                __T_CHECK a : db %1010'1010
                __T_SETUP '2'
                ld      a,%0011'1100
                call    setColour
                ld      a,(CurrentColour)
                __T_CHECK a : db %0111'1000

                ; TEST moreRoom
                __T_SETUP '+'
                ld      hl,__t_con_tiles1+3*2   ; move "dD" to nowhere (will look as no-op)
                ld      c,79
                call    moreRoom
                __T_MEMCMP __t_con_tiles1, "aAbBcCdDeE"
                __T_CHECK flags_hl_bc : dw __t_con_tiles1+3*2, (__t_default_bc&$FF00)|79
                __T_SETUP '2'
                ld      hl,__t_con_tiles1+1*2   ; move "bBcC" over "cCdD"
                ld      c,77
                call    moreRoom
                __T_MEMCMP __t_con_tiles1, "aAbBbBcCeE"
                __T_CHECK flags_hl_bc : dw __t_con_tiles1+1*2, (__t_default_bc&$FF00)|77

                ; TEST lessRoom
                __T_SETUP '-'
                ld      hl,__t_con_tiles1+1*2   ; move new "bBcC" to old "bB" position
                ld      c,77
                call    lessRoom
                __T_MEMCMP __t_con_tiles1, "aAbBcC\0\0eE"
                __T_CHECK flags_hl_bc : dw __t_con_tiles1+1*2, (__t_default_bc&$FF00)|77
                __T_SETUP '2'
                ld      hl,__t_con_tiles1+1*2   ; move "bB" to nowhere (replaced by zero)
                ld      c,79
                call    lessRoom
                __T_MEMCMP __t_con_tiles1, "aA\0\0cC\0\0eE"
                __T_CHECK flags_hl_bc : dw __t_con_tiles1+1*2, (__t_default_bc&$FF00)|79
                ; test printHLWidth with B == 0 (should fail gracefully)
                __T_SETUP 'p'
                ld      b,0
                ld      hl,__t_con_0chTxt
                call    printHLWidth
                __T_CHECK flags_hl_bc : dw __t_con_0chTxt.e, low __t_default_bc

        IFDEF __T_CONSOLE_DO_MANUAL_TEST
                ld a,5 : call colouredPrint : dz "  \"visual\" tests following:",C_ENTER,"X"
                ; test errorPrint (undefine + define back to the test runner stub)
                undefine errorPrint : call errorPrint : dz ".X. I am errorPrint" : define errorPrint __testErrorPrint
                ld a,6 : call colouredPrint : dz "^",C_UP,"v",C_DOWN,"<",C_LEFT,C_LEFT,">",C_RIGHT,"*d1",C_BACKSPACE,"D2.",C_LEFT,C_LEFT,C_DELETE,C_END,"!   == \"^><*dD.!\" and \"v\" above. 80 char limit follows (ends F): "
                ld a,3 : call colouredPrint : dz "0123456789ABCDEF",C_ENTER
                ; test the clamp on 80 non-space characters, the output should end with ..EF
                ld a,5 : call colouredPrint : db " ............... ",C_HOME," ::::::::::::::: ",C_HOME," ;;;;;;;;;;;;;;; ",C_HOME," %%%%%%%%%%%%%%% ",C_HOME," 0123456789ABCDEF"
                    ; this is part of the string for previous colouredPrint command, and should be NOT displayed
                    ASSERT (colouredPrint&$FF00) && (colouredPrint&$00FF)   ; assume the "colouredPrint" address does not contain zero byte
                    ld a,2 : call colouredPrint : dz "WRONG HL!"
                ; test few additional control codes
                ld a,3 : call colouredPrint : dz C_HOME,'.',C_END,". cBAD",C_LEFT,C_LEFT,C_LEFT,C_CLEARTOEND
                ; test refresh of 80 char limit after C_ENTER control char
                ld a,6 : call colouredPrint : dz C_ENTER,"testing refresh of 80 char limit after C_ENTER, should end with 9: ...0123456789"
                ;                                         01234567890123456789012345678901234567890123456789012345678901234567890123456789 = 80 chars
        ELSE
                ld a,5 : call colouredPrint : dz "  \"visual\" tests skipped (enable in 't_console.s')"
        ENDIF

                ;;-----------------------------------------------------------------
                ;; memory.s - couple of tests
                ld a,7 : call colouredPrint : dz C_ENTER,"Memory: "

                ; TEST memfill
                __T_SETUP 'f'
                ld      hl,__t_con_tiles1
                ld      bc,7
                ld      a,'!'
                call    memfill
                ld      hl,__t_con_tiles1+1
                ld      bc,5
                ld      a,'.'
                call    memfill
                __T_MEMCMP __t_con_tiles1, "!.....!"
                __T_CHECK flags_a_hl_bc : db '.' : dw __t_con_tiles1+1, 5

                ; TEST memclear
                __T_SETUP 'z'
                ld      hl,__t_con_tiles1+2
                ld      bc,3
                call    memclear
                __T_MEMCMP __t_con_tiles1, "!.\0\0\0.!"
                __T_CHECK a_hl_bc : db 0 : dw __t_con_tiles1+2, 3

                ; TEST memcpy
                __T_SETUP 'c'
                ld      hl,__t_con_0chTxt
                ld      de,__t_con_tiles1+1
                ld      bc,5
                call    memcpy
                __T_MEMCMP __t_con_tiles1, "!ERROR!"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+1, __t_con_0chTxt, 5
                __T_SETUP '2'
                ld      hl,__t_con_tiles1+1
                ld      de,__t_con_tiles1+2
                ld      bc,2
                call    memcpy
                __T_MEMCMP __t_con_tiles1, "!EEEOR!"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+2, __t_con_tiles1+1, 2

                ; TEST memcpy_r
                __T_SETUP 'r'
                ld      hl,__t_con_tiles1+3
                ld      de,__t_con_tiles1+2
                ld      bc,2
                call    memcpy_r
                __T_MEMCMP __t_con_tiles1, "!EOOOR!"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+2, __t_con_tiles1+3, 2
                __T_SETUP '2'
                ld      hl,__t_con_0chTxt
                ld      de,__t_con_tiles1+1
                ld      bc,5
                call    memcpy_r
                __T_MEMCMP __t_con_tiles1, "!ERROR!"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+1, __t_con_0chTxt, 5

                ; TEST memmove
                __T_SETUP 'm'
                ld      hl,__t_con_tiles1+1
                ld      de,__t_con_tiles1+2
                ld      bc,4
                call    memmove
                __T_MEMCMP __t_con_tiles1, "!EERRO!"
                __T_CHECK cf_de_hl_bc : dw __t_con_tiles1+2, __t_con_tiles1+1, 4
                __T_SETUP '2'
                ld      hl,__t_con_tiles1+2
                ld      de,__t_con_tiles1+1
                ld      bc,4
                call    memmove
                __T_MEMCMP __t_con_tiles1, "!ERROO!"
                __T_CHECK ncf_de_hl_bc : dw __t_con_tiles1+1, __t_con_tiles1+2, 4

                ; TEST memswap
                __T_SETUP 's'
                ld      hl,__t_con_tiles1+1
                ld      de,__t_con_tiles2+1
                ld      bc,5
                call    memswap
                __T_MEMCMP __t_con_tiles1, "!01234!"
                __T_MEMCMP __t_con_tiles2, "#ERROO#"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles2+1, __t_con_tiles1+1, 5
                __T_SETUP '2'
                ld      hl,__t_con_tiles1+1
                ld      de,__t_con_tiles1+2
                ld      bc,4
                call    memswap
                __T_MEMCMP __t_con_tiles1, "!12340!"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+2, __t_con_tiles1+1, 4
                __T_SETUP '3'
                ld      hl,__t_con_tiles1+2
                ld      de,__t_con_tiles1+1
                ld      bc,4
                call    memswap
                __T_MEMCMP __t_con_tiles1, "!23401!"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+1, __t_con_tiles1+2, 4
                __T_SETUP '4'
                ld      hl,__t_con_tiles1+1
                ld      de,__t_con_tiles1+3
                ld      bc,4
                call    memswap
                __T_MEMCMP __t_con_tiles1, "!401!23"
                __T_CHECK flags_de_hl_bc : dw __t_con_tiles1+3, __t_con_tiles1+1, 4

                ; TEST insertSpace
                __T_SETUP 'i'
                ld      hl,__t_con_tiles1+2
                ld      de,__t_con_tiles1+6
                ld      bc,2
                call    insertSpace
                __T_MEMCMP __t_con_tiles1, "!401013"
                __T_CHECK ncf_de_hl_bc : dw __t_con_tiles1+6, __t_con_tiles1+2, 2
                __T_SETUP '2'
                ld      hl,__t_con_tiles1+2
                ld      de,__t_con_tiles1+6
                ld      bc,4
                call    insertSpace     ; insert 4 means till end (zero bytes moved)
                __T_MEMCMP __t_con_tiles1, "!401013"
                __T_CHECK ncf_de_hl_bc : dw __t_con_tiles1+6, __t_con_tiles1+2, 4
                __T_SETUP '3'
                ld      hl,__t_con_tiles1+2
                ld      de,__t_con_tiles1+6
                ld      bc,5
                call    insertSpace     ; insert 5 is too much (nothing gets modified, CF=1)
                __T_MEMCMP __t_con_tiles1, "!401013"
                __T_CHECK cf_de_hl_bc : dw __t_con_tiles1+6, __t_con_tiles1+2, 5

                ; TEST removeSpace
                __T_SETUP 'e'
                ld      hl,__t_con_tiles2+2 ; current buffer is "#ERROO#"
                ld      de,__t_con_tiles2+6
                ld      bc,2
                call    removeSpace
                __T_MEMCMP __t_con_tiles2, "#EOOOO#"
                __T_CHECK ncf_de_hl_bc : dw __t_con_tiles2+6, __t_con_tiles2+2, 2
                __T_SETUP '2'
                ld      hl,__t_con_tiles2+1
                ld      de,__t_con_tiles2+6
                ld      bc,5
                call    removeSpace     ; removing 5 means nothing from end to copy
                __T_MEMCMP __t_con_tiles2, "#EOOOO#"
                __T_CHECK ncf_de_hl_bc : dw __t_con_tiles2+6, __t_con_tiles2+1, 5
                __T_SETUP '3'
                ld      hl,__t_con_tiles2+1
                ld      de,__t_con_tiles2+6
                ld      bc,6
                call    removeSpace     ; removing 6 is too much (nothing gets modified, CF=1)
                __T_MEMCMP __t_con_tiles2, "#EOOOO#"
                __T_CHECK cf_de_hl_bc : dw __t_con_tiles2+6, __t_con_tiles2+1, 6

                ; TEST memcmp
                __T_SETUP 'M'
                ld      hl,__t_mem_txt1
                ld      de,__t_mem_txt1
                ld      bc,4
                call    memcmp
                add     bc,__t_default_bc-4
                __T_CHECK zf_a_hl_de : db '4' : dw __t_mem_txt1, __t_mem_txt1
                __T_SETUP '.'
                ld      hl,__t_con_tiles2
                ld      de,__t_mem_txt1
                ld      bc,0
                call    memcmp
                add     bc,__t_default_bc
                __T_CHECK zf_a_hl_de : db 0 : dw __t_con_tiles2, __t_mem_txt1
                __T_SETUP '2'
                ld      hl,__t_mem_txt1
                ld      de,__t_mem_txt2
                ld      bc,3
                call    memcmp
                add     bc,__t_default_bc-3
                __T_CHECK zf_a_hl_de : db '3' : dw __t_mem_txt1, __t_mem_txt2
                __T_SETUP '.'
                ld      hl,__t_mem_txt1
                ld      de,__t_mem_txt2
                ld      bc,4
                call    memcmp
                add     bc,__t_default_bc-4
                __T_CHECK nzf_a_hl_de : db '5' : dw __t_mem_txt1, __t_mem_txt2
                __T_SETUP '3'
                ld      hl,__t_mem_txt2
                ld      de,__t_mem_txt1
                ld      bc,3
                call    memcmp
                add     bc,__t_default_bc-3
                __T_CHECK zf_a_hl_de : db '3' : dw __t_mem_txt2, __t_mem_txt1
                __T_SETUP '.'
                ld      hl,__t_mem_txt2
                ld      de,__t_mem_txt1
                ld      bc,4
                call    memcmp
                add     bc,__t_default_bc-4
                __T_CHECK nzf_a_hl_de : db '4' : dw __t_mem_txt2, __t_mem_txt1

                ; TEST allocPages
                __T_SETUP 'P'
                ld      bc,$0133    ; "33" is just filler for easy check of C preserved
                ld      hl,__t_alloc_pg
                call    allocPages
                ld      a,__t_default_a
                __T_WORDCMP __t_alloc_pg, $CC20
                __T_CHECK ncf_hl_bc : dw __t_alloc_pg, $0133
                __T_SETUP '.'
                ld      bc,$0333    ; "33" is just filler for easy check of C preserved
                ld      hl,__t_alloc_pg+1
                call    allocPages
                ld      a,__t_default_a
                __T_MEMCMP __t_alloc_pg, <$20, $21, $22, $23, $CC>
                __T_CHECK ncf_hl_bc : dw __t_alloc_pg+1, $0333
                __T_SETUP '2'       ; fail by requiring one more page than available
                ld      bc,$1D33    ; "33" is just filler for easy check of C preserved
                ld      hl,__t_alloc_pg+4
                call    allocPages
                ld      a,__t_default_a
                __T_CHECK cf_hl_bc : dw __t_alloc_pg+4, $1D33
                __T_SETUP '.'       ; check if partially released pages were deallocated
                call    allocPage
                __T_CHECK ncf_a : db $24

                ; TEST freePages
                xor     a
                ld      (__t_alloc_pg+3),a  ; put null-marker instead of page $23
                ; (memory here looks `20 21 22 00 24 25 ....`, $20-24 are legit allocated pages)
                __T_SETUP 'F'
                ld      bc,$0133    ; "33" is just filler for easy check of C preserved
                ld      hl,__t_alloc_pg+4   ; release the sole $24
                call    freePages
                ld      a,__t_default_a
                __T_CHECK ncf_hl_bc : dw __t_alloc_pg+4, $0133
                __T_SETUP '.'
                ld      bc,$0233
                ld      hl,__t_alloc_pg     ; release the two $20,$21
                call    freePages
                ld      a,__t_default_a
                __T_CHECK ncf_hl_bc : dw __t_alloc_pg, $0233
                __T_SETUP '2'
                ld      bc,$0233
                ld      hl,__t_alloc_pg+2   ; try to release the two $22,$00 pair (CF=1 expected, only $22 released)
                call    freePages
                ld      a,__t_default_a
                __T_CHECK cf_hl_bc : dw __t_alloc_pg+2, $0233

                ret

__t_con_preserve_at:
                exx
                ld      hl,(CurrentCoords)
                ld      de,(CurrentPos)
                exx
                ret

__t_con_restore_at:
                exx
                ld      (CurrentCoords),hl
                ld      (CurrentPos),de
                exx
                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_con_tiles1: db      "aAbBcCdDeE"

__t_con_0chTxt: dz      "ERROR"     ; should never display
.e:

__t_con_tiles2: db      "#01234#"

__t_mem_txt1:   db      "1234"
__t_mem_txt2:   db      "1235"

__t_alloc_pg:   ds      34, $CC

                display "[TEST RUN] tests: console.s                    Space: ",/A,$-__test_console_s
