;;----------------------------------------------------------------------------------------------------------------------
; expr.s tests - part 2
; coverage: exprGet, exprIsUnaryOp

__test_expr_s_2:
                ld a,7 : call colouredPrint : dz C_ENTER,"Expr (2): "
                ; page in assembler module
                ld      a,(AsmCode)
                page    7,a

                ; TEST exprGet - extra tests which didn't fit into suite 2
                __T_SETUP 'z'
                ld      de,AsmBuffer        ; divide by zero
                __T_MEMCPY AsmBuffer, <T_VALUE,3,0,'/',T_VALUE,0,0,0>
                call    exprGet
                __T_CHECK errorPrintCnt1
                __T_SETUP '.'
                ld      de,AsmBuffer        ; divide by zero with modulo
                __T_MEMCPY AsmBuffer, <T_VALUE,3,0,OP_MOD,T_VALUE,0,0,0>
                call    exprGet
                __T_CHECK errorPrintCnt1

                ; TEST exprGet - chained unary operators were removed (Matt insists it's syntax error)
                __T_SETUP 'U'               ; +-~3 ; 2b 2d 7e 02 03 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'+', '-', '~', T_VALUE, 3, 0, 0>
                call    exprGet
                __T_CHECK errorPrintCnt1
                ; workaround to evaluate them is to use parentheses
                __T_SETUP '.'               ; +(-(~3)) ; 2b 28 2d 28 7e 02 03 00 29 29 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'+','(','-','(','~',T_VALUE,3,0,')',')',0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db +-~3>>16 : dw +-~3, AsmBuffer+10 : ASSERT 4 == +-~3

                ; TEST exprIsUnaryOp - private API
__t_expr_is_unary:
                __T_SETUP 'u'
                ld      bc,0
                ld      a,'+'
                call    exprIsUnaryOp
                adc     hl,bc
                ld      a,'-'
                call    exprIsUnaryOp
                adc     hl,bc
                ld      a,'~'
                call    exprIsUnaryOp
                adc     hl,bc
                xor     a
.l0:
                call    exprIsUnaryOp
                adc     hl,bc
                inc     a
                jr      nz,.l0
                __T_CHECK a_hl_bc : db 0 : dw __t_default_hl+256-3, 0

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

                display "[TEST RUN] tests: expr.s (2)                   Space: ",/A,$-__test_expr_s_2
