;;----------------------------------------------------------------------------------------------------------------------
; maths.s tests
; coverage: unpackD24

__test_maths_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Maths: "

                ; TEST unpackD24
                __T_SETUP 'u'
                ld      de,$AA00
                ld      hl,$0000
                call    unpackD24   ; EHL -> DEHL, affects AF,DE,HL
                ld      a,__t_default_a
                __T_CHECK de_hl : defw $0000, $0000
                __T_SETUP '.'
                ld      de,$AA12
                ld      hl,$3456
                call    unpackD24   ; EHL -> DEHL, affects AF,DE,HL
                ld      a,__t_default_a
                __T_CHECK de_hl : defw $0119, $3046
                __T_SETUP ':'
                ld      de,$AA00
                ld      hl,$0001
                call    unpackD24   ; EHL -> DEHL, affects AF,DE,HL
                ld      a,__t_default_a
                __T_CHECK de_hl : defw $0000, $0001
                __T_SETUP '.'
                ld      de,$AAFE
                ld      hl,$DCBA
                call    unpackD24   ; EHL -> DEHL, affects AF,DE,HL
                ld      a,__t_default_a
                __T_CHECK de_hl : defw $1670, $2650

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

                display "[TEST RUN] tests: maths.s                      Space: ",/A,$-__test_maths_s
