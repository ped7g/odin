;;----------------------------------------------------------------------------------------------------------------------
; symbols.s tests
; (symDone not covered, BTW symDone should probably do symInit at end?)

__test_symbols_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Symbols: "
                ; page in assembler module
                ld      a,(AsmCode)
                page    7,a

                ; symbols init call (green "i" when survived)
                __T_SETUP 'i'
                call    symInit
                __T_CHECK none

                ; TEST symGetLength
                __T_SETUP 'l'
                ld      hl,__t_sym_s1
                call    symGetLength
                ld      a,__t_default_a
                __T_CHECK cf_hl_bc : dw __t_sym_s1,1
                __T_SETUP '2'
                ld      hl,__t_sym_s2
                call    symGetLength
                ld      a,__t_default_a
                __T_CHECK cf_hl_bc : dw __t_sym_s2,26
                __T_SETUP '3'
                ld      hl,__t_sym_s3
                call    symGetLength
                ld      a,__t_default_a
                __T_CHECK ncf_hl_bc : dw __t_sym_s3,27

                ; TEST symFind (with empty symbol table)
                __T_SETUP 'f'
                ld      hl,__t_sym_s1
                call    symFind
                __T_CHECK only_ncf
                __T_SETUP '2'
                ld      hl,__t_sym_s3
                call    symFind
                __T_CHECK only_ncf

                ; TEST symAdd
                __T_SETUP 'a'
                ld      hl,__t_sym_s3
                call    symAdd
                __T_CHECK errorPrintCnt1
                __T_SETUP '2'
                ld      hl,__t_sym_s1
                call    symAdd
                __T_CHECK only_de : dw $0000
                __T_SETUP '3'
                ld      hl,__t_sym_s1
                call    symAdd      ; symAdd is not protected against adding same name, so this should work
                __T_CHECK only_de : dw $0001
                ; adding 510 symbols to check also paging of symbol table (should end with 512 total symbols)
                __T_SETUP '4'
                ld      bc,$FE02
.t_symAdd3_l:
                push    bc
                ld      hl,__t_sym_s4
                call    symAdd      ; symAdd is not protected against adding same name, so this should work
                ; modify test label by increasing number (l_000, l_001, l_002, ..., l_509)
                ld      hl,__t_sym_s4+4
.t_symAdd3_chg:
                inc     (hl)
                ld      a,'0'+10
                cp      (hl)
                jr      nz,.t_symAdd3_nxt
                ld      (hl),'0'
                dec     hl
                jr      .t_symAdd3_chg
.t_symAdd3_nxt:
                pop     bc
                djnz    .t_symAdd3_l
                dec     c
                jr      nz,.t_symAdd3_l
                __T_CHECK only_de : dw $01FF

                ; TEST symGet - (testing instead of mapped MMU0 the value in A, which is current implementation detail)
                __T_SETUP 'g'
                ld      de,$0000
                call    symGet
                __T_CHECK a_de : db $20 : dw $0000
                __T_SETUP '2'
                ld      de,$01FF
                call    symGet
                __T_CHECK a_de : db $21 : dw $1FE0
                __T_SETUP '3'
                ld      de,$0180
                call    symGet
                __T_CHECK a_de : db $21 : dw $1000

                ; TEST symFind - with non-empty symbol table
                __T_SETUP 'F'
                ld      hl,__t_sym_s2   ; not in table
                call    symFind
                __T_CHECK only_ncf
                __T_SETUP '2'
                ld      hl,__t_sym_s3   ; too long
                call    symFind
                __T_CHECK only_ncf
                __T_SETUP '3'
                ld      hl,__t_sym_l_00 ; not in table (substring of label which *is* in table)
                call    symFind
                __T_CHECK only_ncf
                __T_SETUP '4'
                ld      hl,__t_sym_l_250; in table at position 252
                call    symFind
                __T_CHECK only_cf_de_hl_bc : dw $1F80, __t_sym_l_250, $00FC
                    ; in ideal unit-testing-world ^ this should check string at DE, not DE itself (and similarly with handle BC)
                __T_SETUP '5'
                ld      hl,__t_sym_l_254; in table at position 256
                call    symFind
                __T_CHECK only_cf_de_hl_bc : dw $0000, __t_sym_l_254, $0100

                ; TEST symLookUp - with non-empty symbol table
                __T_SETUP 'u'
                ld      de,__t_sym_s2   ; not in table
                call    symLookUp
                __T_CHECK only_cf_de_hl : dw __t_sym_s2, __t_default_hl
                __T_SETUP '2'
                ld      de,__t_sym_s3   ; too long
                call    symLookUp
                __T_CHECK only_cf_de_hl : dw __t_sym_s3, __t_default_hl
                __T_SETUP '3'
                ld      de,__t_sym_l_00 ; not in table (substring of label which *is* in table)
                call    symLookUp
                __T_CHECK only_cf_de_hl : dw __t_sym_l_00, __t_default_hl
                __T_SETUP '4'
                ld      de,__t_sym_l_250; in table at position 252
                call    symLookUp
                __T_CHECK only_ncf_de_hl_bc : dw __t_sym_l_250+6, __t_default_hl, $0000
                __T_SETUP '5'
                ld      de,__t_sym_l_254; in table at position 256
                call    symLookUp
                __T_CHECK only_ncf_de_hl_bc : dw __t_sym_l_254+6, __t_default_hl, $0000
                call    .changeSymbolValues ; l_250 = $1234, l_254 = $5678 (depends on symFind routine)
                __T_SETUP '6'
                ld      de,__t_sym_l_250; in table at position 252
                call    symLookUp
                __T_CHECK only_ncf_de_hl_bc : dw __t_sym_l_250+6, __t_default_hl, $1234
                __T_SETUP '7'
                ld      de,__t_sym_l_254; in table at position 256
                call    symLookUp
                __T_CHECK only_ncf_de_hl_bc : dw __t_sym_l_254+6, __t_default_hl, $5678

                ret

.changeSymbolValues:
                ld hl,__t_sym_l_250 : call symFind : ld hl,$1234 : call c,.writeValue
                ld hl,__t_sym_l_254 : call symFind : ld hl,$5678 : call c,.writeValue
                ret
.writeValue:    ; DE = symbol address into MMU0 (must be mapped)
                ld a,SymInfo.Value : add de,a : ex de,hl
                ldi (hl),e : ld (hl),d
                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_sym_s1:     dz      "s"
__t_sym_s2:     dz      "s2345678901234567890123456"
__t_sym_s3:     dz      "s23456789012345678901234567"   ; 27 length (over max limit)
__t_sym_s4:     dz      "l_000"
__t_sym_l_00:   dz      "l_00"
__t_sym_l_250:  dz      "l_250"
__t_sym_l_254:  dz      "l_254"

                display "[TEST RUN] tests: symbols.s                    Space: ",/A,$-__test_symbols_s
