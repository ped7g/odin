;;----------------------------------------------------------------------------------------------------------------------
; utils.s tests

__test_utils_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Utils: "

                ; TEST max
                __T_SETUP 'm'
                ld      hl,1234
                ld      de,1234
                call    max
                __T_CHECK zf_ncf_de_hl : dw 1234, 1234
                __T_SETUP '2'
                ld      hl,$FFFF
                ld      de,$FFFE
                call    max
                __T_CHECK nzf_ncf_de_hl : dw $FFFE, $FFFF
                __T_SETUP '3'
                ld      hl,$FFFE
                ld      de,$FFFF
                call    max
                __T_CHECK nzf_cf_de_hl : dw $FFFE, $FFFF
                __T_SETUP '4'
                ld      hl,1
                ld      de,2
                call    max
                __T_CHECK nzf_cf_de_hl : dw 1, 2
                __T_SETUP '5'
                ld      hl,2
                ld      de,0
                call    max
                __T_CHECK nzf_ncf_de_hl : dw 0, 2

                ; TEST compare16
                __T_SETUP 'c'
                ld      hl,1234
                ld      de,1234
                call    compare16
                __T_CHECK zf_ncf_de_hl : dw 1234, 1234
                __T_SETUP '2'
                ld      hl,$FFFF
                ld      de,$FFFE
                call    compare16
                __T_CHECK nzf_ncf_de_hl : dw $FFFE, $FFFF
                __T_SETUP '3'
                ld      hl,$FFFE
                ld      de,$FFFF
                call    compare16
                __T_CHECK nzf_cf_de_hl : dw $FFFF, $FFFE
                __T_SETUP '4'
                ld      hl,1
                ld      de,2
                call    compare16
                __T_CHECK nzf_cf_de_hl : dw 2, 1
                __T_SETUP '5'
                ld      hl,2
                ld      de,0
                call    compare16
                __T_CHECK nzf_ncf_de_hl : dw 0, 2

                ; TEST alignUp8K
                __T_SETUP 'a'
                ld      hl,$4000
                call    alignUp8K
                __T_CHECK flags_hl : dw $4000
                __T_SETUP '2'
                ld      hl,$4001
                call    alignUp8K
                __T_CHECK flags_hl : dw $6000
                __T_SETUP '3'
                ld      hl,$5FFF
                call    alignUp8K
                __T_CHECK flags_hl : dw $6000

                ; TEST hexChar
                __T_SETUP 'h'
                ld      a,'0'
                call    hexChar
                __T_CHECK a : db 0
                __T_SETUP '2'
                ld      b,9
.hCh_l2:        ld      a,b
                or      '0'
                call    hexChar
                cp      b
                jr      nz,.hCh_err2
                djnz    .hCh_l2
.hCh_err2:      ld      a,b
                ld      b,high __t_default_bc
                __T_CHECK a : db 0
                __T_SETUP '3'
                ld      b,15
.hCh_l3:        ld      a,b
                add     'f'-15
                call    hexChar
                cp      b
                jr      nz,.hCh_done3
                cp      10
                jr      z,.hCh_done3
                djnz    .hCh_l3
.hCh_done3:     ld      a,b
                ld      b,high __t_default_bc
                __T_CHECK a : db 10
                __T_SETUP '4'
                ld      b,15
.hCh_l4:        ld      a,b
                add     'F'-15
                call    hexChar
                cp      b
                jr      nz,.hCh_done4
                cp      10
                jr      z,.hCh_done4
                djnz    .hCh_l4
.hCh_done4:     ld      a,b
                ld      b,high __t_default_bc
                __T_CHECK a : db 10

                ; TEST distance
                __T_SETUP 'd'
                ld      hl,1234
                ld      de,1234+2345
                call    distance
                ld      a,__t_default_a
                __T_CHECK de_hl : dw 1234+2345, 2345
                __T_SETUP '2'
                ld      hl,1234+2345
                ld      de,1234
                call    distance
                ld      a,__t_default_a
                __T_CHECK de_hl : dw 1234, 2345

                ; TEST negate16
                __T_SETUP 'n'
                ld      hl,1234
                call    negate16
                ld      a,__t_default_a
                __T_CHECK hl : dw -1234
                __T_SETUP '2'
                ld      hl,-1234
                call    negate16
                ld      a,__t_default_a
                __T_CHECK hl : dw 1234

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

;__t_uti_: db      123

                display "[TEST RUN] tests: utils.s                      Space: ",/A,$-__test_utils_s
