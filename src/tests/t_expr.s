;;----------------------------------------------------------------------------------------------------------------------
; expr.s tests
; coverage: exprIsNext, [internal] exprCheckRoom, exprGet

__test_expr_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Expr: "
                ; page in assembler module
                ld      a,(AsmCode)
                page    7,a

                ; TEST exprIsNext
__t_expr_exprIsNext:
                __T_SETUP 'n'   ; loop through all possible 256 values of A and count the CF=1 returned in HL
                xor     a
                ld      b,a     ; CF=0 expected for: '-','+','~' and (T_EXPR-1) many T_* control bytes
                ld      c,a     ; BC = 0, HL = __t_default_hl
.l0:            call    exprIsNext
                adc     hl,bc
                inc     a
                jr      nz,.l0
                ld      bc,__t_default_bc
                __T_CHECK a_hl : db 0 : dw __t_default_hl + 256 - (T_EXPR-1) - 3
                __T_SETUP '.'   ; check expression starting chars only
                ld      de,0
                ld      a,T_SYMBOL
                call    exprIsNext
                adc     hl,de
                ld      a,T_VALUE
                call    exprIsNext
                adc     hl,de
                ld      a,T_DOLLAR
                call    exprIsNext
                adc     hl,de
                ld      a,T_STRING
                call    exprIsNext
                adc     hl,de
                ld      a,'-'
                call    exprIsNext
                adc     hl,de
                ld      a,'+'
                call    exprIsNext
                adc     hl,de
                ld      a,'~'
                call    exprIsNext
                adc     hl,de
                __T_CHECK a_de: db '~' : dw 0

                ; TEST exprCheckRoom [internal API]
                __T_SETUP 'r'
                ld      hl,ExprBuffer
                call    exprCheckRoom
                ld      c,low __t_default_bc
                __T_CHECK ncf_hl : dw ExprBuffer
                __T_SETUP '.'
                ld      hl,ExprBuffer+256-4 ; already at last slot, exprCheckRoom should fail
                call    exprCheckRoom
                __T_CHECK errorPrintCnt1
                __T_SETUP ':'
                ld      hl,ExprBuffer+256-8 ; last valid slot
                call    exprCheckRoom
                ld      c,low __t_default_bc
                __T_CHECK ncf_hl : dw ExprBuffer+256-8

                ; TEST exprGet - simple cases and each operator alone
                __T_SETUP 'e'               ; empty input
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, "\0\0\0"
                call    exprGet
                __T_EXPECT_ERR
                ld      a,__t_default_a
                ld      hl,__t_default_hl
                __T_CHECK cf_de : dw AsmBuffer
                __T_SETUP 'E'               ; easy input (single value)
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 123, 0, 0>  ; simple value 123 (already processed by lexer)
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0 : dw 123, AsmBuffer+3
                ; all operators alone
                __T_SETUP '+'               ; unary + ; +123 ; 2b 02 7b 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'+', T_VALUE, 123, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0 : dw +123, AsmBuffer+4
                __T_SETUP '-'               ; unary - ; -123 ; 2d 02 7b 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'-', T_VALUE, 123, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db -123>>16 : dw -123, AsmBuffer+4
                __T_SETUP '~'               ; unary ~ ; ~123 ; 7e 02 7b 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'~', T_VALUE, 123, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ~0>>16 : dw ~123, AsmBuffer+4
                __T_SETUP '+'               ; + ; 12+34 ; 02 0c 00 2b 02 22 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 12, 0, '+', T_VALUE, 34, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 12+34>>16 : dw 12+34, AsmBuffer+7
                __T_SETUP '-'               ; - ; 12-34 ; 02 0c 00 2d 02 22 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 12, 0, '-', T_VALUE, 34, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 12-34>>16 : dw 12-34, AsmBuffer+7
                __T_SETUP '|'               ; | ; 8|40 ; 02 08 00 7c 02 28 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 8, 0, '|', T_VALUE, 40, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (8|40)>>16 : dw 8|40, AsmBuffer+7
                __T_SETUP '&'               ; & ; 9&40 ; 02 09 00 26 02 28 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 9, 0, '&', T_VALUE, 40, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (9&40)>>16 : dw 9&40, AsmBuffer+7
                __T_SETUP '^'               ; ^ ; 9^40 ; 02 09 00 5e 02 28 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 9, 0, '^', T_VALUE, 40, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (9^40)>>16 : dw 9^40, AsmBuffer+7
                __T_SETUP '*'               ; * ; 12*34 ; 02 0c 00 2a 02 22 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 12, 0, '*', T_VALUE, 34, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 12*34>>16 : dw 12*34, AsmBuffer+7
                __T_SETUP '/'               ; / ; 123/34 ; 02 7b 00 2f 02 22 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 123, 0, '/', T_VALUE, 34, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 123/34>>16 : dw 123/34, AsmBuffer+7
                __T_SETUP '%'               ; % ; 123 mod 34 ; 02 7b 00 9c 02 22 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 123, 0, OP_MOD, T_VALUE, 34, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 123%34>>16 : dw 123%34, AsmBuffer+7
                __T_SETUP '<'               ; << ; 123<<3 ; 02 7b 00 9a 02 03 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 123, 0, OP_SHL, T_VALUE, 3, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (123<<3)>>16 : dw 123<<3, AsmBuffer+7
                __T_SETUP '>'               ; >> ; 123>>3 ; 02 7b 00 9b 02 03 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 123, 0, OP_SHR, T_VALUE, 3, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (123>>3)>>16 : dw 123>>3, AsmBuffer+7

                ; TEST exprGet - basic precedence (+-*/) and left-to-right
                __T_SETUP '1'               ; left to right ; 40-8-2 ; 02 28 00 2d 02 08 00 2d 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '-', T_VALUE, 8, 0, '-', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40-8-2>>16 : dw 40-8-2, AsmBuffer+11 : ASSERT 30 == 40-8-2
                __T_SETUP '.'               ; left to right ; 40-8+2 ; 02 28 00 2d 02 08 00 2b 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '-', T_VALUE, 8, 0, '+', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40-8+2>>16 : dw 40-8+2, AsmBuffer+11 : ASSERT 34 == 40-8+2
                __T_SETUP '*'               ; mul precedence ; 40-8*2 ; 02 28 00 2d 02 08 00 2a 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '-', T_VALUE, 8, 0, '*', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40-8*2>>16 : dw 40-8*2, AsmBuffer+11 : ASSERT 24 == 40-8*2
                __T_SETUP '.'               ; mul precedence ; 40*8-2 ; 02 28 00 2a 02 08 00 2d 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '*', T_VALUE, 8, 0, '-', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40*8-2>>16 : dw 40*8-2, AsmBuffer+11 : ASSERT 318 == 40*8-2
                __T_SETUP '/'               ; div precedence ; 40-8/2 ; 02 28 00 2d 02 08 00 2f 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '-', T_VALUE, 8, 0, '/', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40-8/2>>16 : dw 40-8/2, AsmBuffer+11 : ASSERT 36 == 40-8/2
                __T_SETUP '.'               ; div precedence ; 40/8-2 ; 02 28 00 2f 02 08 00 2d 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '/', T_VALUE, 8, 0, '-', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40/8-2>>16 : dw 40/8-2, AsmBuffer+11 : ASSERT 3 == 40/8-2
                __T_SETUP '2'               ; left to right ; 40/8*2 ; 02 28 00 2f 02 08 00 2a 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '/', T_VALUE, 8, 0, '*', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40/8*2>>16 : dw 40/8*2, AsmBuffer+11 : ASSERT 10 == 40/8*2
                __T_SETUP '.'               ; left to right ; 40*8/2 ; 02 28 00 2a 02 08 00 2f 02 02 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 40, 0, '*', T_VALUE, 8, 0, '/', T_VALUE, 2, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 40*8/2>>16 : dw 40*8/2, AsmBuffer+11 : ASSERT 160 == 40*8/2

                ; test '3' was modified and moved to suite 3 (chained unary operators are now syntax error)

                ; TEST exprGet - other operators precedence (&|^<<>>) and left-to-right
                __T_SETUP '4'               ; ~4*10/2 mod 10+7-9<<2>>1&4|28^8 ; from most-precedent to least-precedent
                ; 7e 02 04 00 2a 02 0a 00 2f 02 02 00 9c 02 0a 00
                ; 2b 02 07 00 2d 02 09 00 9a 02 02 00 9b 02 01 00
                ; 26 02 04 00 7c 02 1c 00 5e 02 08 00 00        ; expected result 20, data size 45 (including \0)
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'~',T_VALUE,4,0,'*',T_VALUE,10,0,'/',T_VALUE,2,0,OP_MOD,T_VALUE,10,0,'+',T_VALUE,7,0,'-',T_VALUE,9,0,OP_SHL,T_VALUE,2,0,OP_SHR,T_VALUE,1,0,'&',T_VALUE,4,0,'|',T_VALUE,28,0,'^',T_VALUE,8,0,0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 20>>16 : dw ~4*10/2%10+7-9<<2>>1&4|28^8, AsmBuffer+44 : ASSERT 20 == (~4*10/2%10+7-9<<2>>1&4|28^8)
                __T_SETUP '.'               ; 4^12|$FFBF&52>>1<<2-9+7 mod 10/3*5 ; from least-precedent to most-precedent
                ; 02 04 00 5e 02 0c 00 7c 02 bf ff 26 02 34 00 9b
                ; 02 01 00 9a 02 02 00 2d 02 09 00 2b 02 07 00 9c
                ; 02 0a 00 2f 02 03 00 2a 02 05 00 00               ; expected result $98, data size 44 (including \0)
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE,4,0,'^',T_VALUE,12,0,'|',T_VALUE,$BF,$FF,'&',T_VALUE,52,0,OP_SHR,T_VALUE,1,0,OP_SHL,T_VALUE,2,0,'-',T_VALUE,9,0,'+',T_VALUE,7,0,OP_MOD,T_VALUE,10,0,'/',T_VALUE,3,0,'*',T_VALUE,5,0,0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db $98>>16 : dw 4^12|$FFBF&52>>1<<2-9+7%10/3*5, AsmBuffer+43 : ASSERT $98 == (4^12|$FFBF&52>>1<<2-9+7%10/3*5)

                ; TEST exprGet - parentheses precedence
                __T_SETUP '5'               ; ~(-(4*(11/(42 mod (11+(7-(1<<(25>>(3&(5|(28^10)))))))))))
                ; 7e 28 2d 28 02 04 00 2a 28 02 0b 00 2f 28 02 2a
                ; 00 9c 28 02 0b 00 2b 28 02 07 00 2d 28 02 01 00
                ; 9a 28 02 19 00 9b 28 02 03 00 26 28 02 05 00 7c
                ; 28 02 1c 00 5e 02 0a 00 29 29 29 29 29 29 29 29
                ; 29 29 29 00                                       ; expected result 19, data size 68 (including \0)
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'~','(','-','(',T_VALUE,4,0,'*','(',T_VALUE,11,0,'/','(',T_VALUE,42,0,OP_MOD,'(',T_VALUE,11,0,'+','(',T_VALUE,7,0,'-','(',T_VALUE,1,0,OP_SHL,'(',T_VALUE,25,0,OP_SHR,'(',T_VALUE,3,0,'&','(',T_VALUE,5,0,'|','(',T_VALUE,28,0,'^',T_VALUE,10,0,')',')',')',')',')',')',')',')',')',')',')',0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 19>>16 : dw ~(-(4*(11/(42 mod (11+(7-(1<<(25>>(3&(5|(28^10))))))))))), AsmBuffer+67
                ASSERT 19 == ~(-(4*(11/(42 mod (11+(7-(1<<(25>>(3&(5|(28^10)))))))))))
                __T_SETUP '.'               ; ((3-1)-(2-9))*(303-(404-808))*((1<<5)+13) ; = $45E7F, 286335
                ; 28 28 02 03 00 2d 02 01 00 29 2d 28 02 02 00 2d
                ; 02 09 00 29 29 2a 28 02 2f 01 2d 28 02 94 01 2d
                ; 02 28 03 29 29 2a 28 28 02 01 00 9a 02 05 00 29
                ; 2b 02 0d 00 29 00                                 ; expected result 286335 ($5E7F truncated to 16b), size 54
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'(','(',T_VALUE,3,0,'-',T_VALUE,1,0,')','-','(',T_VALUE,2,0,'-',T_VALUE,9,0,')',')','*','(',T_VALUE,low 303,high 303,'-','(',T_VALUE,low 404,high 404,'-',T_VALUE,low 808,high 808,')',')','*','(','(',T_VALUE,1,0,OP_SHL,T_VALUE,5,0,')','+',T_VALUE,13,0,')',0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 286335>>16 : dw (((3-1)-(2-9))*(303-(404-808))*((1<<5)+13))&$FFFF, AsmBuffer+53
                ASSERT 286335 == ((3-1)-(2-9))*(303-(404-808))*((1<<5)+13)

                ; TEST exprGet - other (than T_VALUE) types of arguments: T_DOLLAR, T_STRING
                __T_SETUP '$'               ; $
                ld      hl,$1234
                ld      (LinePC),hl
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_DOLLAR, 0, 0> ; extra \0 for __T_MEMCPY
                call    exprGet
                __T_CHECK ncf_a_hl_de : db $1234>>16 : dw $1234, AsmBuffer+1
                __T_SETUP '"'               ; string "abc" (truncated to 16bit 'bc', ie. $6263)
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_STRING, "abc\0", 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0 : dw 'bc', AsmBuffer+5

                ; TEST exprGet - other (than T_VALUE) types of arguments: T_SYMBOL
                ASSERT EXIST __test_symbols_s, "symbols.s test should be part of the same suite and called before this"
                __T_SETUP '@'       ; checks if the symbols.s test cases did run, and added debug symbols like 'l_000'
                ld      de,__t_sym_l_250    ; actual copy of one value-test from there, to make sure it still works
                call    symLookUp
                __T_CHECK only_ncf_de_hl_bc : dw __t_sym_l_250+6, __t_default_hl, $1234
                __T_SETUP '.'               ; symbol l_250 has value $1234 and l_254 has value $5678
                ld      de,AsmBuffer        ; expression "l_250+l_254"
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "l_250\0", '+', T_SYMBOL, "l_254\0", 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db $1234+$5678>>16 : dw $1234+$5678, AsmBuffer+15
                __T_SETUP ':'               ; unknown symbol should return value 0 in pass 0
                xor     a
                ld      (Pass),a
                ld      de,AsmBuffer        ; "__t_expr_f"
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "__t_expr_f\0", 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0>>16 : dw 0, AsmBuffer+12
                __T_SETUP '.'               ; unknown symbol should emit error in pass 1+
                ld      a,1
                ld      (Pass),a
                ld      de,AsmBuffer        ; "__t_expr_f"
                __T_MEMCPY AsmBuffer, <T_SYMBOL, "__t_expr_f\0", 0>
                call    exprGet
                __T_EXPECT_ERR
                __T_CHECK cf                ; DE/HL undefined

                ; TEST exprGet - some syntax errors
                __T_SETUP 'f'               ; expressions to test "+", "1+", "(1)2", ")", "(", "<HL>" (r16 token)
__t_expr_exprGet_syntax:
                ; put all expressions in the buffer in one go
                __T_MEMCPY AsmBuffer, <'+', 0/**/, T_VALUE, 1, 0, '+', 0/**/, '(', T_VALUE, 1, 0, ')', T_VALUE, 2, 0, 0/**/, ')', 0/**/, '(', 0/**/, OP_HL, 0/**/ >
                ld      b,6                 ; total expressions to test, C will count CF=1 results
.l0:            ; fetch expresion address from the table based on B
                ld      hl,__t_exp_f_adr-2
                ld      a,b
                add     a,a
                add     hl,a
                ld      de,(hl)
                call    exprGet
                __T_EXPECT_ERR              ; account for error print
                ld      a,b
                adc     a,c
                ld      c,a                 ; C += B + CF
                djnz    .l0
                ; check final A to match default value + sum(1..5) + 5x CF
                __T_CHECK only_a : db low(__t_default_bc)+1+2+3+4+5+6+6
                __T_SETUP '.'               ; expression having extra closing bracket is NOT syntax error, but terminator
                ld      de,AsmBuffer        ; try "(5+7)+9)" first, sum results in BC + CF, check final DE too
                __T_MEMCPY AsmBuffer, <'(',T_VALUE,5,0,'+',T_VALUE,7,0,')','+',T_VALUE,9,0,')',0>
                call    exprGet
                push    de
                adc     hl,bc
                ld      bc,hl
                ld      de,AsmBuffer        ; try "(5+7))" (sum results in HL + CF)
                __T_MEMCPY AsmBuffer, <'(',T_VALUE,5,0,'+',T_VALUE,7,0,')',')',0>
                call    exprGet
                adc     hl,bc
                pop     bc                  ; final DE from first try (DE should point at the extra closing parenthesis)
                __T_CHECK ncf_a_de_hl_bc : db 0 : dw AsmBuffer+9, __t_default_bc+((5+7)+9)+(5+7), AsmBuffer+13
                __T_SETUP ':'
                ld      de,AsmBuffer        ; invalid operator like exclamation mark
                __T_MEMCPY AsmBuffer, <'!',T_VALUE,3,0,0>
                call    exprGet
                __T_CHECK errorPrintCnt1
                __T_SETUP '.'
                ld      de,AsmBuffer        ; invalid operator like exclamation mark
                __T_MEMCPY AsmBuffer, <'+','!',T_VALUE,3,0,0>
                call    exprGet
                __T_CHECK errorPrintCnt1

                ; TEST exprGet - shifts of negative values vs negative shifts-values and 24b evaluation check
                __T_SETUP '<'               ; << ; (-1<<16)-1 ; 28 2d 02 01 00 9a 02 10 00 29 2d 02 01 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'(', '-', T_VALUE, 1, 0, OP_SHL, T_VALUE, 16, 0, ')', '-', T_VALUE, 1, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (-1<<16)-1>>16 : dw $FFFF, AsmBuffer+14 ; $FF0000-1 = $FEFFFF
                __T_SETUP '.'               ; << ; (-1<<16)-1>>9 ; 28 2d 02 01 00 9a 02 10 00 29 2d 02 01 00 9b 02 09 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'(', '-', T_VALUE, 1, 0, OP_SHL, T_VALUE, 16, 0, ')', '-', T_VALUE, 1, 0, OP_SHR, T_VALUE, 9, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ((-1<<16)-1>>9)>>16 : dw $FF7F, AsmBuffer+18
                __T_SETUP ':'               ; << ; 1<<256 ; 02 01 00 9a 02 00 01 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 1, 0, OP_SHL, T_VALUE, 0, 1, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (1<<256)>>16 : dw 0, AsmBuffer+7
                __T_SETUP '.'               ; << ; 1<<24 ; 02 01 00 9a 02 18 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 1, 0, OP_SHL, T_VALUE, 24, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0 : dw 0, AsmBuffer+7    ; upper 8 bits of 24b result are also 0 in case of 1<<24
                __T_SETUP ':'               ; << ; 1<<-3 ; 02 01 00 9a 2d 02 03 00 00 -> print error
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 1, 0, OP_SHL, '-', T_VALUE, 3, 0, 0>
                call    exprGet
                __T_CHECK errorPrintCnt1
                __T_SETUP '>'               ; >> ; -$2000>>10 ; 2d 02 00 20 9b 02 0a 00 00
                ld      de,AsmBuffer        ;      $FFE000>>10 = $FFFFF8
                __T_MEMCPY AsmBuffer, <'-', T_VALUE, 0, $20, OP_SHR, T_VALUE, 10, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (-$2000>>10)>>16 : dw $FFF8, AsmBuffer+8
                __T_SETUP '.'               ; >> ; $8000>>256 ; 02 00 80 9b 02 00 01 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 0, $80, OP_SHR, T_VALUE, 0, 1, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0 : dw 0, AsmBuffer+7
                __T_SETUP ':'               ; >> ; -$8000>>256 ; 2d 02 00 80 9b 02 00 01 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'-', T_VALUE, 0, $80, OP_SHR, T_VALUE, 0, 1, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (-$8000>>256)>>16 : dw $FFFF, AsmBuffer+8
                __T_SETUP '.'               ; >> ; $8000>>-3 ; 02 00 80 9a 2d 02 03 00 00 -> print error
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, 0, $80, OP_SHR, '-', T_VALUE, 3, 0, 0>
                call    exprGet
                __T_CHECK errorPrintCnt1

                ; TEST exprGet - all operators exercising full 24bit evaluator (with values requiring "carry" between bytes)
                ; skip unary plus (can't do 24b ... or anything basically, it's just `ret`)
                __T_SETUP '-'               ; unary - ; -0+0 ; 2d 02 00 00 2b 02 00 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'-', T_VALUE, 0, 0, '+', T_VALUE, 0, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 0 : dw 0, AsmBuffer+8
                __T_SETUP '.'               ; unary - ; -$1234>>8 ; 2d 02 34 12 9b 02 08 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'-', T_VALUE, $34, $12, OP_SHR, T_VALUE, 8, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ((-$1234)>>8)>>16 : dw (-$1234)>>8, AsmBuffer+8
                __T_SETUP '~'               ; unary ~ ; ~$0800>>8 ; 7e 02 00 08 9b 02 08 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'~', T_VALUE, 0, 8, OP_SHR, T_VALUE, 8, 0, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db (~$0800>>8)>>16 : dw ~8, AsmBuffer+8
                __T_SETUP '+'               ; + ; $8080+$9090 ; 02 80 80 2b 02 90 90 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, $80, $80, '+', T_VALUE, $90, $90, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ($8080+$9090)>>16 : dw ($8080+$9090)&$FFFF, AsmBuffer+7
                __T_SETUP '-'               ; - ; $8080-$8081 ; 02 80 80 2d 02 81 80 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, $80, $80, '-', T_VALUE, $81, $80, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ($8080-$8081)>>16: dw $8080-$8081, AsmBuffer+7
                __T_SETUP '|'               ; | ; $8041<<4|$2142<<4 ; 02 41 80 9a 02 04 00 7c 02 42 21 9a 02 04 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE,$41,$80,OP_SHL,T_VALUE,4,0,'|',T_VALUE,$42,$21,OP_SHL,T_VALUE,4,0,0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ($8041<<4|$2142<<4)>>16 : dw ($8041<<4|$2142<<4)&$FFFF, AsmBuffer+15 : ASSERT $a1430 == ($8041<<4|$2142<<4)
                __T_SETUP '&'               ; & ; $82c1<<4&$a343<<4 ; 02 c1 82 9a 02 04 00 26 02 43 a3 9a 02 04 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE,$c1,$82,OP_SHL,T_VALUE,4,0,'&',T_VALUE,$43,$a3,OP_SHL,T_VALUE,4,0,0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ($82c1<<4&$a343<<4)>>16 : dw ($82c1<<4&$a343<<4)&$FFFF, AsmBuffer+15 : ASSERT $82410 == ($82c1<<4&$a343<<4)
                __T_SETUP '^'               ; ^ ; $82c1<<4&$a343<<4 ; 02 c1 82 9a 02 04 00 5e 02 43 a3 9a 02 04 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE,$c1,$82,OP_SHL,T_VALUE,4,0,'^',T_VALUE,$43,$a3,OP_SHL,T_VALUE,4,0,0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ($82c1<<4^$a343<<4)>>16 : dw ($82c1<<4^$a343<<4)&$FFFF, AsmBuffer+15 : ASSERT $21820 == ($82c1<<4^$a343<<4)
                __T_SETUP '*'               ; * ; $0111*$8101 ; 02 11 01 2a 02 01 81 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <T_VALUE, $11, $01, '*', T_VALUE, $01, $81, 0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db ($0111*$8101)>>16 : dw ($0111*$8101)&$FFFF, AsmBuffer+7 : ASSERT $899211 == $0111*$8101
                __T_SETUP '/'               ; / ; ($7654<<8)/3 ; 28 02 54 76 9a 02 08 00 29 2f 02 03 00 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'(',T_VALUE,$54,$76,OP_SHL,T_VALUE,8,0,')','/',T_VALUE,3,0,0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db $277155>>16 : dw $277155&$FFFF, AsmBuffer+13
                __T_SETUP '%'               ; % ; ($7654<<8) mod ($1234<<8) ; 28 02 54 76 9a 02 08 00 29 9c 28 02 34 12 9a 02 08 00 29 00
                ld      de,AsmBuffer
                __T_MEMCPY AsmBuffer, <'(',T_VALUE,$54,$76,OP_SHL,T_VALUE,8,0,')',OP_MOD,'(',T_VALUE,$34,$12,OP_SHL,T_VALUE,8,0,')',0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db $91C00>>16 : dw $91C00&$FFFF, AsmBuffer+19

                ; TEST exprGet - "too complex" expressions, overflowing some buffer or stack (checking Odin's error handling)
                __T_SETUP 'c'               ; exprGet2 - syntax check - simple overflow just by throwing 64 items on it
                ld      de,AsmBuffer  ; (+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+1)))))))))))))))))))))
                __T_MEMCPY AsmBuffer, <"(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+",T_VALUE,1,0,")))))))))))))))))))))",0>
                call    exprGet
                __T_CHECK errorPrintCnt1
                __T_SETUP 'p'               ; exprProcess - high strain on the op_stack with unary plus (fails in Dev.2h)
                ; shunting yard resolving precedence can't overflow by adding into output stream, as total items
                ; will be equal/less to input
                ; only the real stack can overflow (~128 bytes should be enough for most complex expressions)
                ld      de,AsmBuffer        ; (+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(1))))))))))))))))))))) (63 items to pass syntax check)
                __T_MEMCPY AsmBuffer, <"(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(+(",T_VALUE,1,0,")))))))))))))))))))))",0>
                call    exprGet
                __T_CHECK ncf_a_hl_de : db 1>>16 : dw 1, AsmBuffer+65
                __T_SETUP 'C'               ; .calc_loop - is pushing values from top of ExprBuffer as value-stack
                ld      de,AsmBuffer        ; maximum valid expression putting low strain on the value_buffer
                ld      hl,__t_exp_max.oneLess      ; 31 values with 30 operators (1+2+...+9+0+1+2+..+9+0+1)
                ld      bc,__t_exp_max.SZoneLess
                call    memcpy
                call    exprGet
                __T_CHECK ncf_a_de_hl_bc : db 3*45+1>>16 : dw AsmBuffer+__t_exp_max.SZoneLess-1, 3*45+1, __t_exp_max.SZoneLess
                __T_SETUP '.'               ; first failing expression in Dev.2h, 31 values with 31 operators
                ld      de,AsmBuffer        ; (same expression as previous, but with extra unary plus at beginning)
                ld      hl,__t_exp_max
                ld      bc,__t_exp_max.SZ
                call    memcpy
                call    exprGet             ; does work after rewrite of exprCalculate
                __T_CHECK ncf_a_de_hl_bc : db 3*45+1>>16 : dw AsmBuffer+__t_exp_max.SZ-1, 3*45+1, __t_exp_max.SZ
                __T_SETUP ':'               ; expression putting more strain on value_stack (RPN with more operators at end)
                ld      de,AsmBuffer        ; maximum valid expression putting high strain on the value_buffer
                ld      hl,__t_exp_max2.oneLess
                ld      bc,__t_exp_max2.SZoneLess
                call    memcpy
                call    exprGet
                __T_CHECK ncf_a_de_hl_bc : db $AA01>>16 : dw AsmBuffer+__t_exp_max2.SZoneLess-1, $AA01, __t_exp_max2.SZoneLess
                __T_SETUP '.'               ; first failing expression in Dev.2h with strain on value stack
                ld      de,AsmBuffer        ; (almost same expression as previous, with extra "1+" at beginning)
                ld      hl,__t_exp_max2
                ld      bc,__t_exp_max2.SZ
                call    memcpy
                call    exprGet             ; does work after rewrite of exprCalculate
                __T_CHECK ncf_a_de_hl_bc : db $AA02>>16 : dw AsmBuffer+__t_exp_max2.SZ-1, $AA02, __t_exp_max2.SZ

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_exp_f_adr:  ; start of expressions in the syntax-fails set
                dw      AsmBuffer+0
                dw      AsmBuffer+2
                dw      AsmBuffer+7
                dw      AsmBuffer+16
                dw      AsmBuffer+18
                dw      AsmBuffer+20

; 64 slots in ExprBuffer -> only last two needed for binary operators (.calc_loop), if no extra value is pushed under
; that leaves 61+$FF for RPN expression => 31 values + 30 operators should still work, 31+31 is first to fail in Dev.2h
__t_exp_max:    ; +1+2+3+4+5+6+7+8+9+0+1+2+3+4+5+6+7+8+9+0+1+2+3+4+5+6+7+8+9+0+1
                ; 62 items, 31 values, 31 pluses (1 is unary), expected result 3*45+1
                db      '+'
.oneLess:       db      T_VALUE,1,0,'+',T_VALUE,2,0,'+',T_VALUE,3,0,'+',T_VALUE,4,0,'+',T_VALUE,5,0,'+'
                db      T_VALUE,6,0,'+',T_VALUE,7,0,'+',T_VALUE,8,0,'+',T_VALUE,9,0,'+',T_VALUE,0,0,'+'
                db      T_VALUE,1,0,'+',T_VALUE,2,0,'+',T_VALUE,3,0,'+',T_VALUE,4,0,'+',T_VALUE,5,0,'+'
                db      T_VALUE,6,0,'+',T_VALUE,7,0,'+',T_VALUE,8,0,'+',T_VALUE,9,0,'+',T_VALUE,0,0,'+'
                db      T_VALUE,1,0,'+',T_VALUE,2,0,'+',T_VALUE,3,0,'+',T_VALUE,4,0,'+',T_VALUE,5,0,'+'
                db      T_VALUE,6,0,'+',T_VALUE,7,0,'+',T_VALUE,8,0,'+',T_VALUE,9,0,'+',T_VALUE,0,0,'+'
                db      T_VALUE,1,0
                db      0
.SZ:            equ     $-__t_exp_max
.SZoneLess:     equ     $-.oneLess

; near-maximum expression when putting more strain on the value_stack in final calculation loop, making it to store
; several values into stack before reaching the ending block of operators (in RPN notation)
; (unary pluses at beginning are padding to reach failure, calculated early after first value, the strain starts after)
__t_exp_max2:   ; 1+(1|$AAAA&$FFFF<<0+1*(1|8&1<<9-1*(2|4&1<<0+1*(2|1&0<<0+1*2))))
                ; 22 values, ~30 ops+parentheses, expected result $AA02 - in Dev.2h fails in calc loop with push_error
                db      T_VALUE,1,0,'+'
.oneLess:       ; following works in Dev.2h, expected result $AA01 (without the leading "1+")
                db      '('
                db      T_VALUE,1,0,'|',T_VALUE,$AA,$AA,'&',T_VALUE,$FF,$FF,OP_SHL,T_VALUE,0,0,'+',T_VALUE,1,0,'*','('
                db      T_VALUE,1,0,'|',T_VALUE,8,0,'&',T_VALUE,1,0,OP_SHL,T_VALUE,9,0,'-',T_VALUE,1,0,'*','('
                db      T_VALUE,2,0,'|',T_VALUE,4,0,'&',T_VALUE,1,0,OP_SHL,T_VALUE,0,0,'+',T_VALUE,1,0,'*','('
                db      T_VALUE,2,0,'|',T_VALUE,1,0,'&',T_VALUE,0,0,OP_SHL,T_VALUE,0,0,'+',T_VALUE,1,0,'*',T_VALUE,2,0
                db      ')',')',')',')',0
.SZ:            equ     $-__t_exp_max2
.SZoneLess:     equ     $-.oneLess

                display "[TEST RUN] tests: expr.s                       Space: ",/A,$-__test_expr_s
