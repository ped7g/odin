;;----------------------------------------------------------------------------------------------------------------------
;; Odin Fullscreen Editor
;;----------------------------------------------------------------------------------------------------------------------

kEditorBase     equ     $2800

;                       +---------+---------+---------+---------+---------+---------+---------+---------+
EditorStatus    dz      "                                                   MEM       LINE       COL     "
EditorNoName    dz      "<No Name>"
kEditorFNameLen equ     40

OldCursor       dw      0               ; Stores the cursor position from the previous mode (e.g. monitor)
AutoIndentBy    db      0               ; Preserve auto-indentation width inside editorNewLine

;;----------------------------------------------------------------------------------------------------------------------
;; editorDrawFileName
;; Draw a filename of maximum 40 characters (padded to 41 characters total with '\0')
;;
;; Input:
;;      HL = screen address
;;      DE = filename address
;;      C = colour
;;
;; Output:
;;      HL += 41*2
;;
;; Affects:
;;      AF, B, DE   (A and B is zero, could become part of ABI if it helps)
;;
editorDrawFileName:
                push    bc
                ex      de,hl                   ; DE = screen, HL = filename
                xor     a
                cp      (hl)
                jr      nz,.has_fname
                ld      hl,EditorNoName
.has_fname:
                call    strlen                  ; length without null-term
                ex      de,hl                   ; HL = screen, DE = filename
                ld      a,kEditorFNameLen
                sub     c
                pop     bc
                ld      b,a                     ; amount of spaces to draw after name to align it to 40 chars
                jr      nc,.name_fits           ; name is <= 40 chars long
                ; draw ellipsis and adjust start of name to fit after it
                cpl
                add     a,3+1                   ; amount of chars to skip (neg(a)+3)
                add     de,a
                ld      a,'.'
                ld      b,3
                call    .fillChars
                ; leave B zeroed (zero spaces to add after the name)
.name_fits:
                inc     b                       ; +1 to draw always at least 1 "space" padding
.draw_l0:
                ldi     a,(de)
                and     a
                jr      z,.fillChars            ; fill remaining part of 40 (+1) chars with zero-char
                ldi     (hl),a
                ldi     (hl),c
                jp      .draw_l0

.fillChars:
                ldi     (hl),a
                ldi     (hl),c
                djnz    .fillChars
editorReturn:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorEnter
;;

editorEnter:
                di

                ;;
                ;; Check to see if we have a document
                ;;

                call    docExists
                call    c,docNewNoName

                ;;
                ;; Clear editor screen and switch to it
                ;;

.no_reset:
                call    pageVideo
                call    cursorHide
                ld      hl,$4000+kEditorBase
                ld      (ScreenBase),hl
                call    cls

                bchilo  kScreenHeight-2,0
                call    calcAddr                ; HL = tile address for given coords

                ld      b,80
                ld      de,EditorStatus
                ld      c,kInkStatus << 1
.loop_status:
                ; Write space
                ldi     a,(de)
                ldi     (hl),a
                ldi     (hl),c
                djnz    .loop_status


                ; Write the filename
                bchilo  kScreenHeight-2,1
                call    calcAddr
                ld      c,kInkFileName<<1
                ld      de,MainDocName
                call    editorDrawFileName
                ld      a,kInkNormal<<1
                call    editorClearStatus

                nextreg REG_TMAP_BASE,high kEditorBase

;;----------------------------------------------------------------------------------------------------------------------
;; editorDrawAll
;; Draw whole screen of text
;;

                ;;
                ;; Draw the whole screen
                ;;

editorDrawAll:
                ld      hl,(MainDoc.TopLine)
                ld      (editorLineToDraw),hl      ; Store the current line number
                
                ld      hl,(MainDoc.TopAddr)
                ld      de,$4000+kEditorBase    ; Start of display for editor
                ld      b,kScreenHeight-2
.loop_draw:
                push    de
                ld      de,(MainDoc.Length)
                dec     de                      ; Don't include the trailing $ff
                and     a
                sbc     hl,de
                add     hl,de
                pop     de
                jp      z,.loop_empty
.loop_text:
                call    editorCheckColours
                call    editorDrawLine
                push    hl
                ld      hl,(editorLineToDraw)
                inc     hl
                ld      (editorLineToDraw),hl
                pop     hl
                djnz    .loop_draw
                ret

                ; Reached end of document
.loop_empty:
                call    pageVideo
                ex      de,hl
                ld      e,b
                ld      bc,158                  ; Length of line past tilde
.l0
                ; Write tilde at start of line
                ld      a,'~'
                ldi     (hl),a
                ld      a,kInkTilde<<1
                ldi     (hl),a

                ; Clear rest of line
                call    memclear
                add     hl,bc
                dec     e
                jp      nz,.l0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorMoveTop
;; Move the view of the document to the give line position.  Then render what is required.
;;
;; Input:
;;      HL = address of top line
;;      BC = line # of top line
;;

editorMoveTop:
                ld      (MainDoc.TopAddr),hl
                ld      (MainDoc.TopLine),bc
                call    editorDrawAll
                jp      editorEnterLine

;;----------------------------------------------------------------------------------------------------------------------
;; editorCheckColours
;;
;; INPUT:
;;      HL = line number
;;

editorCheckColours:
                ; If we're not in a block, use normal colours
                push    bc,hl
                ld      hl,(editorLineToDraw)
                ld      a,(MainDoc.InBlock)
                and     a
                push    de
                jr      z,.no_block

                ; Calculate first and last line of block
                push    hl
                ld      hl,(MainDoc.BlockStart)
                ld      de,(MainDoc.BlockEnd)
                call    max                     ; HL = last line, DE = first line
                ld      (.last_line),hl
                pop     hl

                ; check to see that current line is between (inclusively) start and end
                call    compare16
                jr      c,.no_block             ; Line is before first line of block?

                ld      de,(.last_line)
                ex      de,hl
                call    compare16
                jr      c,.no_block             ; Line is after last_line of block?

                ; We are inside a block
                pop     de
                bchilo  kInkBlockNormal<<1,kInkBlockComment<<1
                jr      .set_colours
.last_line      dw      0
.no_block
                pop     de
                bchilo  kInkNormal<<1,kInkComment<<1
.set_colours
                ld      a,b
                ld      (editorNormalColour),a
                ld      a,c
                ld      (editorCommentColour),a
                pop     hl,bc
                ret



;;----------------------------------------------------------------------------------------------------------------------
;; editorDrawCurrentLine
;; Draw the current line (detokenised in Buffer) at the correct screen position
;;

editorNormalColour      db      0
editorCommentColour     db      0
editorLineToDraw        dw      0

editorDrawCurrentLine:
                push    hl
                ld      hl,(MainDoc.Line)
                ld      (editorLineToDraw),hl
                pop     hl
                call    editorCheckColours

                call    editorCalcAddr
                ex      de,hl

                ; Fallthrough to next routine

;;----------------------------------------------------------------------------------------------------------------------
;; editorDrawBuffer
;; Draw a single line stored in Buffer and fill 160 bytes in the display from it.
;;
;; Input:
;;      DE = display position
;;

editorDrawBuffer:
                ld      hl,EditorBuffer

editorDrawHL:
                ;;
                ;; Render line out
                ;;
                call    pageVideo
                ld      b,80
                ld      a,(editorNormalColour)
                ld      ixl,a
                xor     a
                ld      (.state),a
.text_loop:
                ld      a,(hl)
                and     a
                jp      z,.end_text

                ld      a,(.state)
                ld      c,a
                and     a
                jr      nz,.state_1

                ; State 0
                ;       ; -> 1
                ;       ' -> 2
                ;       A -> 3 (used to detect sequence AF')
                ;       " -> 4
                ld      a,(hl)
                inc     c                       ; Initialise state
                cp      ';'
                jp      z,.comment
                inc     c
                cp      $27     ; single quote
                jp      z,.print
                inc     c
                cp      'A'
                jp      z,.print
                cp      'a'
                jp      z,.print
                inc     c
                cp      $22     ; double quotes
                jp      z,.print
.home
                ; Reset back to home state
                ld      c,0
                jp      .print

.state_1:
                ; Deal with characters after ; - they are just comments so fast
                ; loop and pint
                cp      1
                jp      z,.print
.state_2:
                ; Deal with characters after ' - print until we find another quote
                cp      2
                jp      nz,.state_3
                ld      a,(hl)
                cp      $27
                jp      nz,.print
.state_3:
                ; Deal with AF'
                cp      3
                jp      nz,.state_4
                ld      a,(hl)
                cp      ';'
                jp      z,.comment
                cp      'F'
                jp      nz,.home
                ld      c,5
                jp      .print
.state_4:
                cp      4
                jp      nz,.state_5
                ld      a,(hl)
                cp      $22
                jp      nz,.print
                jp      .home
.state_5:
                ld      a,(hl)
                cp      ';'
                jp      z,.comment
                jp      .home

.comment:
                ld      a,(editorCommentColour)
                ld      ixl,a
.print:
                ; C = character to print
                ; D = next state
                ld      a,c
                ld      (.state),a
                ldi     a,(hl)
                ldi     (de),a
                ld      a,ixl
                ldi     (de),a
                djnz    .text_loop
                ; full line shown (80+ chars), mark the end of line with extra ">" symbol
                dec     de
                dec     de
                ld      a,'>'
                ldi     (de),a
                ld      a,kInkLongLine<<1
                ldi     (de),a
.end_text:
                ld      a,b
                and     a
                ret     z

                ; Line not completely drawn
                push    hl
                ex      de,hl
                ld      a,(editorNormalColour)
                ld      e,a
                xor     a
.l1:
                ldi     (hl),a
                ldi     (hl),e
                djnz    .l1
                ex      de,hl
                pop     hl
                ret

.state          db      0

;;----------------------------------------------------------------------------------------------------------------------
;; editorDrawLine
;; Draw a single line and fill 160 bytes in the display from it
;;
;; Input:
;;      HL = offset into document
;;      DE = display position
;;
;; Output:
;;      HL = offset into document after line just drawn
;;      DE = display position of next line
;;

editorDrawLine:
                ;;
                ;; Detokenise line into buffer
                ;;

                push    bc

                call    pageDoc
                push    de
                ld      de,TokenisedBuffer
                call    detokenise              ; HL = document pointer at null terminator of this line
                pop     de

                push    hl
                ld      hl,TokenisedBuffer
                call    editorDrawHL
                pop     hl
                call    pageDoc
                ld      a,(hl)                  ; Did we reach the null terminator?
                and     a
                call    nz,strEnd               ; No, so find it
                inc     hl                      ; Skip past it
                pop     bc
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; editorLeave
;;

editorLeave:
                call    editorLeaveLine
                nextreg REG_TMAP_BASE,0
                ld      hl,$4000
                ld      (ScreenBase),hl
                jp      cursorShow

;;----------------------------------------------------------------------------------------------------------------------
;; editorLoop
;;

editorLoop:
                ld      bc,(CurrentCoords)
                ld      (OldCursor),bc
                call    editorEnter
                call    editorEnterLine
                ei

.main_loop:
                ; Update status line
                call    pageVideo
                bchilo  30,66
                ld      hl,(MainDoc.Line)
                inc     hl
                call    editorPrintNum16

                bchilo  30,76
                ld      a,(MainDoc.Col)
                inc     a
                call    editorPrintNum8

                bchilo  30,55
                ld      hl,49152
                ld      de,(MainDoc.Length)
                and     a
                sbc     hl,de
                call    editorPrintNum16

                call    editorUpdateCursor

                ; Show the dirty flag
                ld      hl,MainDoc.Flags
                ld      a,' '
                bit     0,(hl)
                jr      z,.no_dirty
                ld      a,'*'
.no_dirty:
                bchilo  kScreenHeight-2,0
                call    calcAddr
                ld      (hl),a

.loop:
                //call    editorDebug
                call    consoleUpdate
                call    inKey
                jr      z,.loop

                call    cursorHide

                ld      c,a
                ld      a,kInkNormal<<1
                call    editorClearStatus
                ld      a,c

                cp      $20
                jr      c,.table00
                cp      $80
                jr      nc,.table80

                call    editorInsertChar
                call    cursorShow
                jr      .main_loop

.table00:
                ld      hl,CmdTable00
                jr      .do_cmd
.table80:
                ld      hl,CmdTable80
                sub     $80
.do_cmd:
                add     hl,a
                add     hl,a
                ldhl
                callhl
                call    editorEnsureVisible
                call    editorDrawCurrentLine

                jr      .main_loop

;;----------------------------------------------------------------------------------------------------------------------
;; editorError/Msg
;; Show a message in the status line
;;
;; Output:
;;      CF = 1
;;

editorError:
                ld      a,kInkError<<1
                jr      editorMsg.error
editorMsg:
                ld      a,kInkNormal<<1
.error:
                ex      (sp),hl                 ; HL = message
                push    de
                call    editorClearStatus       ; Video paged in

                ; HL = string
                ; DE = screen address
.l0:
                ldi     a,(hl)
                and     a
                jr      z,.end
                ld      (de),a
                inc     de
                inc     de
                jp      .l0
.end:
                pop     de
                ex      (sp),hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorDebug
;; Show a status message that shows the contents of 16-bit values
;;

EditorDebugMsg  dz      "Debug:"
EditorDebugPtr:
                dw      MainDoc.TopLine
                dw      MainDoc.Line
                dw      MainDoc.Addr
                dw      MainDoc.TopAddr
                dw      0

editorDebug:
                bchilo  kScreenHeight-1,0
                call    calcAddr
                ld      de,EditorDebugMsg
                ld      c,kInkNormal<<1

.l0:
                ldi     a,(de)
                and     a
                jr      z,.end_prefix
                ldi     (hl),a
                ldi     (hl),c
                jr      .l0

.end_prefix:
                ld      ix,EditorDebugPtr
                bchilo  kScreenHeight-1,7
.l1:
                ld      hl,(ix+0)
                inc     ix
                inc     ix
                ld      a,h
                or      l
                ret     z
                ldhl
                call    editorPrintHex16
                ld      a,c
                add     a,5
                ld      c,a
                jr      .l1

                ret


;;----------------------------------------------------------------------------------------------------------------------
;; editorClearStatus
;; Clear the bottom row of the screen
;;
;; Input:
;;      A = colour attribute
;;
;; Output:
;;      DE = screen address to start writing status message
;;

editorClearStatus:
                push    bc,hl,af
                call    pageVideo
                bchilo  kScreenHeight-1,0
                call    calcAddr
                ld      d,h
                ld      e,l
                pop     af
                bchilo  80,' '
.l0:
                ldi     (hl),c          ; Write space
                ldi     (hl),a          ; Write colour
                djnz    .l0
                pop     hl,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorPrintNum16
;; Print a 16-bit number (HL) in decimal at the coordinates given in BC (YX).
;;
;; Input:
;;      HL = 16-bit number
;;      BC = YX coords
;;

editorPrintNum16:
                push    bc,de,hl,ix
                ld      e,0
                call    unpackD24
                push    hl
                call    calcAddr
                ex      (sp),hl                 ; EHL = 5 digit number
                pop     ix                      ; IX = screen address
                call    editorPrintBCD5
                pop     ix,hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorPrintHex16
;; Print a 16-bit number (HL) in hexadecimal at the coordinates given in BC (YX).
;;
;; Input:
;;      HL = 16-bit number
;;      BC = YX coords
;;

editorPrintHex16:
                push    bc,hl
                ld      a,h
                call    editorPrintHex8
                ld      a,l
                inc     c
                inc     c
                call    editorPrintHex8
                pop     hl,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorPrintHex8
;; Print an 8-bit number (A) in hexadecimal at the coordinates given in BC (YX).
;;
;; Input:
;;      A = 8-bit number
;;      BC = YX coords
;;

editorPrintHex8:
                push    bc,de,hl
                ld      e,a
                swapnib
                and     $0f
                call    editorPrintNybble
                ld      a,e
                and     $0f
                inc     c
                call    editorPrintNybble
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorPrintNybble
;; Print a single hex digit at the given coordinates.
;;
;; Input:
;;      A = nybble
;;      BC = YX coords
;;

editorPrintNybble:
                push    bc,hl
                call    calcAddr
                
                cp      10
                sbc     a,$69
                daa

                ld      (hl),a
                pop     hl,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorPrintNum8
;; Print a 16-bit number (A) in decimal at the coordinates given in BC (YX).
;;
;; Input:
;;      A = 8-bit number
;;      BC = YX coords
;;

editorPrintNum8:
                push    bc,de,hl,ix
                ld      l,a
                ld      h,0
                ld      e,0
                call    unpackD24
                push    hl
                call    calcAddr
                ex      (sp),hl                 ; EHL = 5 digit number
                pop     ix                      ; IX = screen address
                call    editorPrintBCD3
                pop     ix,hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorPrintBCD5/3
;; Print either a 5 digit or 3 digit number
;;
;; Input:
;;      EHL = 5 digit BCD number
;;      HL = 3 digit BCD number
;;      IX = screen address
;;
;; Output:
;;      IX = next screen address
;;

editorPrintBCD5:
                ld      a,e
                and     $0f
                call    editorPrintDigit
                ld      a,h
                swap
                and     $0f
                call    editorPrintDigit
editorPrintBCD3:
                ld      a,h
                and     $0f
                call    editorPrintDigit
                ld      a,l
                swap
                and     $0f
                call    editorPrintDigit
                ld      a,l
                and     $0f

editorPrintDigit:
                add     a,$30
                ld      (ix+0),a
                inc     ix
                inc     ix
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorUpdateCursor
;; Checks that the position of the cursor is on the screen and shows it.
;;
;; Assumes: cursor is hidden.  Will show at end
;;

editorUpdateCursor:
                ld      hl,(MainDoc.Line)
                ld      de,(MainDoc.TopLine)
                call    compare16               ; Line # < Top Line # ?
                jr      c,.outside              ; Cursor is before top of screen

                sbc     hl,de                   ; # lines down the screen
                ld      de,kScreenHeight-2
                call    compare16               ; Cursor on screen?
                jr      c,.inside

.outside:
                ; Turn the cursor off
                jp      cursorOff

.inside:
                ; HL = line # on screen for cursor
                ld      b,l
                ld      a,(MainDoc.Col)
                ld      c,a
                call    at
                call    cursorOn
                jp      cursorShow

;;----------------------------------------------------------------------------------------------------------------------
;; editorCalcAddr
;; Update the screen address of the current line using MainDoc.TopLine and MainDoc.Line
;;
;; Output:
;;      HL = screen address of current line
;;

editorCalcAddr:
                ld      de,(MainDoc.TopLine)
                ld      hl,(MainDoc.Line)
                and     a
                sbc     hl,de
                ld      b,l                     ; C = Y coord
                ld      c,0
                call    calcAddr                ; HL = screen address
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorEnterLine
;; Called whenever the line changes.  It copies the line from the document to the edit buffer.
;;
;; Output:
;;      DE = Buffer that contains line (256-byte aligned)
;;      (EditorTLineLen) = length of line on entry
;;

editorEnterLine:
                push    bc,de,hl
                call    pageDoc

                ; Fill the editing buffer with the current line and update the column position
                ld      hl,(MainDoc.Addr)
                call    bufferFromDoc
                ld      hl,MainDoc.Col
                ld      a,(hl)
                call    editorClampPos
                ld      (hl),a

                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorLeaveLine
;; Called whenever the line changes or the screen needs to be redrawn.  It updates the document with the line in the 
;; edit buffer.
;;
;; Output:
;;      CF = 1 if error occurs trying to leave line (i.e. not enough memory in document)
;;

editorLeaveLine:
                push    bc,de,hl
                call    pageDoc

                ld      hl,(MainDoc.Addr)
                call    bufferToDoc
                jr      nc,.end

                ; Not enough memory for new line so show error
                call    editorError
                dz      "Not enough memory for line changes"
.end:
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorCmdFormat
;; Format the current line
;;
;; There are 4 possible fields:
;;
;;      | Field | Offset | Description | Start byte                            |
;;      |-------|--------|-------------|---------------------------------------|
;;      | 1     | 0      | Label       | Label character                       |
;;      | 2     | 8      | Opcode      | Label character                       |
;;      | 3     | 13     | Operands    | Any character other than a semicolon  |
;;      | 4     | 32     | Comments    | Semicolon                             |
;;
;; We'll try to place the various fields at those offsets.  If any of the fields are passed their offset, we're
;; insert a space and continue with the next field.
;;

editorCmdFormat:
                call    editorInBlockMode
                dw      .blockFormat
                call    editorFormat
                jp      editorUpdateLine
.blockFormat:
                ld      hl,(MainDoc.BlockStart)
                ld      de,(MainDoc.BlockEnd)
                call    max                     ; DE = start line, HL = end line
                and     a
                inc     hl
                sbc     hl,de                   ; HL = number of lines
                ex      de,hl                   ; DE = number of lines, HL = line number
                push    de,hl
                call    editorGotoLine
                pop     hl,de

.format_loop:
                xor     a
                ld      (MainDoc.Col),a
                call    editorResetReqCol
.l0
                push    de,hl
                call    editorFormat
                call    editorLeaveLine
                call    editorNextLine
                call    editorEnterLine
                pop     hl,de
                dec     de
                inc     hl
                ld      a,d
                or      e
                jr      nz,.l0
                ld      (MainDoc.InBlock),a
                ld      hl,(MainDoc.BlockStart)
                call    editorGotoLine
                jp      editorLinesChanged

editorFormat:
                ld      hl,EditorBuffer     ; And output here
                xor     a
                ld      (.reached_opcode),a ; Set to opcode mode
                ld      (.seen_label),a
                ld      (MainDoc.Col),a     ; Move cursor to beginning of line

.next_field:
                call    .delete_spaces      ; Remove initial spaces
                ret     z
                call    .process_field
                jr      nc,.next_field

.reached_opcode db      0                   ; Bit 0 = seen opcode
.seen_label     db      0                   ; Bit 0 = seen label
.process_field:
                ld      a,(hl)
                cp      ';'
                jr      z,.process_comment

                ; Identify which field it is
                ld      a,(.reached_opcode)
                and     a                   ; Have we seen an opcode yet?
                jr      nz,.process_operand ; Yes, this must be operands

                ; Check to see if it's an opcode or a label
                ld      ix,tokenise_is_ins
                ld      de,hl
                push    hl
                call    tokenise
                pop     hl
                jr      nc,.process_opcode

                ; It's a label!
                ld      a,(.seen_label)
                and     a
                jr      z,.first_label
                ld      a,' '
                call    bufferInsertChar
                ret     c
                inc     l
.first_label:
                ld      a,1
                ld      (.seen_label),a
                jp      .search_end         ; Find end of it!

.process_comment:
                ld      a,l
                and     a
                jp      z,.search_eol       ; Return if comment is at start of line
                ld      a,32
                call    .indent
                ret     c
                jp      .search_eol

.process_opcode:
                ld      a,1
                ld      (.reached_opcode),a
                ld      a,8
                call    .indent
                ret     c
                jp      .search_end

.process_operand:
                ld      a,13
                call    .indent
                ret     c
                jp      .search_eoi

.delete_spaces:
                ld      a,(hl)
                and     a
                ret     z
                cp      $21
                ret     nc
                call    bufferDeleteChar
                jr      .delete_spaces


.search_end:
                ; Search for either space, eol or ;
                ; CF = 0 at end
                ld      a,(hl)
                cp      $21
                jr      c,.found_end    ; Found space or EOL
                cp      ';'
                ret     z               ; Found comment start
                inc     l
                jr      .search_end
.found_end:     ccf                     ; Ensure CF = 0
                ret

.search_eol:
                ; Search for EOL
                ; CF = 0 at end
                ld      a,(hl)
                and     a
                ret     z
                inc     l
                jr      .search_eol

.search_eoi:
                ; Search for end of chars before EOL or ;
                ; CF = 0 at end
                ld      a,(hl)
                and     a
                jr      z,.trim_ws
                cp      ';'
                jr      z,.trim_ws
                inc     l
                jr      .search_eoi

                ; Now trim the whitespace of the end
.trim_ws:
                dec     l
                ld      a,(hl)
                cp      $21
                jr      c,.trim_ws
                inc     l           ; Point to character after field
                ret

.indent:
                ; A = required indentation
                ; CF = 1 if no room to indent
                cp      l           ; CF = 1 if required indentation is less than current indentation
                ret     z           ; Already here
                jr      c,.needs_space

                ; We need to keep insert spaces until we reach the required indentation of
                ; the field.
                ld      b,a
                ld      a,' '
                call    bufferInsertChar
                ret     c
                ld      a,b
                inc     l
                jr      .indent
.needs_space:
                ; We've overshot our field start, so just add a single space
                ld      a,' '
                call    bufferInsertChar
                ret     c
                inc     l
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorInsertChar
;; Insert a single character into the line
;;
;; Input:
;;      A = character
;;

editorInsertChar:
                call    editorInBlockMode
                dw      editorBlockInsertChar
.skip_block:

                ld      c,a

                ; Update buffer
                ld      a,(MainDoc.Col)
                ld      l,a
                ld      a,(InsertMode)
                and     a
                ld      a,c
                jr      z,.insert_mode
                call    bufferOverwriteChar
                jr      .cont
.insert_mode:
                call    bufferInsertChar
.cont:
                jr      c,.no_room

                ; Update screen
                ld      hl,MainDoc.Col
                ld      a,(hl)
                inc     a
                call    editorClampPos
                ld      (hl),a

                call    editorResetReqCol
                jp      editorDrawCurrentLine
.no_room:
                call    editorError
                dz      "Cannot insert character as line is at maximum length"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorGetPos
;; Return the position information of the editor
;;
;; Output:
;;      HL = address of line
;;      BC = Y position of cursor in document
;;      E = X position of cursor in document
;;

editorGetPos:
                ld      hl,(MainDoc.Addr)
                ld      bc,(MainDoc.Line)
                ld      a,(MainDoc.Col)
                ld      e,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorSetPos
;; Given a new position in BC/E, recalculate the actual address of the current line and new cursor position.
;;
;; Input:
;;      BC = Y requested position of cursor
;;      E = X requested position of cursor
;;

editorSetPos:
                call    editorLeaveLine         ; We could be leaving the line so update current line

                ;
                ; Find the correct line
                ;
                
                push    bc,de,hl

                exa
                ld      a,e                     ; A' = request X postion
                exa

                ld      hl,(MainDoc.Line)
                ld      de,hl
                and     a
                sbc     hl,bc                   ; Current line < requested line
                ld      hl,(MainDoc.Addr)
                jr      c,.move_down

                ; Move cursor up until line is found
.prev_line:
                ; Have we reached the requested line
                ld      a,e
                cp      c
                jr      nz,.cont_up
                ld      a,d
                cp      b
                jr      z,.found
.cont_up:
                call    docPrevLine
                dec     de
                jr      .prev_line

.move_down:
                ; Have we reached the requested line
                ld      a,e
                cp      c
                jr      nz,.cont_down
                ld      a,d
                cp      b
                jr      z,.found
.cont_down:
                call    docNextLine
                inc     de
                jr      nc,.move_down
                dec     de

                ; EOF reached
                exa
                ld      a,$ff           ; Go to end of current line
                exa

.found:
                ; HL = requested line
                ld      (MainDoc.Addr),hl
                ld      (MainDoc.Line),de
                exa
                ld      (MainDoc.Col),a

                ; Figure out the X position
                call    editorEnterLine         ; DE = buffer

                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorSetReqCol
;; Set the requested column if not set already
;;

editorSetReqCol:
                push    hl
                ld      hl,MainDoc.ReqCol
                inc     (hl)
                jr      z,.set
                dec     (hl)
                pop     hl
                ret
.set:
                ld      a,(MainDoc.Col)
                ld      (hl),a
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorResetReqCol
;; Reset the requested column to unknown.  This is called when horizontal change occurs.
;;

editorResetReqCol:
                ld      a,$ff
                ld      (MainDoc.ReqCol),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorEnsureVisible
;; Ensure that the current cursor position is on screen.  If not, adjust the top line to the correct position.
;;
;; OUTPUT:
;;      CF = 0 if redraw happened
;;

editorEnsureVisible:
                push    bc,de,hl

                ld      hl,(MainDoc.Line)
                ld      de,(MainDoc.TopLine)
                and     a
                sbc     hl,de                   ; HL = offset from top
                jp      m,.move                 ; Cursor went past top
                ld      de,kScreenHeight-2
                and     a
                sbc     hl,de                   ; Offset < height of window?
                jr      c,.done

.move:
                ld      hl,(MainDoc.Line)
                ld      de,(kScreenHeight-2)/2
                and     a
                sbc     hl,de
                jr      nc,.set_top
                ld      hl,0                    ; Can't move past top
.set_top:
                ld      bc,hl
                call    docFindLine             ; Convert HL as line number to screen address
                call    editorMoveTop           ; Redraw and set new top position
                and     a
.done:
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorUpdateLine
;; Makes sure code written in the buffer is put into the document and the result of tokenisation is back in the buffer
;; and displayed.
;;

editorUpdateLine:
                call    editorLeaveLine
                call    editorEnterLine
                call    editorDrawCurrentLine
                call    pageDoc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorGotoLine
;; Goto a specific line using different methods depending on the current line.
;;
;; TODO: Use different starting point to move cursor from?
;;
;; INPUT:
;;      HL = line to reach (or last line)
;;

editorGotoLine:
                push    bc,de,hl
                push    hl
                call    editorUpdateLine
                call    editorResetReqCol
                pop     bc
                ld      e,0
                call    editorSetPos
                pop     hl,de,bc
                jp      editorBlockUpdate

;;----------------------------------------------------------------------------------------------------------------------
;; editorLinesChanged
;; Used by commands if the current line is changed in any way or line number has changed
;;

editorLinesChanged:
                call    editorEnterLine
                call    editorEnsureVisible
                jr      nc,.drawn
                call    editorDrawAll
.drawn:
                jp      editorResetReqCol


;;----------------------------------------------------------------------------------------------------------------------
;; editorClampPos
;; Adjust a given cursor position to make sure it doesn't go beyond buffer OR screen.
;;
;; Input:
;;      A = position to check
;;
;; Output:
;;      A = clamped position
;;

editorClampPos:
                call    bufferClampPos          ; clamp first to the size of underlying buffer
                cp      kScreenWidth
                ret     c
                ld      a,kScreenWidth-1
                ret


;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
;; C O M M A N D S
;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Leaves the editor
editorCmdExit:
                pop     af                      ; Drop the return address
                call    cursorHide
                call    editorLeave
                ld      bc,(OldCursor)
                call    at
cmdDoNothing:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor left
editorCmdLeft:
                call    editorInBlockMode
                dw      editorReturn
                ld      hl,MainDoc.Col
                ld      a,(hl)
                and     a
                jr      z,.bol                  ; At beginning of line

                ; Move left
                dec     (hl)
.finish:
                jp      editorResetReqCol
.bol:
                ; Move to end of previous line
                ld      hl,(MainDoc.Line)
                ld      a,h
                or      l
                jp      z,editorResetReqCol       ; Can't move left any further if at first line

                ; Move to end of previous line
                call    editorCmdUp
                jp      editorCmdEnd

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor right
editorCmdRight:
                call    editorInBlockMode
                dw      editorReturn
                ld      hl,MainDoc.Col
                inc     (hl)                    ; move cursor right +1
                ld      a,(hl)
                call    editorClampPos          ; clamp the new position
                cp      (hl)
                jp      z,editorResetReqCol     ; if no clamp happened, new position is valid

                ; position was clamped -> cursor was already at end of line, advance to next line instead

                call    editorCmdHome
                jr      editorCmdDown

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor forward through words

editorCmdNextWord:
                ; First attempt to move forward in the current line
                ld      a,(MainDoc.Col)
                ld      de,EditorBuffer
                add     de,a                    ; DE = current position
                ld      a,(de)
                and     a
                ret     z
                inc     e                       ; Move once just in case we're already on the end of a word.

                ; Skip all non-label chars
.skip_next:
                ldi     a,(de)
                and     a
                jr      z,.eol
                call    isAlpha
                jr      c,.skip_next
                dec     e

                ; DE = pointer to terminating character
.skip_text:
                ldi     a,(de)
                call    isAlpha
                jr      nc,.skip_text
.eol:
                dec     e                       ; Point to last character
                ld      a,e
                ld      (MainDoc.Col),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor backwards through words

editorCmdPrevWord:
                ld      a,(MainDoc.Col)
                ld      de,EditorBuffer
                add     de,a
                ld      a,e
                and     a
                ret     z                       ; At beginning of line

.skip_prev:
                dec     e
                jr      z,.bol                  ; Reached the beginning of the line
                ld      a,(de)
                call    isAlpha
                jr      c,.skip_prev

.skip_text:
                dec     e
                jr      z,.bol
                ld      a,(de)
                call    isAlpha
                jr      nc,.skip_text
                inc     e
.bol:
                ld      a,e
                ld      (MainDoc.Col),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor up
editorCmdUp:
                call    editorUpdateLine
                call    editorSetReqCol         ; Update requested column if necessary
                call    editorGetPos            ; HL = addr of line, BC = position in doc

                ld      a,b
                or      c
                jr      z,.home

                ld      a,(MainDoc.ReqCol)
                ld      e,a
                dec     bc
.done:
                call    editorSetPos
                jp      editorBlockUpdate
.home:
                ld      bc,0
                ld      e,0
                call    editorResetReqCol
                jr      .done

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor down
editorCmdDown:
                call    editorUpdateLine
                call    editorNextLine
                jp      editorBlockUpdate

editorNextLine:
                call    editorSetReqCol         ; Update requested column if necessary
                call    editorGetPos            ; HL = addr of line, BC = position in doc
                ld      a,(MainDoc.ReqCol)
                ld      e,a
                inc     bc
                jp      editorSetPos

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor to beginning of line

editorCmdHome:
                call    editorInBlockMode
                dw      editorReturn
                xor     a
                ld      (MainDoc.Col),a
                jp      editorResetReqCol

;;----------------------------------------------------------------------------------------------------------------------
;; Move cursor to end of line

editorCmdEnd:
                call    editorInBlockMode
                dw      editorReturn
                ld      a,$FF
                call    editorClampPos
                ld      (MainDoc.Col),a
                jp      editorResetReqCol

;;----------------------------------------------------------------------------------------------------------------------
;; Delete character before cursor

editorCmdBackspace:
                call    editorInBlockMode
                dw      editorBlockDelete

                ; Make sure we're not at the start of the line
                ; TODO: Join two lines together if at beginning of line
                ld      hl,MainDoc.Col
                ld      a,(hl)
                and     a
                ret     z

                ;
                ; Just delete a character
                ;
                dec     (hl)
.delete_draw:
                ld      l,(hl)
                ld      h,high EditorBuffer     ; (HL) == character about to delete
                ld      a,(hl)
                cp      ' '                     ; Are we deleting a space character?
                jr      z,.delete_spaces
.delete_single:
                call    bufferDeleteChar
                jr      .draw

                ;
                ; Undeleting tabulations
                ;
.delete_spaces:
                ld      de,Tabs
.next_tab:
                ld      a,(de)
                inc     de
                dec     a
                jp      m,.delete_single        ; Not on tab position, so only delete single
                cp      l                       ; Same position as tab?
                jr      nz,.next_tab

                ; Ok, we need to keep deleting spaces
                ; HL = space we're about to delete
.next_space:
                call    bufferDeleteChar
                ld      a,l
                and     a
                jr      z,.finished             ; We deleted to beginning of line
                dec     l
                ld      a,(hl)
                cp      ' '                     ; Did we find another space
                jr      z,.next_space
                inc     l

.finished:
                ld      a,l
                ld      hl,MainDoc.Col
                ld      (hl),a

                ;
                ; Update screen
                ;
.draw:
                call    editorResetReqCol
                jp      editorDrawCurrentLine

;;----------------------------------------------------------------------------------------------------------------------
;; Delete a character under the cursor

editorCmdDelete:
                call    editorInBlockMode
                dw      editorBlockDelete
                ld      hl,MainDoc.Col
                jr      editorCmdBackspace.delete_draw

;;----------------------------------------------------------------------------------------------------------------------
;; Delete all characters from under the cursor till end of line

editorCmdDeleteToEnd:
                call    editorInBlockMode
                dw      editorReturn
                ld      hl,(MainDoc.Col)        ; L = cursor column
.l0:
                call    bufferDeleteChar
                jr      nz,.l0                  ; keep deleting till end of line
                jr      editorCmdBackspace.draw

;;----------------------------------------------------------------------------------------------------------------------
;; Formats the current line and enters new line

editorCmdFormatNewLine:
                call    editorInBlockMode
                dw      editorReturn
                call    editorCmdFormat

                ; Fall through

;;----------------------------------------------------------------------------------------------------------------------
;; Accepts the current line and goes to the beginning of a newly inserted line

editorCmdNewLine:
                call    editorInBlockMode
                dw      editorBlockNewline
                call    editorLeaveLine
                jr      c,.error

                ; Update buffer to see it changed from any tokenisation or reformatting
                call    editorEnterLine
                call    editorDrawCurrentLine

                ; calculate Auto-indent width from current line
                xor     a
                ld      (AutoIndentBy),a        ; reset auto-indent to zero
                ld      hl,(MainDoc.Addr)
                ld      a,(hl)
                cp      $21                     ; Are there spaces? ($0a to $20)
                jr      nc,.no_indent           ; $21..$FF -> no
                sub     $22
                cp      $0a-$22
                jr      c,.no_indent            ; $00..$09 -> no
                cpl                             ; A = number of spaces to fill for $0b..$20
                jr      nz,.not_0a_space
                inc     hl
                ld      a,(hl)                  ; read next byte for $0a case
.not_0a_space:
                ld      (AutoIndentBy),a        ; preserve amount of spaces to auto-indent new line
.no_indent:

                ; Insert new line after current (HL points inside current line)
                call    docNewLine              ; HL = start of new line
                jr      c,.error

                ; Update the triplets
                ld      (MainDoc.Addr),hl
                xor     a
                ld      (MainDoc.Col),a
                ld      de,(MainDoc.Line)
                inc     de
                ld      (MainDoc.Line),de

                ; Ensure visibility (will scroll + redraw full screen if needed with other side effects)
                call    editorEnsureVisible

                ; set up new line buffer + auto-indent (when no scrolling happened, it is not set yet)
                call    editorEnterLine
                ld      bc,(AutoIndentBy-1)     ; B = AutoIndentBy
                inc     b
                jr      .l0_entry
.l0:
                ld      l,0
                ld      a,' '
                call    bufferInsertChar
.l0_entry:
                djnz    .l0

                call    editorCmdEnd               ; move after the spaces, but clamp it to screen space (if some line has 80+ indentation)
                call    editorDrawAll
                jp      editorResetReqCol

.error:
                call    editorError
                dz      "Not enough memory to create a new line."
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Inserts new line above the current one

editorCmdNewLineAbove:
                call    editorInBlockMode
                dw      editorBlockNewline
.skip_block:
                call    editorLeaveLine
                jr      c,editorCmdNewLine.error
                call    editorEnterLine
                call    editorDrawCurrentLine

                ld      hl,(MainDoc.Addr)
                call    docNewLineAbove
                jr      c,editorCmdNewLine.error

                ; Update the triplets (MainDoc.Line doesn't need to change)
                ld      (MainDoc.Addr),hl
                xor     a
                ld      (MainDoc.Col),a

                call    editorEnterLine
                call    editorDrawAll
                jp      editorResetReqCol

;;----------------------------------------------------------------------------------------------------------------------
;; Insert spaces until we reach the next tabulation point.

Tabs            db      8,13,32,36,40,44,48,52,56,60,64,68,72,76,0

editorCmdTab:
                call    editorInBlockMode
                dw      editorBlockTab
.skip_block:
                ld      a,(MainDoc.Col)

                ; Find the next tabulation point
                ld      hl,Tabs
.l0:
                ld      c,(hl)          ; C = next tab point
                inc     c
                dec     c
                ret     z               ; No more tabs left
                cp      c               ; Current position < next tab point?
                jr      c,.found        ; Yes, we've found our next tab position
                inc     hl
                jr      .l0
.found:
                ; C = requested column
                cp      c
                jr      z,.done

                ld      l,a
                ld      a,' '
                call    bufferInsertChar
                ret     c               ; No room to insert any more spaces
                inc     l
                ld      a,l
                ld      (MainDoc.Col),a
                jr      .found

.done:
                jp      editorResetReqCol

;;----------------------------------------------------------------------------------------------------------------------
;; Copy the selection into the clipboard

editorCmdBlockCopy:
                call    editorBlockCopySelection
                jp      editorCmdCancel

;;----------------------------------------------------------------------------------------------------------------------
;; Copy the current line or block into the clipboard

editorCmdCopy:
                call    editorLeaveLine
                call    editorInBlockMode
                dw      editorCmdBlockCopy

                ld      hl,(MainDoc.Addr)
                ld      de,hl
                call    strEnd
                inc     hl                  ; DE = start of line, HL = end of line
                and     a
                sbc     hl,de               ; HL = number of bytes
                call    clipCopy

                ; Fallthrough

;;----------------------------------------------------------------------------------------------------------------------
;; editorTestCopy
;; Test that the copy succeeded and display appropriate message
;;
;; INPUT:
;;      CF = 1 if error occurred
;;
;; OUTPUT:
;;      CF is maintained
;;

editorTestCopy:
                jr      nc,.fits

                call    editorError
                dz      "Out of memory while copying to clipboard"
                scf
                ret

.fits:
                call    editorMsg
                dz      "Text copied into clipboard"
                xor     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Delete the current line and put it in the clipboard

editorCmdCut:
                call    editorLeaveLine

                ; If we're in block mode, it's the same as delete.
                call    editorInBlockMode
                dw      editorCmdDelete

                ; Else we just copy the line and delete it
                call    editorCmdCopy
                ret     c               ; copy failed
                ld      hl,(MainDoc.Addr)
                call    docDeleteLine
                jr      nc,.end

                ; A line change occurred
                ld      (MainDoc.Addr),hl
                ld      hl,(MainDoc.Line)
                dec     hl
                ld      (MainDoc.Line),hl

.end:
                xor     a
                ld      (MainDoc.Col),a
                jp      editorLinesChanged

;;----------------------------------------------------------------------------------------------------------------------
;; Insert the line stored on the clipboard

editorCmdPaste:
                call    editorLeaveLine

                ; Make sure we have space
                ld      hl,(MainDoc.Addr)
                call    clipSize
                call    docInsertSpace
                jr      c,.oom

                ; HL = start of space
                ex      de,hl
                call    clipPaste
                jp      editorLinesChanged

.oom:
                call    editorError
                dz      "No space in document to paste text"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Duplicate the current line and move to the next line

editorCmdDupLine:
                call    editorUpdateLine

                ld      hl,(MainDoc.Addr)
                ld      de,hl       ; current line will work as source data for docInsertLine
                call    strEnd      ; can't use docNextLine (would fail at last line ahead of EOF marker)
                inc     hl          ; next line pointer or EOF marker, insert new line ahead of it
                call    docInsertLine
                jr      c,.error

                call    editorCmdDown
                jp      editorLinesChanged
.error:
                call    editorError
                dz      "No space in document to duplicate line"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Move down one page

editorCmdPageDown:
                call    editorUpdateLine
                call    editorSetReqCol
                call    editorGetPos

                ; Check to see if we're close to the greatest possible line
                ld      hl,65535-(kScreenHeight-2)
                and     a
                sbc     hl,bc                   ; MaxLine < Current line
                jr      nc,.page
                ld      bc,$ffff-(kScreenHeight-2)

.page:
                add     bc,kScreenHeight-2
                call    editorSetPos
                jp      editorBlockUpdate

;;----------------------------------------------------------------------------------------------------------------------
;; Move up one page

editorCmdPageUp:
                call    editorUpdateLine
                call    editorSetReqCol
                call    editorGetPos

                ; Check to see if we're close to the top
                ld      hl,kScreenHeight-2
                and     a
                sbc     hl,bc                   ; MinLine < CurrentLine
                jr      c,.page                 ; Yes? Continue
                ld      bc,kScreenHeight-2

.page:
                add     bc,-(kScreenHeight-2)
                call    editorSetPos
                jp      editorBlockUpdate

;;----------------------------------------------------------------------------------------------------------------------
;; Move to the start of the document

editorCmdStartDoc:
                call    editorUpdateLine
                call    editorResetReqCol
                ld      bc,0
                ld      e,0
                call    editorSetPos
                jp      editorBlockUpdate

;;----------------------------------------------------------------------------------------------------------------------
;; Move to the end of the document

editorCmdEndDoc:
                call    editorUpdateLine
                call    editorResetReqCol
                ld      bc,$ffff
                ld      e,0
                call    editorSetPos
                jp      editorBlockUpdate

;;----------------------------------------------------------------------------------------------------------------------
;; Save the document if it is dirty

editorCmdSave:
                call    editorUpdateLine
                ld      hl,MainDoc.Flags
                bit     0,(hl)
                ret     z

                ld      a,(MainDocName)
                and     a
                jr      nz,.named

                call    editorError
                dz      "Cannot save as current file does not have a filename."
                ret
.named:
                call    docSave
                jr      c,.error

                call    editorMsg
                dz      "Saved"
                ret
.error:
                call    editorError
                dz      "Unable to save file"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Will attempt to close document only if clean, otherwise prompt for save or close

editorCmdClose:
                call    cursorOff
                call    editorUpdateLine
                call    docTryClose
                jr      nc,.show_new_doc

                ; Current document is dirty!
                call    editorMsg
                dz      "File not saved: Are you sure? (Y/N)"

.l0:
                call    waitKey
                call    upperCase
                cp      'N'
                jr      z,.no
                cp      'Y'
                jr      nz,.l0
                call    docClose
.show_new_doc:
                call    docExists
                jr      nc,.no_new
                call    docNewNoName
.no_new:
                jp      editorCmdSwitch.done

.no:
                ld      a,kInkNormal<<1
                jp      editorClearStatus

;;----------------------------------------------------------------------------------------------------------------------
;; Enter block mode to be able to select multiple contiguous lines.

editorCmdBlockMode:
                call    editorInBlockMode
                dw      editorReturn

                call    cursorOff

                ld      hl,(MainDoc.Line)
                ld      (MainDoc.BlockStart),hl
                ld      (MainDoc.BlockEnd),hl
                ld      a,1
                ld      (MainDoc.InBlock),a
                call    editorUpdateLine
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Cancel block mode if inside it.

editorCmdCancel:
                ld      a,(MainDoc.InBlock)
                and     a
                ret     z

                xor     a
                ld      (MainDoc.InBlock),a
                jp      editorDrawAll

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll the screen up

editorScrollUp:
                ld      bc,(MainDoc.TopLine)
                ld      a,b
                or      c
                ret     z                       ; Already at the top

                dec     bc
                call    docFindLine
                ret     c

                ld      (MainDoc.TopLine),bc
                ld      (MainDoc.TopAddr),hl

                ; Ensure the cursor is still on screen
                ld      hl,(MainDoc.Line)
                sbc     hl,bc                   ; HL = line on screen
                ld      de,kScreenHeight-2
                call    compare16
                jr      c,.draw

                call    editorLeaveLine
                ld      bc,(MainDoc.Line)
                dec     bc
.updateLine:
                call    docFindLine
                ld      (MainDoc.Line),bc
                ld      (MainDoc.Addr),hl
                call    editorEnterLine
                call    editorBlockUpdate
.draw
                jp      editorDrawAll

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll the screen down

editorScrollDown:
                ld      bc,(MainDoc.TopLine)
                call    docFindLine
                call    docNextLine
                ret     c                       ; Cannot scroll down any further

                inc     bc
                ld      (MainDoc.TopLine),bc
                ld      (MainDoc.TopAddr),hl

                ; Ensure the cursor is still on screen
                ld      hl,(MainDoc.Line)
                ld      de,bc                   ; Line < Top Line
                call    compare16
                jr      nc,editorScrollUp.draw

                call    editorLeaveLine
                ld      bc,(MainDoc.Line)
                inc     bc
                jr      editorScrollUp.updateLine

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
;; B L O C K   M A N A G E M E N T
;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockUpdate
;; Update the block positions to match the current line if we're in block mode
;;

editorBlockUpdate:
                call    editorInBlockMode
                dw      .in_block_mode
                ret 
.in_block_mode:
                ld      hl,(MainDoc.Line)
                ld      (MainDoc.BlockEnd),hl
                jp      editorDrawAll

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockDelete
;; Delete the lines and exit block mode.  If not enough memory to copy lines, will cancel operation and return CF=1.
;;
;; OUTPUT:
;;      CF = 1 if out of memory to copy lines

editorBlockDelete:
                ld      hl,(MainDoc.BlockStart)
                ld      de,(MainDoc.BlockEnd)
                call    max                         ; DE = start line, HL = end line
                call    editorBlockCopy
                inc     hl
                
                ; DE = line number of start of block
                ; HL = line number after end of block
                ret     c                           ; Not enough memory to delete blocks
                xor     a
                ld      (MainDoc.InBlock),a
                sbc     hl,de                       ; HL = number of lines
                ex      de,hl                       ; DE = number of lines, HL = first line

                call    editorGotoLine
                ld      hl,(MainDoc.Addr)

.next_line:
                call    docDeleteLine
                jr      c,.deleted_last_line
                dec     de
                ld      a,d
                or      e
                jr      nz,.next_line
                jp      editorLinesChanged

.deleted_last_line:
                ld      (MainDoc.Addr),hl
                ld      hl,(MainDoc.Line)
                dec     hl
                ld      (MainDoc.Line),hl
                call    editorLinesChanged
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; editorInBlockMode
;; Test to see if we're in block mode, if so, jump to the address following the call.
;;

editorInBlockMode:
                ex      (sp),hl
                push    af
                ld      a,(MainDoc.InBlock)
                and     a
                jr      nz,.in_block_mode

                pop     af
                inc     hl
                inc     hl
                ex      (sp),hl
                ret 
.in_block_mode:
                ldhl
                pop     af
                inc     sp
                inc     sp
                jp      (hl)

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockCopySelection
;; Copies the lines as determined by the current selection

editorBlockCopySelection:
                ; This is called if in block mode.  Figure out DE, HL and fallthrough.
                ld      hl,(MainDoc.BlockStart)
                ld      de,(MainDoc.BlockEnd)
                call    max
                
                ; Fallthrough

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockCopy
;; Copy lines to clipboard.
;;
;; INPUT:
;;      DE = first line
;;      HL = last line       

editorBlockCopy:
                push    de,hl

                ; Calculate address of actual lines
                ld      bc,hl
                call    docFindLine
                ex      de,hl           ; DE = last line addr

                ld      bc,hl
                call    docFindLine     ; HL = first line addr

.find_end
                ldi     a,(de)
                and     a
                jr      nz,.find_end

                ; HL = address of first line
                ; DE = address of byte after last line
                ex      de,hl
                sbc     hl,de           ; DE = address of first line, HL = number of bytes
                call    clipCopy
                pop     hl,de
                jp      editorTestCopy

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockNewline
;;

editorBlockNewline:
                call    editorBlockDelete
                jp      editorCmdNewLineAbove.skip_block

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockInsertChar
;;

editorBlockInsertChar:
                push    af
                call    editorBlockDelete
                call    editorCmdNewLineAbove.skip_block
                pop     af
                jp      editorInsertChar.skip_block

;;----------------------------------------------------------------------------------------------------------------------
;; editorBlockTab
;;

editorBlockTab:
                call    editorBlockDelete
                call    editorCmdNewLineAbove.skip_block
                jp      editorCmdTab.skip_block

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
