;;----------------------------------------------------------------------------------------------------------------------
;; Clipboard management

ClipPages       ds      6,0
ClipSize        dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; clipCopy
;; Overwrite the clipboard if new bytes (up to 48K)
;;
;; INPUT:
;;      DE = offset into document to start copy
;;      HL = number of bytes to copy
;;

clipCopy:
                push    de,hl
                call    clipClear
                call    pageDoc
                call    pagesStore
                pop     hl,de
                ret     c
                push    bc,de,hl
                ld      (ClipSize),hl       ; Store amount of bytes
                ld      hl,PageBuffer
                ld      de,ClipPages
                ld      bc,6
                ldir                        ; Store page indexes.
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; clipPaste
;; Copy the contents of the clipboard to a buffer.
;;
;; INPUT:
;;      DE = offset in document to write to.
;;

clipPaste:
                push    de,hl,bc
                call    pageDoc

                ; Transfer pages to PageBuffer
                ld      de,PageBuffer
                ld      hl,ClipPages
                ld      bc,6
                ldir
                pop     bc,hl,de

                ld      hl,(ClipSize)
                call    pagesRestore
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; clipSize
;; Get the number of bytes stored in the clipboard
;;
;; OUTPUT:
;;      BC = number of bytes
;;

clipSize:
                ld      bc,(ClipSize)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; clipClear
;; Clear the clipboard
;;

clipClear:
                push    bc,hl
                ld      hl,ClipPages
                ld      b,6
                call    freePages
                ld      bc,8
                call    memclear
                pop     hl,bc
                ret


;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------