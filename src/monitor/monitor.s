;;----------------------------------------------------------------------------------------------------------------------
;; Editor system
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------

OdinLogo:
                ; Line 1
                dz      6,'  ',$80,$81,C_ENTER
                ; Line 2
                dz      7,$82,$83
                dz      6,$84
                dz      7,$85,$86,$87
                dz      2,$88,"    EDITOR/ASSEMBLER",C_ENTER
                ; Line 3
                dz      7,$89,$8a
                dz      6,$8b
                dz      7,$8c,$8d,$8e,$8f,$90,$91,"  Version Dev.2k",C_ENTER
                ; Line 4
                dz      7,$92,$93
                dz      6,$94
                dz      7,$95,$96,$97,$98,$99,$9a,"  Copyright ",127,"2021 Matt Davies, all rights reserved",C_ENTER
                dz      6,' ',$9b
                ; Line 5
                dz      7,"         Additional coding, optimisation and testing by Peter Helcmanovsky",C_ENTER
                dz      7,"           Logo designed by Phoebus Dokos",C_ENTER
                ; Rest of greeting
                dz      6,C_ENTER
                dz      4,"Type '.guide odin' at the command line for the manual.",C_ENTER
                dz      4,"Press H for a list of commands and Ext+Q to quit.",C_ENTER,C_ENTER
                dz      4,"For any feedback or bug reporting, please go to:",C_ENTER
                dz      5,"https://gitlab.com/next-tools/odin/-/issues",C_ENTER
                dz      4,C_ENTER,"Press "
                dz      7,"EDIT"
                dz      4," to enter and leave the fullscreen editor",C_ENTER
                dz      C_ENTER,C_ENTER
                ; END
                db      0

;;----------------------------------------------------------------------------------------------------------------------
;; Editor/Monitor entry point
;;----------------------------------------------------------------------------------------------------------------------

monMain:
                ;;
                ;; Set up
                ;;

                ; Disable divMMC
                in      a,($e3)
                ld      (DivMMCPage),a
                call    pageOutDivMMC

                ; Calculate the highest memory page
                ; This code and docInit requires the system vars to be still paged in.
                call    calcHiPage

                call    docInit                 ; Initialise the document system
                jr      nc,.all_good
                ld      a,(docInit.error_occurred)
                and     a
                jp      z,exit.oom              ; An error occurred due to out of memory
.all_good:
                call    initConsole
                call    initKeys

                ; Output Odin Logo
                call    pageVideo
                ld      hl,OdinLogo
.l1:
                ldi     a,(hl)
                and     a
                jr      z,.done
                call    setColour
                call    printHL
                jr      .l1
.done:
                ld      a,kInkNormal
                call    setColour
                call    cursorOn

                ; Let's check the loading error flag again and output a mesage if all is not good.
                ld      a,(docInit.error_occurred)
                and     a
                jr      z,mainLoop

                call    errorPrint
                dz      "ERROR: Unable to load all documents"

                ;;
                ;; Main loop
                ;;

mainLoop:
                call    pageVideo               ; Page in the video
.waitKey
                call    consoleUpdate
                call    inKey
                jr      z,.waitKey

                cp      VK_EDIT
                jp      nz,.no_editor

                ld      hl,editorLoop
                ld      a,(EditorCode)
                call    farCall
                jr      mainLoop

.no_editor:
                ; Process key press
                cp      VK_EXTQ
                jp      nz,.no_exit

                ; Try to close all the documents
.close_loop:
                call    docExists
                jp      c,exit
                call    docTryClose
                jr      nc,.close_loop

                ; A dirty document exists
                call    errorPrint
                dz      "UNABLE TO EXIT BECAUSE A DOCUMENT IS NOT SAVED."
                call    errorPrint
                dz      "SAVE OR CLOSE DOCUMENT IN EDITOR."
                jp      mainLoop

.no_exit:
                call    cursorHide
                call    printChar
                call    cursorShow

                cp      C_ENTER                 ; Did we press Enter?
                jp      nz,.waitKey             ; No, keep the input coming!

                ; Get input from line
                ld      hl,(CurrentPos)
                ld      de,160
                sbc     hl,de                   ; HL points to line that contains input
                ld      de,InputBuffer
                ld      b,81

                ; Skip past any whitespace?
.skip_ws:
                dec     b
                ld      a,b
                and     a
                jp      z,mainLoop

                ld      a,(hl)
                inc     hl
                inc     hl
                and     a
                jr      z,.skip_ws
                cp      $20
                jr      z,.skip_ws
                dec     hl
                dec     hl

.l2:
                ldi     a,(hl)
                and     a
                jr      nz,.copy
                ld      a,' '
.copy:          ldi     (de),a
                inc     hl
                djnz    .l2

                ; Convert all trailing spaces to nulls
.l3:
                dec     de
                ld      a,(de)
                cp      ' '
                jr      nz,.done_input
                xor     a
                ld      (de),a
                jr      .l3
.done_input:
                ;
                ; Start processing input
                ;
                ld      de,InputBuffer

                ; Is it a line number or null terminator
                ld      a,(de)
                and     a
                jp      z,mainLoop

                ;
                ; Handle command
                ;

                ; Is it a possible command?
                ex      de,hl
                call    strSkipWS               ; Skip past any leading whitespace
                call    isAlpha
                jr      c,error

                ; A = command letter
                ; Figure out if it's a command and the call the handler
                call    cmdHandle
                call    resetKeys               ; Enables interrupts as cmdHandle disables them
                ld      a,kInkNormal
                call    setColour
                jp      mainLoop

                ;;
                ;; Main Loop for Editor
                ;;

mainLoop_editor:
                jp      mainLoop

error:
                call    errorPrint
                dz      "ERROR"
                jp      mainLoop


                ;;
                ;; Exit
                ;;

exit:
                ld      a,(AsmCode)
                ld      hl,asmDone
                call    farCall
                call    docDone
                call    doneKeys
                call    doneConsole
                call    waitNoKey
.oom:
                call    pageDivMMC
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
