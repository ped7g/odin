;;----------------------------------------------------------------------------------------------------------------------
;; Odin Assembler
;; Copyright (C)2020-2021 Matt Davies, all right reserved.
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE          ZXSPECTRUMNEXT
                CSPECTMAP       "odin.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Memory map
;;
;;      +---------------------------------------+ 0000
;;      | Document Page 1                       |
;;      +---------------------------------------+ 2000
;;      | Document Page 2                       |
;;      +---------------------------------------+ 4000
;;      | Document Page 3                       |
;;      +---------------------------------------+ 6000
;;      | Document Page 4                       |
;;      +---------------------------------------+ 8000
;;      | Document Page 5                       |
;;      +---------------------------------------+ a000
;;      | Document Page 6                       |
;;      +---------------------------------------+ c000
;;      | Shared code and stack  | Workspace    |
;;      +---------------------------------------+ e000
;;      | Paged code                            |
;;      +---------------------------------------+
;;

;;----------------------------------------------------------------------------------------------------------------------
;; Includes

                include "src/shared/defines.s"
                include "src/shared/consts.s"

                ;;
                ;; Shared code
                ;;

                MMU     6,20,$c000

                include "src/shared/keyboard.s"
                include "src/shared/console.s"
                include "src/shared/document.s"
                include "src/shared/filesys.s"
                include "src/shared/maths.s"
                include "src/shared/memory.s"
                include "src/shared/page.s"
                include "src/shared/string.s"
                include "src/shared/tokens.s"
                include "src/shared/utils.s"
                include "src/shared/vars.s"
                include "src/shared/args.s"

                defcallhl
                defcallix

                display "[ SHARED ] Final address (Max = $dcff): ",$," Space: ",$dd00-$

                org     $dd00
AsmBuffer
KeyboardBuffer  ds      256

TokenisedBuffer                         ; Buffer used for tokenising line while using the editor
ExprBuffer                              ; Used for calculating expressions during assembly
FileBuffer      ds      256             ; Used for loading and saving

Stack           ds      256

                SAVEBIN "odin_shared", $c000, $2000

                ;;
                ;; Monitor
                ;;

                MMU     7,21,$e000

MonitorBuffer   ds      256             ; must be 256 aligned and not at end of RAM (!$FF00)

                include "src/monitor/commands.s"
                include "src/monitor/monitor.s"

                display "[MONITOR ] Final address (Max = $ffff): ",$," Space: ",$10000-$

                SAVEBIN "odin_monitor", $e000, $2000

                ;;
                ;; Editor
                ;;

                MMU     7,22,$e000

EditorBuffer    ds      256             ; must be 256 aligned and not at end of RAM (!$FF00)

                include "src/editor/buffer.s"
                include "src/editor/cmdtable.s"
                include "src/editor/editor.s"
                include "src/editor/switch.s"
                include "src/editor/clipboard.s"

                display "[ EDITOR ] Final address (Max = $ffff): ",$," Space: ",$10000-$

                SAVEBIN "odin_editor", $e000, $2000

                ;;
                ;; Assembler
                ;;

                MMU     7,23,$e000

                include "src/asm/symbols.s"
                include "src/asm/lex.s"
                include "src/asm/expr.s"
                include "src/asm/ld.s"
                include "src/asm/asm.s"
                include "src/asm/emit.s"
                include "src/asm/opt.s"
                include "src/asm/table.s"

                display "[  ASM   ] Final Address (Max = $ffff): ",$," Space: ",$10000-$
                SAVEBIN "odin_asm", $e000, $2000

                ;;
                ;; Debugger
                ;;

                MMU     0,24,$0000

                include "src/debugger/debugger.s"

                display "[DEBUGGER] Final Address (Max = $1fff): ",$," Space: ",$2000-$
                SAVEBIN "odin_dbg", $0000, $2000

;;----------------------------------------------------------------------------------------------------------------------
;; Start

                ORG     $2000

Start:
                ;;------------------------------------------------------------------------------------------------------
                ;; Set up the application
                ;;------------------------------------------------------------------------------------------------------

                ld      (OldSP),sp
                ld      sp,$3f80

                ; Prepare for the start of the DOT command now the stack is set up
                call    dotStart

                ;;------------------------------------------------------------------------------------------------------
                ;; Load in the other sections of code
                ;;------------------------------------------------------------------------------------------------------

                ; Allocate memory to receive code
                ld      b,NUM_CODE_PAGES
                ld      hl,SharedPage
.loop_alloc:
                call    bootAlloc
                jr      nc,.no_alloc
                ldi     (hl),a
                djnz    .loop_alloc

                ; Install the shared page at MMU 6
                ld      hl,SharedPage
                ld      a,6
                call    installCode

                ; Install all the other code pages at MMU 7
                ld      b,NUM_CODE_PAGES-2      ; Don't include shared and data
                inc     a
.loop_install:
                call    installCode
                djnz    .loop_install

                ; Load in the font
                dos     M_GETHANDLE
                ld      hl,$6000
                ld      bc,$800
                dos     F_READ

                ; Copy the page #s out of DivMMC memory
                ld      hl,SharedPage
                ld      de,SharedCode
                ld      bc,NUM_CODE_PAGES
                ldir

                ; Set up any fake keys here
                ld      hl,TestKeys
                ld      (FakeKeys),hl

                ; Jump into the monitor code
                ld      a,(MonitorPage)
                page    7,a
                ld      sp,$e000
                ld      hl,(Args)

                call    monMain

                ;;------------------------------------------------------------------------------------------------------
                ;; Shut down
                ;;------------------------------------------------------------------------------------------------------

.quit:

                ; Free up memory used by code
                ld      sp,$4000
.no_alloc:
                ld      b,NUM_CODE_PAGES
                ld      hl,SharedPage
.loop_dealloc:
                ldi     a,(hl)
                and     a
                jr      z,.invalid_alloc
                call    bootFree
.invalid_alloc:
                djnz    .loop_dealloc

                DetectLeaks
                jp      dotEnd

;;----------------------------------------------------------------------------------------------------------------------
;; installCode
;; Loads the next 8K from the dot command and installs it in a certain page.
;;
;; Input:
;;      HL = Address of byte containing next page to install into
;;      A = next register for MMU
;;      BC = Length of code
;;
;; Output:
;;      HL = Address of next byte containing next page to install into
;;
;; NOTE: Pages are not restored
;;

installCode:
                push    af,bc,de,hl

                ; Calculate DE to start of MMU
                ld      d,$20
                ld      e,a
                mul                     ; E = MSB of address
                ld      d,e
                ld      e,0

                ; Page in correct page to load into
                add     a,REG_MMU0
                ld      (.smc0),a
                ld      a,(hl)
.smc0           equ     $+2
                reg     0,a

                dos     M_GETHANDLE     ; Get file handle of dot command
                ex      de,hl
                ld      bc,$2000
                dos     F_READ

                pop     hl,de,bc,af
                inc     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Memory access routines

bootAlloc:
                ; Allocate a page by using the OS function IDE_BANK.
                ; CF = 1 if successful
                ;
                push    bc,de,hl

                ld      hl,$0001        ; Select allocate function and allocate from normal memory.
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7             ; We want RAM 7 swapped in when we run this function (so that the OS can run).
                rst     8
                db      M_P3DOS         ; Call the function, new page # is in E
                ld      a,e

                pop     hl,de,bc
                ret

bootFree:
                ; Free a page
                ; A = page #
                push    af,bc,de,hl
                ld      e,a
                ld      hl,$0003        ; Deallocate function from normal memory
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7
                rst     8
                db      M_P3DOS
                pop     hl,de,bc,af
                ret

SharedPage      db      0
MonitorPage     db      0
EditorPage      db      0
AsmPage         db      0
DbgPage         db      0
DataPage        db      0

;;----------------------------------------------------------------------------------------------------------------------
;; Bootstrap code

                include "src/shared/dot.s"

;;----------------------------------------------------------------------------------------------------------------------
;; End of code

                display "[  BOOT  ] Final address (Max = $3dff): ",$," Space: ",$4000-$

;;----------------------------------------------------------------------------------------------------------------------
;; Buffers

                org     $3e00

                ds      256             ; Stack

;;----------------------------------------------------------------------------------------------------------------------
;; Data

                ORG     $0000

                incbin  "data/topan.fnt"
                incbin  "data/logo.fnt"

;;----------------------------------------------------------------------------------------------------------------------
;; Binary file generation

                SAVEBIN "odin_data", $0000, $800
                SAVEBIN "odin_boot", $2000, $2000

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
