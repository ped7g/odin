;;----------------------------------------------------------------------------------------------------------------------
;; Math routines
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; mul_16_8_24
;; Multiply a 16-bit value by an 8-bit value to get a 24-bit value
;;
;; Input:
;;      HL = 16-bit value
;;      A = 8-bit value
;;
;; Output:
;;      EHL = 24-bit value
;;

mul_16_8_24:
                ld      d,l
                ld      e,a
                mul     de
                ex      de,hl   ; HL = L*A
                ld      e,a
                mul     de      ; DE = H*A
                ld      a,h
                add     de,a    ; result in DEL
                ld      h,e     ; move it to EHL
                ld      e,d
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; muladd_32_8_8_40
;; Multiple a 32-bit value by an 8-bit value and add an 8-bit value to get a 40-bit result (SIZE optimised)
;;
;; Input:
;;      DEHL = 32-bit value
;;      C = 8-bit value to multiply by
;;      A = 8-bit value to add to result
;;
;; Output:
;;      ADEHL = 40-bit product
;;
;; Affects:
;;      AF,BC
;;

muladd_32_8_8_40:
                ; do all four segments: DEHL * C = ADEHL with adding initial A as 8bit add-value
                call    .do_two ; do two segments (HL * C)
                ; do remaining two segments (DE * C)
.do_two:
                call    .do_one ; do two segments (call + fallthrough)
.do_one:
                ld      b,d     ; B = D
                ld      d,l     ; D = next 8bits of multiplier (at bottom of current DEHL)
                ld      l,h     ; shift result DEH down to BHL (by 8) (B already set)
                ld      h,e
                ld      e,c     ; arg2
                mul     de      ; DE = arg1_8bit_part * arg2
                add     de,a    ; DE adjusted with overflow from previous sub-multiplication
                ld      a,d     ; A = next overflow to add or top byte of result
                ld      d,e
                ld      e,b     ; result = ADEHL
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; mul_24_24_24
;; Multiply a 24-bit value by an 24-bit value to get a 24-bit value
;;
;; Input:
;;      HLE = 24-bit value
;;      BCD = 24-bit value
;;
;; Output:
;;      HLE = 24-bit value
;;
;; Taken from Ped7g's ZXSpectrumNextMisc snippets project: https://github.com/ped7g/ZXSpectrumNextMisc
;;

mul_24_24_24:
                push    de      ; y0,x0
                ld      ixl,d   ; preserve y0
                ld      d,b     ; DE = y2x0, B released
                ld      b,e     ; preserve x0
                mul     de      ; x0y2 = +r2
                ld      a,e
                ld      d,b
                ld      e,c
                mul     de      ; x0y1 = +r1
                add     a,d
                ld      d,c     ; C released
                ld      c,e
                ld      e,l
                mul     de      ; x1y1 = +r2
                add     a,e     ; AC = truncated(x*y1 + (x*y2<<8))
                ld      d,h
                ld      e,ixl
                ld      h,e
                mul     de      ; x2y0 = +r2
                add     a,e
                ld      d,a     ; x2y0 added to future top byte (currently B)
                ld      e,c
                ex      de,hl
                mul     de      ; x1y0 = +r1
                add     hl,de   ; HL = upper 16 bits of result without x0y0 overflow
                pop     de
                mul     de      ; x0y0 = +r0
                ld      a,d
                add     hl,a    ; HLE = 24b result
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; unpackD24
;; Convert a 24-bit value into BCD
;;
;; Input:
;;      EHL = 24-bit value
;;
;; Output:
;;      DEHL = BCD of value in 8-digit form
;;
;; Affects:
;;      AF, DE, HL
;;

unpackD24:
                push    bc,ix
                ld      c,e
                push    hl
                pop     ix              ; CIX = 24-bit value
                ld      hl,0
                ld      d,h
                ld      e,h
                ld      b,24            ; 24 bits to process, DEHL = 00000000

.l1:        ; Fast loop until first non-zero bit is found
                add     ix,ix
                rl      c               ; Push top bit of CIX into carry
                jr      c,.l2_entry
                djnz    .l1             ; Find highest 1-bit
                jr      .exit           ; All bits are 0

.l2:        ; Slow loop BCD-shifting-left result by one + top bit of CIX
                add     ix,ix
                rl      c               ; Push top bit of CIX into carry
.l2_entry:  ; BCD left shift by one and add initial carry
                ld      a,l
                adc     a,a
                daa
                ld      l,a
                ld      a,h
                adc     a,a
                daa
                ld      h,a
                ld      a,e
                adc     a,a
                daa
                ld      e,a
                ld      a,d
                adc     a,a
                daa
                ld      d,a
                djnz    .l2
.exit:
                pop     ix,bc
                ret

/*
; NOTE: mul_16_16_32 and mul_32_32_32 are commented out as they are currently not used (replaced by mul_24_24_24)

;;----------------------------------------------------------------------------------------------------------------------
;; mul_16_16_32
;; Multiply two 16-bit values to get a 32-bit value
;;
;; Input:
;;      HL & HL' = 16 bit values (X&Y)
;;
;; Output:
;;      DEHL = 32-bit product
;;      CF = 0
;;
;; Uses:
;;      AF, BC, DE, HL, BC', DE', HL'
;;
;; Taken from Z88DK source code
;;

mul_16_16_32:
                ; HL & DE become the values
                push    hl
                exx
                pop     de

                ; HL = X
                ; DE = Y

mul_DE_HL_DEHL:
                ld      b,l     ; B = X0
                ld      c,e     ; C = Y0
                ld      e,l     ; E = X0
                ld      l,d     ; L = Y1
                push    hl
                ld      l,c     ; L = Y0

                ; BC = X0 Y0
                ; DE = Y1 X0
                ; HL = X1 Y0
                ; STACK = X1 Y1

                mul             ; Y1*X0
                ex      de,hl
                mul             ; X1*Y0

                xor     a
                add     hl,de   ; Sum cross products
                adc     a,a

                ld      e,c     ; X0
                ld      d,b     ; Y0
                mul             ; X0*Y0

                ld      b,a
                ld      c,h

                ld      a,d
                add     a,l
                ld      h,a
                ld      l,e

                pop     de
                mul

                ex      de,hl
                adc     hl,bc
                ex      de,hl

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; mul_32_32_32
;; Multiply 2 unsigned 32-bit values into a 32-bit product.
;;
;; Input:
;;      DEHL = value 1
;;      DE'HL' = value 2
;;
;; Output:
;;      DEHL = 32-bit product
;;      CF = 0
;;
;; Uses:
;;      AF, BC, DE, HL, BC', DE', HL'
;;
;; Taken from Z88DK source code
;;

mul_32_32_32:
                ; If DE and DE' are both 0, use the 16x16 version.
                xor     a
                or      e
                or      d

                exx
                or      e
                or      d
                jr      z,mul_16_16_32

                push    hl
                exx
                ld      c,l
                ld      b,h
                pop     hl
                push    de
                ex      de,hl
                exx
                pop     bc

                ; DEDE' = 32-bit X
                ; BCBC' = 32-bit Y
                push    de      ; X3 X2
                exx
                push    bc      ; Y1 Y0
                push    de      ; X1 X0
                exx
                push    bc      ; Y3 Y2

                ; Save material for the byte p2 = x2*y0 + x0*y2 + x1*y1 + p1 carry

                ld      h,e
                ld      l,c
                push    hl      ; X2 Y2

                exx             ; Working in low order bytes
                ld      h,e
                ld      l,c
                push    hl      ; X0 Y0

                ld      h,d
                ld      l,b
                push    hl      ; X1 Y1

                ld      h,d     ; X1
                ld      d,b     ; Y1
                ld      l,c     ; Y0
                ld      b,e     ; X0

                ; BC = X0 Y0
                ; DE = Y1 X0
                ; HL = X1 Y0
                ; STACK = X1 Y1

                mul             ; Y1*X0
                ex      de,hl
                mul             ; X1*Y0

                xor     a
                add     hl,de
                adc     a,a

                ld      e,c     ; X0
                ld      d,b     ; Y0
                mul             ; Y0*X0

                ld      b,a
                ld      c,h

                ld      a,d
                add     a,l
                ld      h,a
                ld      l,e     ; LSW in HL p1 p0

                pop     de
                mul             ; X1*Y1

                ex      de,hl
                adc     hl,bc   ; HL = interim MSW p3 p2
                ex      de,hl   ; DEHL = end of 16x16->32

                push    de      ; Stack interim p3 p2

                ; Continuing doing the p2 byte

                exx             ; Now we're working in the high-order bytes
                                ; DE'HL' = end of 16x16->32
                pop     bc      ; Stack interim p3 p2

                pop     hl      ; X0 Y0
                pop     de      ; X2 Y2
                ld      a,h
                ld      h,d
                ld      d,a
                mul             ; X0*Y2
                ex      de,hl
                mul             ; X2*Y0

                add     hl,bc
                add     hl,de
                ld      b,h
                ld      c,l

                ; Start doing the p3 byte

                pop     hl      ; Y3 Y2
                pop     de      ; X1 X0
                ld      a,h
                ld      h,d
                ld      d,a
                mul             ; Y3*X0
                ex      de,hl
                mul             ; X1*Y2

                ld      a,b
                add     a,e
                add     a,l

                pop     hl      ; Y1 Y0
                pop     de      ; X3 X2
                ld      b,h
                ld      h,d
                ld      d,b
                mul             ; Y1*X2
                ex      de,hl
                mul             ; X3*Y0

                add     a,l
                add     a,e
                ld      b,a

                push    bc

                exx
                pop     de
                xor     a
                ret
*/

;;----------------------------------------------------------------------------------------------------------------------
;; divs_32_32_32
;; Division of two 32-bit signed numbers to create 32-bit quotient (unsigned) and 32-bit remainder (signed).
;;
;; Input:
;;      DE'HL' = 32-bit dividend
;;      DEHL = 32-bit divisor
;;
;; Output:
;;      Success:
;;              CF = 0
;;              DEHL = 32-bit quotient
;;              DE'HL' = 32-bit remainder
;;
;;      Divide by zero:
;;              CF = 1
;;
;; Uses:
;;      AF, BC, DE, HL, BC', DE', HL'
;;
;; Taken from Z88DK source code
;;

divs_32_32_32:
                ld      a,d
                or      e
                or      h
                or      l
                scf
                ret     z

                ; A = (A/B)*B + A%B

                ld      a,d

                exx

                ld      b,d             ; B = MSB of dividend
                ld      c,a             ; C = MSB of divisor

                push    bc              ; Save sign info
                bit     7,d
                call    nz,neg_dehl     ; Take absolute value of divisor

                exx

                bit     7,d
                call    nz,neg_dehl

                call    divu_32_32_32_no0

                ; DEHL = unsigned quotient
                ; DE'HL = unsigned remainder

                pop     bc              ; BC = sign info
                ld      a,b
                xor     c
                call    m,neg_dehl
                bit     7,b
                ret     z               ; If dividend > 0

                exx
                call    neg_dehl        ; Negate remainder
                exx

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; neg_dehl
;; Negate a 32-bit word held in DEHL
;;
;; Input:
;;      DEHL = value
;;
;; Output:
;;      DEHL = -value
;;
;; Taken from Z88DK source code
;;

invert          macro   reg
                ld      a,reg
                cpl
                ld      reg,a
                endm

neg_dehl:
                invert  l
                invert  h
                invert  e
                invert  d
                inc     l
                ret     nz
                inc     h
                ret     nz
                inc     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; divu_32_32_32
;; Unsigned 32-bit division
;;
;; Input:
;;      DE'HL' = 32-bit dividend
;;      DEHL = 32-bit divisor
;;
;; Output:
;;      Success:
;;              CF = 0
;;              DEHL = 32-bit quotient
;;              DE'HL' = 32-bit remainder
;;
;;      Divide by zero:
;;              CF = 1
;;
;; Taken from Z88DK source code
;;

divu_32_32_32:
                ld      a,d
                or      e
                or      h
                or      l
                scf
                ret     z
divu_32_32_32_no0:
                xor     a
                push    hl
                exx
                ld      c,l
                ld      b,h
                pop     hl
                push    de 
                ex      de,hl
                ld      l,a
                ld      h,a
                exx
                pop     bc
                ld      l,a
                ld      h,a

                ; BCBC' = 32-bit dividend
                ; DEDE' = 32-bit divisor
                ; HLHL' = 0

                ld      a,b
                ld      b,32
.l0:
                exx
                rl      c
                rl      b
                exx
                rl      c
                rla

                exx
                adc     hl,hl
                exx
                adc     hl,hl

                exx
                sbc     hl,de
                exx
                sbc     hl,de
                jr      nc,.l1

                exx
                add     hl,de
                exx
                adc     hl,de
.l1:
                ccf
                djnz    .l0

                exx
                rl      c
                rl      b
                exx
                rl      c
                rla

                ; Quotient = ACBC'
                ; Remainder = HLHL'
                push    hl
                exx
                pop     de
                push    bc
                exx
                pop     hl
                ld      e,c
                ld      d,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
