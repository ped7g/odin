;;----------------------------------------------------------------------------------------------------------------------
;; DOT command initialisation and shutdown
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Saves state

OldSP           dw      0       ; Original SP
OldIY           dw      0       ; Original IY
OldMMUs         ds      6       ; Original MMU state
OldSpeed        db      0       ; Original clock speed
OldIntState     db      0
OldIntVector    db      0
Args            dw      0       ; Contains pointer to arguments

;;----------------------------------------------------------------------------------------------------------------------
;; dotStart
;;
;; Run after SP has first been saved and set up:
;;
;;              ld (OldSP),sp
;;              ld sp,$....
;;

dotStart:
                ld      (Args),hl
                
                ; Store interrupt state
                ld      hl,0
                push    hl
                pop     hl              ; Above stack is 0
                scf                     ; Assume interrupts are on!
                ld      a,i             ; Put IFF2 into p/v flags
                ld      (OldIntVector),a

                ; If parity even interrupts are on, but interrupt could happen during this instruction and screw flags
                jp      pe,.continue

                ; If the interrupt did occur during then the value above stack will be non-zero
                dec     sp
                dec     sp
                pop     hl              ; Grab the value above the top of stack
                ld      a,h
                or      l
                jr      z,.continue     ; No interrupt happen, CF = 0 and jump.
                scf                     ; Interrupt did occur, CF = 1

.continue:
                ld      a,1
                jr      c,.ints_on
                xor     a
.ints_on:
                ld      (OldIntState),a



                ; Wait for the keyboard to be fully released
                di
.wait_key:
                xor     a
                in      a,($fe)
                cpl
                and     %00011111
                jr      nz,.wait_key

                ; Store IY
                ld      (OldIY),iy

                ; Store the MMU state
                rreg    REG_MMU2
                ld      (OldMMUs+0),a
                rreg2   REG_MMU3
                ld      (OldMMUs+1),a
                rreg2   REG_MMU4
                ld      (OldMMUs+2),a
                rreg2   REG_MMU5
                ld      (OldMMUs+3),a
                rreg2   REG_MMU6
                ld      (OldMMUs+4),a
                rreg2   REG_MMU7
                ld      (OldMMUs+5),a

                ; Set the state of the MMU to a known state.
                ; This pages in the ULA and sysvars pages.
                page    2,$0a
                page    3,$0b

                ; Set the clock speed to 28MHz (because why the hell not?)
                rreg2   REG_CLK_SPEED
                and     3
                ld      (OldSpeed),a
                reg     REG_CLK_SPEED,3

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; dotEnd
;;
;; Don't forget to restore interrupts.
;;

dotEnd:
                di

                ; Restore MMU state
                ld      a,(OldMMUs+0)
                page    2,a
                ld      a,(OldMMUs+1)
                page    3,a
                ld      a,(OldMMUs+2)
                page    4,a
                ld      a,(OldMMUs+3)
                page    5,a
                ld      a,(OldMMUs+4)
                page    6,a
                ld      a,(OldMMUs+5)
                page    7,a

                ld      sp,(OldSP)
                ld      iy,(OldIY)
                ld      a,(OldSpeed)
                reg     REG_CLK_SPEED,a

                ; Restore interrupt state
                ld      a,(OldIntVector)
                ld      i,a
                ld      a,(OldIntState)
                and     a
                jr      z,.ints_off
                ei
.ints_off:
                xor     a
                ret


