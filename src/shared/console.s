;;----------------------------------------------------------------------------------------------------------------------
;; Console emulation for .ed
;;----------------------------------------------------------------------------------------------------------------------

ConsolePages    db      0, 0
OrigBorder      db      0
ScreenBase      dw      $4000


C_LEFT          equ     VK_LEFT
C_RIGHT         equ     VK_RIGHT
C_DOWN          equ     VK_DOWN
C_UP            equ     VK_UP
C_ENTER         equ     VK_ENTER
C_BACKSPACE     equ     VK_DELETE
C_DELETE        equ     VK_EXT0
C_CLS           equ     VK_EXTL
C_CLEARLINE     equ     VK_EXTX
C_OVERWRITE     equ     VK_EXTO
C_HOME          equ     VK_EXT5
C_END           equ     VK_EXT8
C_CLEARTOEND    equ     VK_EXTE
C_EDITOR        equ     VK_EDIT


;;----------------------------------------------------------------------------------------------------------------------
;; initConsole
;; Initialise the video mode, load the font, set up the palette and clear the screen.
;;----------------------------------------------------------------------------------------------------------------------

initConsole:
                ld      a,($5c48)
                ld      (OrigBorder),a

                page    2,10
                page    3,11

                ; Backup MMU 2 & 3
                call    allocPage
                ld      (ConsolePages+0),a
                page    4,a
                call    allocPage
                ld      (ConsolePages+1),a
                page    5,a

                ld      hl,$4000
                ld      de,$8000
                ld      bc,$4000
                call    memcpy

                ; Load in font
                call    pageDivMMC
                dos     M_GETHANDLE     ; A = file handle to dot command
                ld      hl,$6000        ; Tiles data
                ld      bc,$800         ; 1K of data
                dos     F_READ          ; Read in the font data
                call    pageOutDivMMC
                ;  |
                ; fallthrough into modeConsole
                ;  |
                ;  v
modeConsole:
                ; Set the border back
                xor     a
                out     ($fe),a

                ; Disable L2
                ld      bc,IO_LAYER2
                xor     a
                out     (c),a

                ; Set up the tilemap
                reg     REG_TMAP_CTL,%11001011          ; Tilemap control: Enable, 80x32, Attrs, 1st palette,
                                                        ;                  text mode, 512 tiles, tilemap over ULA
                reg     REG_TMAP_BASE,$00               ; Tilemap base offset: $4000-$53ff (80*32*2)
                reg     REG_TILES_BASE,$20              ; Tiles base offset: $6000-$6800 (8*256)
                reg     REG_TMAP_TRANS,8                ; Transparency colour (bright black)
                reg     REG_ULA_CTL,%10000000           ; Disable ULA output

                ; Reset scrolling and clip window
                xor     a
                reg     REG_CLIP_TMAP,a
                reg     REG_CLIP_TMAP,159
                reg     REG_CLIP_TMAP,a
                reg     REG_CLIP_TMAP,255
                reg     REG_TMAP_XMSB,a
                reg     REG_TMAP_XLSB,a
                reg     REG_TMAP_Y,a

                ; Clear the screen
                call    cls

                ; Initialise the tilemap palette
                reg     REG_PAL_CTL,%00110000           ; Set tilemap palette for editing
                reg     REG_PAL_INDEX,a                 ; Start at palette index 0
                ld      c,8
                ld      de,Palette
.l1:            ; colours set work with attribute byte: PPPIIII0, P=paper, I=ink)
                ld      b,16
                ld      hl,Palette
.l2:
                ld      a,(de)
                reg     REG_PAL_VAL8,a                  ; Set the paper colour
                ld      a,(hl)
                reg     REG_PAL_VAL8,a                  ; Set the ink colour
                inc     hl                              ; ++ink
                djnz    .l2                             ; go for ink colours 0-15 with paper X
                inc     de                              ; ++paper
                dec     c
                jr      nz,.l1                          ; go for paper colours 0-7 (ink 0-15 each)

                ; Set cursor position, BC is zero from palette loop
                jr      at

;;----------------------------------------------------------------------------------------------------------------------
;; Palette table

Palette:                        ;       R       G       B
                db  %00000000   ;       0       0       0       Black
                db  %00000010   ;       0       0       5       Blue
                db  %10100000   ;       5       0       0       Red
                db  %10100010   ;       5       0       5       Magenta
                db  %00010100   ;       0       5       0       Green
                db  %00010110   ;       0       5       5       Cyan
                db  %10110100   ;       5       5       0       Yellow
                db  %10110110   ;       5       5       5       Light grey
                db  %01101101   ;       3       3       3       Dark grey
                db  %00000011   ;       0       0       7       Bright blue
                db  %11100000   ;       7       0       0       Bright red
                db  %11100011   ;       7       0       7       Bright magenta
                db  %00011100   ;       0       7       0       Bright green
                db  %00011111   ;       0       7       7       Bright cyan
                db  %11111100   ;       7       7       0       Bright yellow
                db  %11111111   ;       7       7       7       White

;;----------------------------------------------------------------------------------------------------------------------
;; doneConsole
;; Restore the ULA mode
;;----------------------------------------------------------------------------------------------------------------------

doneConsole:
                ; Restore pages 10 and 11
                ld      a,(ConsolePages+0)
                page    4,a
                ld      a,(ConsolePages+1)
                page    5,a
                ld      hl,$8000
                ld      de,$4000
                ld      bc,$4000
                call    memcpy
                ld      a,(ConsolePages+0)
                call    freePage
                ld      a,(ConsolePages+1)
                call    freePage
                ;  |
                ; fallthrough into modeULA
                ;  |
                ;  v
modeULA:
                ; Disable tilemap and enable ULA
                xor     a
                reg     REG_ULA_CTL,a
                reg     REG_TMAP_CTL,a

                ; Restore border colour
                ld      a,(OrigBorder)
                rrca
                rrca
                rrca
                out     (IO_ULA),a

                ; Clear the screen
                page    2,10
                page    3,11
                ld      hl,$4000
                ld      bc,$1800
                call    memclear
                ld      hl,$5800
                ld      bc,768
                ld      a,(OrigBorder)
                and     $38
                call    memfill

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cls
;; Clear the screen
;;

cls:
                push    af,bc,hl
                ld      hl,(ScreenBase)
                ld      bc,2*kScreenWidth*kScreenHeight
                call    memclear
                pop     hl,bc,af
                ret

CurrentCoords   dw      0               ; YX coords of cursor position
CurrentPos      dw      0               ; Tile address of cursor position

;;----------------------------------------------------------------------------------------------------------------------
;; at
;; Sets the cursor position
;;
;; Input:
;;      BC = YX coord
;;
;; Output:
;;      HL = tile address
;;      CurrentPos and CurrentCoords are set accordingly too
;;

at:
                call    cursorHide
                ld      (CurrentCoords),bc
                call    calcAddr
                ld      (CurrentPos),hl
                jr      cursorShow

;;----------------------------------------------------------------------------------------------------------------------
;; scrAddr
;; Get the tile address at the current cursor.
;;
;; Output:
;;      HL = tile address
;;      BC = current coords (YX)
;;

scrAddr:
                ld      bc,(CurrentCoords)
                ;  |
                ; fallthrough into caclAddr
                ;  |
                ;  v

;;----------------------------------------------------------------------------------------------------------------------
;; calcAddr
;; Calculate address for given coords
;;
;; Input:
;;      BC = coords (YX)
;;
;; Output:
;;      HL = tile address
;;

calcAddr:
                push    af,de
                ld      e,b
                ld      d,kScreenWidth
                mul
                ld      a,c
                add     de,a            ; DE = y * 80 + x
                ld      hl,(ScreenBase)
                add     hl,de
                add     hl,de           ; HL = screen base + 2 * DE
                pop     de,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; setColour
;; Set the current colour for printing
;;
;; Input:
;;      A = colour (0 PPP B III)        P = paper, B = bright (for ink only), I = ink
;;
;; Output:
;;      A = tilemap attribute for text mode
;;

setColour:
                add     a,a
                ld      (CurrentColour),a
                ret

CurrentColour   db      %0'000'0'111 << 1

;;----------------------------------------------------------------------------------------------------------------------
;; Cursor control
;;
;; If enable = 1, the cursor can be shown.  If 0, visible should also be 0.
;; If visible = 1, the cursor is currently shown on the screen.
;; If hidden = 1, the cursor can not be rendered as it's moving.  Visible should also be 0 at this point.
;;

CursorFlag      db      0               ; Bit 0 = Enable, 1 = Visible, 2 = Hidden
CursorColour    db      %0'100'1'111    ; Current cursor colour
CursorBack      db      0               ; Saved attribute when cursor is drawn

cursorDraw:
                ; Draw the cursor on the screen
                push    af,bc

                ; Don't draw the cursor if disabled, visible or hidden
                ld      a,(CursorFlag)
                dec     a               ; must be %001 to draw
                jr      nz,.end

                push    hl
                
                call    pageVideo
                call    scrAddr         ; HL = address of cursor
                inc     hl              ; HL = address of attribute
                ld      a,(hl)
                ld      (CursorBack),a
                ld      a,(CursorColour)
                add     a,a
                ld      (hl),a

                ld      hl,CursorFlag
                set     1,(hl)

                pop     hl
.end:
                pop     bc,af
                ret

cursorRemove:
                ; Remove the cursor from the screen
                push    af,bc

                ; Don't remove the cursor if disabled, not visible or hidden
                ld      a,(CursorFlag)
                xor     %011
                jr      nz,.end

                push    hl

                call    pageVideo
                call    scrAddr         ; HL = address of cursor
                inc     hl              ; HL = address of attribute
                ld      a,(CursorBack)
                ld      (hl),a

                ld      hl,CursorFlag
                res     1,(hl)

                pop     hl
.end:
                pop     bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorOn
;; Turn the cursor on
;;

cursorOn:
                push    hl
                ld      hl,CursorFlag
                set     0,(hl)
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorOff
;; Turn the cursor off

cursorOff:
                call    cursorRemove            ; Remove the cursor if it's there
                push    hl
                ld      hl,CursorFlag
                res     0,(hl)
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorHide
;; Remove the cursor from the screen to move it.  This does not affect it's show/hide state.

cursorHide:
                ; Remove the cursor if showing and not hidden yet (checks CursorFlag)
                call    cursorRemove

                ; Mark as hidden (re-setting it multiple times doesn't affect result)
                push    hl
                ld      hl,CursorFlag
                set     2,(hl)
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorShow
;; Display the cursor and copy the attribute under the cursor - BE CAREFUL don't call showCursor while it us being
;; shown.

cursorShow:
                push    af,bc,hl

                ; If already showing, just exit
                ld      hl,CursorFlag
                bit     2,(hl)
                jr      z,.end

                ; Update the flag
                res     2,(hl)

                ; Update the colour to remove the cursor
                call    scrAddr
                inc     hl
                ld      a,(hl)
                ld      (CursorBack),a

.end:
                pop     hl,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; consoleUpdate
;; Update the cursor flashing

consoleUpdate:
                ; If the cursor is off, there's nothing to do
                ld      a,(Counter)
                and     16              ; Flash every 16 frames
                jp      nz,cursorDraw
                jp      cursorRemove

;;----------------------------------------------------------------------------------------------------------------------
;; colouredPrint
;;
;; Input:
;;      A = colour
;;

colouredPrint:
                call    setColour
                ex      (sp),hl
                call    printHL
                ex      (sp),hl
                ld      a,kInkNormal
                jp      setColour

;;----------------------------------------------------------------------------------------------------------------------
;; errorPrint
;; Print a message after the call as an error.  VK_EDIT is output to clear the line and the ink is set to the error 
;; colour.  Ink is restored afterwards to normal text.  Finally, VK_ENTER is outputted afterwards.
;;

errorPrint:
                call    pageVideo
                call    cursorHide
                ld      a,kInkError
                call    setColour
                ld      a,C_CLEARLINE
                call    printChar
                ex      (sp),hl
                call    printHL
                ex      (sp),hl
                ld      a,C_ENTER
                call    printChar
                ld      a,kInkNormal
                call    setColour
                scf
                jr      cursorShow

;;----------------------------------------------------------------------------------------------------------------------
;; print
;; Print the text after the call point

print:
                call    cursorHide
                ex      (sp),hl         ; HL = data to print
                call    printHL
                ex      (sp),hl         ; Restore HL and set return address after the data
                jr      cursorShow

printHL:
                push    bc
                ld      b,kScreenWidth
                call    printHLWidth
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; printHLWidth
;; Similar to printHL but only allows a maximum number of characters to be printed.
;;
;; Input:
;;      HL = null terminated text
;;      B = number of characters
;;
;; Output:
;;      HL = points beyond the null terminator
;;

printHLWidth:
                push    af,bc

                ; Turn off insert mode
                ld      a,(InsertMode)
                ld      c,a
                ld      a,$ff
                ld      (InsertMode),a

                inc     b
                jr      .entry

.l1:
                ldi     a,(hl)
                and     a
                jr      z,.end          ; Finish if character is null
                call    printChar
                cp      C_ENTER
                jr      nz,.not_eol
                ld      b,kScreenWidth  ; 80 because C_ENTER is $0d -> `jr c,.l1` below loops
.not_eol:
                cp      $21
                        ; ^ Matt&Ped could not make sense of this, why spaces are not counted?
                        ; looks like legacy feature from old editor, keeping it like that ATM
                jr      c,.l1           ; Next character, don't count whitespace
                cp      $e1
                jr      nc,.l1
.entry:
                djnz    .l1
                call    strEnd          ; if print is aborted by char limit, advance HL
                inc     hl              ; beyond the null-terminator, at next Z80 instruction
.end:
                ; Restore insert mode
                ld      a,c
                ld      (InsertMode),a

                pop     bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; printSpace
;; Print a single space character on the screen.  Ensure that cursor is not showing.
;;

printSpace:
                push    af
                ld      a,' '
                jr      printChar.p_space_entry

;;----------------------------------------------------------------------------------------------------------------------
;; printChar
;; Print a character on the screen and handle control codes.  Ensure that cursor is not showing.  print will do this
;; for you.
;;

printChar:
                push    af
.p_space_entry:
                push    bc,hl           ; DE is not used by current printChar, add it if needed
                call    .print_char
                pop     hl,bc,af
                ret

.print_char:
                ld      hl,(CurrentPos)
                ld      bc,(CurrentCoords)
                cp      $b0
                jp      nc,.ctrlCode
                cp      ' '
                jp      c,.ctrlCode     ; Character is a control code, so deal with it

                ; Check insert mode (0 = insert, 1 = overwrite)
                push    af
                ld      a,(InsertMode)
                add     a,a                     ; A->CF
                ; Shift the characters to the right if we're in insert mode
                call    nc,moreRoom     ; Shift the characters on the line to the right
                pop     af
                ld      (hl),a          ; Draw character
                inc     l               ; char address should be even, so inc+dec L is enough
                ld      a,(CurrentColour)
                ld      (hl),a          ; And colour it
                dec     l

.move_right:
                ld      a,c
                cp      kScreenWidth-1  ; Reached edge of screen?
                ret     nc              ; don't advance then = keep old CurrentCoords/Pos
.move_right_force:
                inc     c
                inc     l
                inc     hl
.update:
                ld      (CurrentCoords),bc
                ld      (CurrentPos),hl
                ret

.ctrlCode:
                ; Deal with control codes
                cp      C_RIGHT                 ; Right
                jr      z,.move_right
                cp      C_LEFT                  ; Left
                jr      z,.move_left
                cp      C_DOWN                  ; Down
                jr      z,.move_down
                cp      C_UP                    ; Up
                jr      z,.move_up
                cp      C_ENTER                 ; Enter
                jr      z,.enter
                cp      C_BACKSPACE             ; Delete backwards
                jr      z,.backspace
                cp      C_DELETE                ; Delete forwards
                jr      z,.delete
                cp      C_CLS                   ; CLS
                jr      z,.cls
                cp      C_CLEARLINE             ; Clear line
                jp      z,.clearLine
                cp      C_OVERWRITE             ; Overwrite mode
                jp      z,.overwrite
                cp      C_HOME                  ; Home
                jr      z,.home
                cp      C_END                   ; End
                jr      z,.end
                cp      C_CLEARTOEND            ; Clear to end of line
                jr      z,.clearToEnd

                ; All unknown codes are ignored
                ret

.enter:
                ld      c,0
                call    calcAddr        ; Go to beginning of line
                ;  | fallthrough
                ;  v
.move_down:
                inc     b
                add     hl,kScreenWidth*2
                ld      a,b
                cp      kScreenHeight   ; Reached end of screen?
                jr      c,.update
                call    scrollUp
                ; Get the cursor back on the screen
                ;  | fallthrough
                ;  v
.move_up:
                dec     b
                ret     m               ; Y was 0, stay at CurrentCoords/Pos
                add     hl,-kScreenWidth*2
                jr      .update

.move_left:
                dec     hl
                dec     l
                dec     c
                jp      p,.update               ; ok, 0 <= --X
                ld      c,kScreenWidth-1        ; Cursor at end of previous line
                dec     b
                ret     m                       ; already at Y == 0, ignore C_LEFT
                jr      .update                 ; ok, 0 <= --Y

.backspace:
                dec     c
                ret     m                       ; first column will produce -1, do nothing
                dec     hl
                dec     l
                ;  | fallthrough
                ;  v
.delete:
                call    lessRoom
                jr      .update

.cls:
                call    cls
                ld      bc,0
                ld      hl,(ScreenBase)
                jr      .update

.home:
                ld      c,0
                call    calcAddr
                jr      .update

.end:
                ld      c,kScreenWidth-1
                call    calcAddr                ; HL+BC = end of line
                ld      a,(hl)
                and     a
                jp      nz,.update              ; the last character is non-zero
.end_loop:
                dec     hl
                dec     l
                dec     c
                jp      m,.move_right_force     ; X == -1, no char found, move right+update
                ld      a,(hl)
                and     a
                jr      z,.end_loop
                jp      .move_right_force       ; put cursor after the last non-zero char

.clearLine:
                call    .home                   ; HL+BC = beginning of line + ".update"
                ;  | fallthrough
                ;  v
.clearToEnd:
                ld      a,kScreenWidth
                sub     c
                add     a,a
                ld      c,a
                xor     a
                ld      b,a
                jp      memfill                 ; return through memfill, don't ".update"

.overwrite:
                ld      a,(InsertMode)
                cpl
                ld      (InsertMode),a          ; alternate between 0/$FF (insert/overwrite)
                and     %0'110'0'000
                xor     %0'100'1'111            ; A = %0'100'1'111 / %0'010'1'111 (insert/overwrite)
                ld      (CursorColour),a
                ret                             ; no need to ".update", keeps old Coords/Pos

InsertMode      db      0

;;----------------------------------------------------------------------------------------------------------------------
;; moreRoom
;; Make more room on the line by shifting the characters to the right
;;
;; Input:
;;      HL = tilemap position
;;      C = X position
;;

moreRoom:
                push    af,bc,de,hl
                ld      a,kScreenWidth-1
                sub     c
                add     a,a
                ld      c,a
                ld      b,0
                ld      de,hl
                inc     de
                inc     de
                call    memcpy_r
                pop     hl,de,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; lessRoom
;; Shift the characters from the position to the left, inserting empty spaces on the right
;;
;; Input:
;;      HL = tilemap position
;;      C = X position
;;

lessRoom:
                push    af,bc,de,hl
                ld      a,kScreenWidth-1
                sub     c
                add     a,a
                ld      c,a
                xor     a
                ld      b,a
                ld      de,hl
                inc     hl
                inc     hl
                call    memcpy
                add     hl,bc
                dec     hl
                ld      (hl),a
                dec     hl
                ld      (hl),a
                pop     hl,de,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; scrollUp
;; Scroll the screen upwards
;;

scrollUp:
                push    af,bc,de,hl

                ; Move tiles up wards
                ld      hl,kScreenTop+(kScreenWidth*2)      ; Point to 2nd line
                ld      de,kScreenTop                       ; Move to 1st line
                ld      bc,kScreenWidth*2*(kScreenHeight-1) ; Move 31 lines
                call    memcpy
                ld      hl,kScreenTop+(kScreenWidth*2*(kScreenHeight-1))    ; HL = last line
                ld      bc,kScreenWidth*2
                call    memclear

                pop     hl,de,bc,af
                ret
 
;;----------------------------------------------------------------------------------------------------------------------
;; printBCD7
;; Print a 7 digit number in DEHL
;;
;; printBCD5
;; Print a 5 digit number in EHL
;;
;; printBCD4
;; Print a 4 digit number in HL
;;

printBCD7:
                ; Digit 1
                ld      a,d
                call    printDigit

                ; Digit 2
                ld      a,e
                swap
                call    printDigit

printBCD5:
                ; Digit 3
                ld      a,e
                call    printDigit

printBCD4:
                ; Digit 4
                ld      a,h
                swap
                call    printDigit

                ; Digit 5
                ld      a,h
                call    printDigit

                ; Digit 6
                ld      a,l
                swap
                call    printDigit

                ; Digit 7
                ld      a,l

printDigit:
                and     $0f
                add     a,$30
                jp      printChar

;;----------------------------------------------------------------------------------------------------------------------
;; printHexWord
;;
;; Input:
;;      HL = word
;;

printHexWord:
                ld      a,h
                call    printHexByte
                ld      a,l
                ;  |
                ; fallthrough into printHexByte
                ;  |
                ;  v
;;----------------------------------------------------------------------------------------------------------------------
;; printHexByte
;;
;; Input:
;;      A = byte
;;

printHexByte:
                push    af
                swap
                call    printHexDigit
                pop     af
                ;  |
                ; fallthrough into printHexDigit
                ;  |
                ;  v
;;----------------------------------------------------------------------------------------------------------------------
;; printHexDigit
;;
;; Input:
;;      A = nybble
;;

printHexDigit:
                and     $0f
                ; Convert nybble to ASCII
                cp      10
                sbc     a,$69
                daa
                jp      printChar

;;----------------------------------------------------------------------------------------------------------------------
;; checkLineCount
;; Check to see if the screen as been filled yet
;;
;; Output:
;;      ZF = 1 if break is pressed
;;

checkLineCount:
                ld      a,(LineCount)
                inc     a
                ld      (LineCount),a
                sub     kScreenHeight-1
                jp      nz,breakPressed
                ld      (LineCount),a
                call    pageVideo
                call    cursorHide
                ld      a,kInkNormal
                call    colouredPrint
                dz      C_CLEARLINE,"[Press BREAK to stop]"
                ld      a,kInkResponse
                call    setColour
                call    cursorShow
                call    breakPause
                call    print
                dz      C_CLEARLINE
                ret

LineCount       db      0

resetLineCount:
                xor     a
                ld      (LineCount),a
                call    resetKeys
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; insertOff
;; Used to temporarily turn off insert
;;

insertOff:
                push    af
                ld      a,(InsertMode)
                ld      (.lastInsert),a
                ld      a,$ff
.setAndPopAf:
                ld      (InsertMode),a
                pop     af
                ret

.lastInsert     db      0

;;----------------------------------------------------------------------------------------------------------------------
;; insertRestore
;; Restore the insert mode before insertOff was called
;;

insertRestore:
                push    af
                ld      a,(insertOff.lastInsert)
                jr      insertOff.setAndPopAf

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

