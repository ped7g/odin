# Editor commands

## Single command keys

| Key        | Description                  |
| ---------- | ---------------------------- |
| BREAK      | Escape block mode            |
| EDIT       | Exit editor                  |
| DELETE     | Delete previous character    |
| TRUE VIDEO | Move to next field           |
| INV VIDEO  | Scroll up                    |
| ENTER      | Insert new line below        |
| CAPSLOCK   | Toggle Capslock              |
| GRAPH      | Scroll down                  |
| LEFT       | Move cursor left             |
| DOWN       | Move cursor to next line     |
| UP         | Move cursor to previous line |
| RIGHT      | Move cursor right            |

## Extended commands

These are keys that are pressed in conjunction with `EXTENDED MODE`.

| Key            | Description                   |
|----------------|-------------------------------|
| EXT+ENTER      | Insert new line above         |
| EXT+EDIT       | Switch file                   |
| EXT+CAPS       | -                             |
| EXT+TRUE VIDEO | Beginning of file             |
| EXT+INV VIDEO  | End of file                   |
| EXT+LEFT       | Move to start of line         |
| EXT+DOWN       | Move one page down            |
| EXT+UP         | Move one page up              |
| EXT+RIGHT      | Move to end of line           |
| EXT+GRAPH      | -                             |
| EXT+DELETE     | Delete character under cursor |
| EXT+A          | -                             |
| EXT+B          | -                             |
| EXT+C          | Copy current line/block       |
| EXT+D          | Duplicate current line/block  |
| EXT+E          | Delete to end                 |
| EXT+F          | -                             |
| EXT+G          | -                             |
| EXT+H          | -                             |
| EXT+I          | -                             |
| EXT+J          | -                             |
| EXT+K          | -                             |
| EXT+L          | -                             |
| EXT+M          | -                             |
| EXT+N          | -                             |
| EXT+O          | Toggle overwrite mode         |
| EXT+P          | -                             |
| EXT+Q          | Reformat                      |
| EXT+R          | -                             |
| EXT+S          | Save current file             |
| EXT+T          | -                             |
| EXT+U          | -                             |
| EXT+V          | Paste line/block              |
| EXT+W          | Close current document        |
| EXT+X          | Cut line/block                |
| EXT+Y          | -                             |
| EXT+Z          | -                             |
| EXT+SPACE      | Enter block mode              |

## Symbol-Shift commands

| Key   | Description   |
| ----- | ------------- |
| Sym+W | Previous word |
| Sym+E | Next word     |
| Sym+I | -             |